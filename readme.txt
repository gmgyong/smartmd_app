- Get source from SVN: https://svn.s3corp.com.vn:8443/Projects/GMG/Artifacts/SourceCode/GlobalMedicalGroup - just add this folder on D:/
- Open Command Line on folder project
  + npm i
- Open Visual Studio, add project: project/android, waiting update plugin, run build first step
- Close and open Visual Studio again.
- Open Explorer, focus this project, open file .gitignore get list will ignore for nodejs and android. Add them on SVN.
- Open simulator or connect real android device
- Open Command Line on folder project
  + react-native run-android
