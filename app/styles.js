import React, { Component, PropTypes } from 'react';
import { StyleSheet, Dimensions, } from 'react-native';
import { Colors, FontSizes } from './configs/constants';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const GET_SUBTRACT = DEVICE_WIDTH - DEVICE_HEIGHT >= 0 ? DEVICE_WIDTH - DEVICE_HEIGHT : DEVICE_HEIGHT - DEVICE_WIDTH;

var styles = StyleSheet.create({
  /* commons */
  wrapper: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_MAIN_SCREEN,
  },
  /* Loading indicator */
  indicatorPageLoading: {
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  indicatorPageLoadingNew: {
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    opacity: 0
  },
  textBig: {
    fontSize: FontSizes.BIG
  },
  textBold: {
    fontWeight: 'bold',
  },
  textCenter: {
    textAlign: 'center',
  },
  normalText: {
    fontSize: FontSizes.NORMAL,
    color: Colors.PRIMARY,
    paddingTop: 3,
  },
  redText: {
    fontSize: FontSizes.NORMAL,
    color: Colors.TEXT_TITLE_ERROR,
    paddingTop: 3,
  },
  errorColor: {
    color: Colors.TEXT_TITLE_ERROR,
  },
  line: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.TEXT_PRIMARY,
    marginTop: 5,
    marginBottom: 5,
  },
  buttonView: {
    marginTop: 10,
    marginLeft: 0,
    marginRight: 0,
  },
  button: {
    height: 35,
    borderRadius: 4,
  },
  buttonPurple: {
    backgroundColor: Colors.BACKGROUND_BUTTON_PURPLE,
  },
  buttonPurpleOver: {
    backgroundColor: Colors.BACKGROUND_BUTTON_PURPLE_OVER,
  },
  buttonBlue: {
    backgroundColor: Colors.BACKGROUND_BUTTON_BLUE,
  },
  buttonBlueOver: {
    backgroundColor: Colors.BACKGROUND_BUTTON_BLUE_OVER,
  },
  buttonBlueDisable: {
    backgroundColor: Colors.BACKGROUND_BUTTON_BLUE_DISABLE,
  },
  buttonGrey: {
    backgroundColor: Colors.BACKGROUND_BUTTON_GREY,
  },
  buttonGreyOver: {
    backgroundColor: Colors.BACKGROUND_BUTTON_GREY_OVER,
  },
  blockInline: {
    flex: 1,
    flexDirection: 'row',
  },
  blockgreyDetail: {
    backgroundColor: Colors.BACKGROUND_FIELD,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 15,
    marginTop: 10,
    // marginBottom: 10,
  },
  chatLogContainer: {
    backgroundColor: Colors.BACKGROUND_MAIN_SCREEN,
    borderWidth: 1,
    borderColor: Colors.BORDER,
    marginTop: 10,
  },
  chatLogWrapper: {
    height: 250,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20,
  },
  chatLogBlock: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 10,
    borderRadius: 4,
  },
  chatLogDoctor: {
    alignSelf: 'flex-start',
    backgroundColor: Colors.BACKGROUND_CHATLOG_DOCTOR,
  },
  chatLogPatient: {
    alignSelf: 'flex-end',
    backgroundColor: Colors.BACKGROUND_CHATLOG_PATIENT
  },
  chatLogTimeText: {
    fontSize: FontSizes.SMALL,
    color: Colors.TEXT_CHATLOG_DATETIME,
  },
});

module.exports = styles;
