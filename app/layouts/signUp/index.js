import React from 'react'
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator
} from 'react-native'
import {connect} from 'react-redux'

import I18n from 'react-native-i18n'
import {CheckBox, Button} from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Picker from '../../components/form/Picker'
import  TopLabelField from '../../components/form/TopLabelField'
import {Label, TextField, ErrorMes} from '../../components/form/Field'
import {dataCC} from '../../data/CountryCode'
import {dataTimezone} from '../../data/Timezone'
import localStyles from './localStyles'
import globalStyles from '../../styles'

import {APIs, StorageKeys, Colors} from '../../configs/constants'
import GMGClient from '../../configs/gmgHttpClient'
import LocalStorage from '../../components/localStorage'


class SignUpScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      invalid: true,
      swithPasswordErrorMess: false,
      swithEmailErrorMess: false,
      errorAPI: {
        value: '',
        error: false
      },
      fullName: {
        value: '',
        error: undefined,
      },
      email: {
        value: '',
        error: undefined,
      },
      countryCode: {
        value: '',
        error: undefined,
      },
      number: {
        value: '',
        error: undefined,
      },
      mobileNo: {
        value: '',
        error: undefined,
      },
      timezone: {
        value: '',
        label: '',
        error: undefined,
      },
      password: {
        value: '',
        error: undefined,
      },
      reTypePass: {
        value: '',
        error: undefined,
      },
      terms: {
        value: false,
        error: undefined,
      },
    }
  }
  static navigationOptions = ({navigation}) => ({
    headerTitle: I18n.t('signUp.title'),
  })

  /*static navigationOptions = {
   title: I18n.t("signUp.title")
   };*/

  handleSubmit = () => {
    this.setState({errorAPI: {error: false}, swithEmailErrorMess: false});

    let check = this.validate();
    (check) ? this.callAPI() : null;
  };

  handleCancel = () => {
    const {navigate} = this.props.navigation;
    navigate('SignIn');
  };

  callAPI = () => {

    this.setLoadingProgress(true);

    const bodyData = {
      name: this.state.fullName.value,
      country_code: this.state.countryCode.value,
      phone: this.state.number.value,
      email: this.state.email.value,
      timezone: this.state.timezone.value,
      password: this.state.password.value,
    }

    GMGClient.postData(
      APIs.SIGN_UP,
      bodyData,
      json => {
        this.handleAPI(json);
        this.setLoadingProgress(false);
      },
      error => {
        console.log('error');
        console.log(error);
        this.setLoadingProgress(false);
      }
    )
  }

  handleAPI = data => {
    switch (data.status_code) {
      case 200: {
        const {navigate} = this.props.navigation;

        //save email login to LocalStorage
        LocalStorage.setString('username', this.state.email.value);

        navigate('SignIn');
        break;
      }
      case 402: {
        this.setState({errorAPI: {value: data.errors.join('\n'), error: true}});
        break;

      }
      default: {
        this.setState({errorAPI: {value: data.status_code + '\n' + data.errors.join('\n'), error: true}});
        break;
      }
    }
  }

  validate = () => {
    if (this.state.invalid) {
      if (this.state.fullName.error === undefined)
        this.setState({fullName: {error: true}});

      if (this.state.countryCode.error === undefined)
        this.setState({countryCode: {error: true}});

      if (this.state.number.error === undefined)
        this.setState({number: {error: true}});

      if (this.state.mobileNo.error === undefined)
        this.setState({mobileNo: {error: true}});

      if (this.state.email.error === undefined)
        this.setState({email: {error: true}});

      if (this.state.timezone.error === undefined)
        this.setState({timezone: {error: true}});

      if (this.state.password.error === undefined)
        this.setState({password: {error: true}});

      if (this.state.reTypePass.error === undefined)
        this.setState({reTypePass: {error: true}});

      if (this.state.terms.value === false)
        this.setState({terms: {error: true}});
      return false;
    } else {
      return true;
    }
  };

  onChangeFullName = async value => {
    const regex = /^\s+/g;

    let check = () => {
      if (value === '' || value === undefined || regex.test(value)) {
        this.setState({fullName: {error: true}});
      } else {
        this.setState({fullName: {value: value, error: false}})
      }
    };
    await check();
    this.checkValidate();
  };

  onChangeNumber = async value => {
    const regex = /^\d+$/;

    let checkNo = () => {
      if (value === '' || value === undefined || !regex.test(parseInt(value))) {
        this.setState({number: {error: true}});
      } else {
        this.setState({number: {value: value, error: false}})
      }

    };

    let checkMobileNo = () => {
      if (this.state.countryCode.error === false && this.state.number.error === false) {
        this.setState({mobileNo: {error: false}});
      } else {
        this.setState({mobileNo: {error: true}});
      }
    };

    await checkNo();
    await checkMobileNo();
    this.checkValidate();

  };

  onChangeEmail = async value => {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let check = () => {
      if (value === '' || value === undefined ) {
        this.setState({email: {error: true}});
      }else if(!regex.test(value)) {
        this.setState({email: {error: true}, swithEmailErrorMess: true});
      }
      else {
        this.setState({email: {value: value, error: false}})
      }
    };

    await check();
    this.checkValidate();
  };

  onChangePassword = async value => {
    let check = () => {
      if (value === '' || value === undefined) {
        this.setState({password: {error: true}});
      }else if (value.length < 8) {
        this.setState({password: {error: true}, swithPasswordErrorMess: true})
      } else {
        this.setState({password: {value: value, error: false}})
      }
    };

    await check();
    this.checkValidate();
  };

  onChangeReTypePass = async value => {
    let check = () => {
      let password = this.state.password.value;
      if (value === '' || value === undefined || value !== password) {
        this.setState({reTypePass: {error: true}});
      } else {
        this.setState({reTypePass: {value: value, error: false}})
      }
    };

    await check();
    this.checkValidate();

  };

  onChangeTerms = async () => {
    let set = () => {
      this.setState(prevState => {
        return {terms: {value: !prevState.terms.value, error: false}}
      });
    };

    await set();
    this.checkValidate();
  };
  componentDidMount(){
    this.setState({timezone: {value: 'Asia/Singapore', label: 'Asia/Singapore - GMT+8', error: false}});
  }

  onChangeTimezone = (option) => {
    this.setState({timezone: {value: option.value, label: option.label, error: false}});
    this.checkValidate();
  };

  onChangeCC = async value => {
    let check = () => {
      this.setState({countryCode: {value: value, error: false}});

      if (this.state.countryCode.error === false && this.state.number.error === false) {
        this.setState({mobileNo: {error: false}});
      } else {
        this.setState({number: {error: true}});
        this.setState({mobileNo: {error: true}});
      }
    };

    await check();
    this.checkValidate();
  };

  checkValidate = () => {
    if (this.state.fullName.error === false &&
      this.state.countryCode.error === false &&
      this.state.number.error === false &&
      this.state.email.error === false &&
      this.state.timezone.error === false &&
      this.state.password.error === false &&
      this.state.reTypePass.error === false &&
      this.state.terms.value === true
    ) {
      this.setState({invalid: false});
    } else {
      this.setState({invalid: true});

    }
  };


  renderLoadingIndicator = (isShowing) => {
    if (isShowing) {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={globalStyles.indicatorPageLoading}
          color={Colors.TEXT_SECONDARY}
          size='small'/>
      )
    } else {
      return null;
    }
  }

  setLoadingProgress = (isDataLoading) => {
    this.setState({isDataLoading});
  }

  render() {
    return (
      <View style={ localStyles.wrapper }>
        <KeyboardAwareScrollView keyboardShouldPersistTaps='handled' style={ localStyles.container }>
          <Text style={ localStyles.text }>{ I18n.t("signUp.textIntro") }</Text>
          <TopLabelField
            label={ I18n.t("signUp.fullName.label") }
            placeholder={ I18n.t("signUp.fullName.placeholder") }
            onChangeText={ (value) => this.onChangeFullName(value) }
            errorMes={ I18n.t("signUp.fullName.errorMes") }
            error={ this.state.fullName.error }

            returnKeyType="next"
            onSubmitEditing={ () => this.inputPhoneNumber.focus() }
          />
          <View>
            <Label label={ I18n.t("signUp.mobileNo.label") } error={ this.state.mobileNo.error }/>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Picker
                  data={ dataCC }
                  placeholder={ I18n.t("signUp.mobileNo.placeholderCC") }
                  error={ this.state.countryCode.error }
                  value={ this.state.countryCode.value }
                  onChange={(option) => this.onChangeCC(option.dial_code) }
                />
              </View>
              <View style={{flex: 5, marginLeft: 10}}>
                <TextField
                  placeholder={ I18n.t("signUp.mobileNo.placeholderNumber") }
                  error={ this.state.number.error }
                  onChangeText={ (value) => this.onChangeNumber(value) }

                  inputRef={ node => this.inputPhoneNumber = node }
                  returnKeyType="next"
                  onSubmitEditing={ () => this.inputEmail.focus() }
                />
              </View>
            </View>
            { (this.state.mobileNo.error) ? <ErrorMes errorMes={ I18n.t("signUp.mobileNo.errorMes") }/> : null }
            <Text style={ localStyles.text }>{ I18n.t("signUp.mobileNo.caption") }</Text>
          </View>
          <TopLabelField
            label={ I18n.t("signUp.email.label") }
            placeholder={ I18n.t("signUp.email.placeholder") }
            onChangeText={ (value) => this.onChangeEmail(value) }
            errorMes={ this.state.swithEmailErrorMess ? I18n.t("signUp.email.swithEmailErrorMess") : I18n.t("signUp.email.errorMes") }
            error={ this.state.email.error }

            inputRef={ node => this.inputEmail = node }
            returnKeyType="next"
            onSubmitEditing={ () => this.inputPassword.focus() }
          />
          <Picker
            label={ I18n.t("signUp.timezone.label") }
            data={ dataTimezone }
            placeholder={ I18n.t("signUp.timezone.placeholder") }
            error={ this.state.timezone.error }
            errorMes={ I18n.t("signUp.timezone.errorMes") }
            value={ this.state.timezone.label }
            onChange={(option) => this.onChangeTimezone(option) }
          />
          <TopLabelField
            label={ I18n.t("signUp.password.label") }
            placeholder={ I18n.t("signUp.password.placeholder") }
            onChangeText={ (value) => this.onChangePassword(value) }
            errorMes={ this.state.swithPasswordErrorMess? I18n.t("signUp.password.swithPasswordErrorMess") : I18n.t("signUp.password.errorMes") }
            error={ this.state.password.error }
            secureTextEntry={ true }

            inputRef={ node => this.inputPassword = node }
            returnKeyType="next"
            onSubmitEditing={ () => this.inputReTypePass.focus() }
          />
          <Text style={ localStyles.text }>{ I18n.t("signUp.password.caption") }</Text>
          <TopLabelField
            label={ I18n.t("signUp.reTypePass.label") }
            placeholder={ I18n.t("signUp.reTypePass.placeholder") }
            onChangeText={ (value) => this.onChangeReTypePass(value) }
            errorMes={ I18n.t("signUp.reTypePass.errorMes") }
            error={ this.state.reTypePass.error }
            secureTextEntry={ true }

            inputRef={ node => this.inputReTypePass = node }
            returnKeyType="done"
          />
          <CheckBox
            title={ I18n.t("signUp.terms.text") }
            textStyle={ localStyles.textCheckBox }
            checked={ this.state.terms.value }
            onPress={ this.onChangeTerms }
            containerStyle={ localStyles.containerCheckBox }
            checkedColor="#35c1cf"
            uncheckedColor={ (this.state.terms.error) ? "#f23d2c" : "#bcbcbc" }
          />
          { (this.state.terms.error) ? <ErrorMes errorMes={ I18n.t("signUp.terms.errorMes") }/> : null }
          { (this.state.errorAPI.error) ? <ErrorMes errorMes={ this.state.errorAPI.value } arrow={ false}/> : null}
          <Button
            title={ I18n.t("signUp.btnSignUp") }
            onPress={ this.handleSubmit }
            buttonStyle={ localStyles.buttonBlue }
            containerViewStyle={ localStyles.button }
          />
          <Button
            title={ I18n.t("signUp.btnCancel") }
            onPress={ this.handleCancel }
            buttonStyle={ localStyles.buttonGray }
            containerViewStyle={ localStyles.button }
          />
          {this.renderLoadingIndicator(this.state.isDataLoading)}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

export default connect(mapStateToProps, null)(SignUpScreen)
