import {StyleSheet, Dimensions} from 'react-native'
import { MaxWidth, SPACE } from '../../configs/constants'

// let SPACE = (Dimensions.get('window').width - MaxWidth)/2;

// SPACE = (SPACE < 30) ? 30 : SPACE

const localStyles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff',
    flex: 1
  },
  container: {
    paddingLeft: SPACE,
    paddingRight: SPACE,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#fff',
    flex: 1
  },
  text: {
    fontSize: 12,
    lineHeight: 18,
    color: '#898989',
    marginBottom: 10,
  },
  textCheckBox: {
    fontWeight: 'normal',
    fontSize: 12,
    color: '#898989',
  },
  containerCheckBox: {
    marginTop: 0, marginRight: 0, marginBottom: 15, marginLeft: 0,
    paddingTop: 0, paddingRight: 0, paddingBottom: 0, paddingLeft: 0,
    backgroundColor: null,
    borderWidth: 0,
  },
  button: {
    marginLeft: 0, marginRight: 0,
  },
  buttonBlue: {
    backgroundColor: '#35c1cf',
    height: 35,
    borderRadius: 4,
    marginBottom: 10,
  },
  buttonGray: {
    backgroundColor: '#bcbcbc',
    height: 35,
    borderRadius: 4,
    marginBottom: 50,
  },
});
module.exports = localStyles;
