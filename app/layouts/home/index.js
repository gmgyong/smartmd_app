import React, { Component, PropTypes } from 'react'
import { View, Text, ActivityIndicator, ScrollView, Alert, Platform, AsyncStorage, TouchableHighlight } from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import { navigate } from 'react-navigation'
import { Icon } from 'react-native-elements'
// import TestAPIActions from '../../lib/mutilLanguages/Redux/TestRedux'
import ListAppointments from '../../components/listView/appointments'
import ListFastConnect from '../../components/listView/fastConnect'
import LocalStorage from '../../components/localStorage'
import {Button} from 'react-native-elements'
import moment from 'moment-timezone'
import FormatDate from '../../configs/formatDate'
import Icons from '../../configs/icons'
import { Colors, StorageKeys, APIs } from '../../configs/constants'
import localStyles from './localStyles'
import globalStyles from '../../styles'
import GMGClient from '../../configs/gmgHttpClient'
import ActionsAPIs from '../../configs/actionsAPIs'

class HomeScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isDataLoading: true,
      userId:'',
      listFastConnect: [],
      totalFastConnect: 0,
      listAppointmentConfirmed: []/*{
        data:[],
        total: 0
      }*/,
      listAppointmentReady: {
        data: [],
        total: 0
      },
      listAppointmentRequest: {
        data:[],
        total: 0
      },
      listAppointmentWaiting: {
        data: [],
        total: 0
      },
      notification: null
      }
    }

  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarIcon: Icons.Home,
    tabBarPosition: 'top',
  })

  async componentDidMount() {
    const userId = await LocalStorage.getString(StorageKeys.USERID)
    const notification = JSON.parse(await LocalStorage.getJson(StorageKeys.NOTIFICATION))

    console.log(notification, 'notification init')

    this.setState({userId})

    if(notification !== undefined && notification !== null && notification && notification.user_id.toString() === userId) {
      this.setState({notification})
      console.log(notification, 'check notification')
    }
    // ActionsAPIs.getListAppointmentConfirmed(userId)
    this.getListFastConnect()
    this.getlistAppointmentConfirmed()
    this.getListAppointmentRequest()
    this.checkTimezone()

    // this.props.screenProps.setNavigate(this.props.navigation.navigate)
  }

  checkTimezone = () => {
    const {userId} = this.state
    const urlAPI = APIs.USER_INFO + '/' + userId + '?'

    GMGClient.getData(
      urlAPI,
      json => {
        if (json.success) {
          let timezoneSignUp = moment.tz(json.data.timezone).format('Z')
          let timezoneLocal = moment.tz(moment.tz.guess()).format('Z')

          timezoneSignUp !== timezoneLocal
          ? Alert.alert(
              'Different Timezone',
              'Your device time zone does not match your current Profile setting',
            )
          : null
        }
      },
      error => {
        alert(error)
      }
    )
  }
  getListFastConnect = async () => {
    // const userEmail = "thu.do@s3corp.com.vn"
    const userEmail = await LocalStorage.getString(StorageKeys.USEREMAIL)
    const urlAPI = APIs.LIST_FAST_CONNECT + "?filter_patient_email=" + userEmail
    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          console.log('json.data.fastconnect: ', json.data)
          this.setState({
            listFastConnect: json.data,
            totalFastConnect: json.data.length

          })
        }
        console.log(this.state)
      },
      error => {
        alert(error)
      }
    )
  }

  getlistAppointmentConfirmed = () => {
    const urlAPI =
      APIs.LIST_APPOINTMENT_READY
      + "?user_id=" + this.state.userId + "&per_page=100000"

    this.setState({ isDataLoading: true })

    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          console.log('json.data: ', json.data)
          // console.log('json.data: ', json.data)
          this.setState(
            {
              isDataLoading: false,
              listAppointmentConfirmed: json
            },
            () => this.handleListAppointmentReady(json.data)
          )
        }
      },
      error => {
        alert(error)
      }
    )
  }

  handleListAppointmentReady = data => {
    this.setState(
      {
        listAppointmentReady: {
          data: [],
          total: 0
        },
        listAppointmentConfirmed: data
      }
    )
    return data.map(item => {
      item.scheduled_time = FormatDate.switchToLocalTime(item.scheduled_time)

      item.status === 'Ready' || item.status === 'Upcoming'
        ? this.setState(prevState => (
        {
          listAppointmentReady: {
            data: [
              ...prevState.listAppointmentReady.data, item
            ],
            total: prevState.listAppointmentReady.total + 1
          }
        }))
        : null
    })
  }
  getListAppointmentRequest = () => {
    const urlAPI =
      APIs.LIST_APPOINTMENT_REQUEST
      + "?user_id=" + this.state.userId + "&per_page=100000"

    this.setState({ isDataLoading: true })

    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          this.setState(
            {
              isDataLoading: false,
              listAppointmentRequest: json
            },
            () => this.handleListAppointmentWaiting(json.data)
          )
        }
        console.log(this.state)
      },
      error => {
        alert(error)
      }
    )
  }

  handleListAppointmentWaiting = data => {
    this.setState(
      {
        listAppointmentWaiting: {
          data: [],
          total: 0
        }
      }
    )
    return data.map( item => (
      item.doctor_response_type === 'Awaiting Response'
        ? this.setState( prevState => (
        {
          listAppointmentWaiting: {
            data: [
              ...prevState.listAppointmentWaiting.data, item
            ],
            total: prevState.listAppointmentWaiting.total + 1
          }
        }))
        : null
    ))
  }

  renderListAppointment = () => (
    <View>
      {this.state.notification !== null && this.state.notification !== undefined ? this.renderNotification() : null}
      {this.state.totalFastConnect !== 0 ? this.renderlistFastConnect() : null}
      {this.state.listAppointmentReady.total !== 0 ? this.renderlistAppointmentReady() : null}
      {this.state.listAppointmentWaiting.total !== 0 ? this.renderListAppointmentWaiting() : null}
    </View>
  )

  renderlistFastConnect = () =>  (
    <View>
      <Text style={localStyles.title}>
        { I18n.t('appointments.fastConnect.title').toUpperCase() }
      </Text>
      <ListFastConnect
        navigation = { this.props.navigation }
        listRef='fastConnectList'
        listFastConnect = {this.state.listFastConnect} />
    </View>
  )

  renderlistAppointmentReady = () => (
    <View>
      <Text style={localStyles.title}>
        { I18n.t('home.readyList.title').toUpperCase() }
      </Text>
      <ListAppointments
        navigation = { this.props.navigation }
        listRef='readyList'
        listAppointments = {this.state.listAppointmentReady.data} />
    </View>
  )

  renderListAppointmentWaiting = () => (
    <View>
      <Text style={localStyles.title}>
        { I18n.t('home.requestList.title').toUpperCase() }
      </Text>
      <ListAppointments
        navigation = { this.props.navigation }
        listRef='awaitingList'
        listAppointments = {this.state.listAppointmentWaiting.data} />
    </View>
  )

  renderAlertAppointment = () => (
    <View style={{paddingTop: 5, paddingLeft:5, paddingRight: 5}}>
      <Text>{ I18n.t('home.isNotAppointment') }</Text>
    </View>
  )

  renderLoadingIndicator = () => {
    if (this.state.isDataLoading) {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={[globalStyles.indicatorPageLoading, {backgroundColor: '#FFF'}]}
          color={Colors.TEXT_SECONDARY}
          size='large'
        />
      )
    } else {
      return null
    }
  }

  renderNotification = () => {
    let  {notification} = this.state
    let title = ''

    switch(notification.action) {
      case 'DOCTOR_CONFIRM': {
        title = 'Doctor confirmed'
        break
      }
      case 'DOCTOR_CANCEL': {
        title = 'Doctor cancel'
        break
      }
      case 'DOCTOR_WAITING': {
        title = 'Doctor waiting'
        break
      }
      case 'DOCTOR_RESCHEDULE': {
        title = 'Doctor reschedule'
        break
      }
      case 'FAST_CONNECT_INVITE': {
        title = 'Fast connect invite'
        break
      }
      case 'FAST_CONNECT_CONFIRM': {
        title = 'Fast connect confirm'
        break
      }
      default: {
        break
      }
    }

    return (
      <View>
        <Text style={localStyles.title}>
          { I18n.t('home.notification.title').toUpperCase() }
        </Text>
        <TouchableHighlight
          underlayColor='#dcccb0'
          style={localStyles.blockDoctor}
          onPress={ () => this.loadDetailNotification(notification) }>
          <View style={localStyles.blockDetail}>
            <View style={[localStyles.blockInfo, {flex: 2}]}>
              <Text style={[localStyles.textInfo]}>
                {title.toUpperCase()}
              </Text>
              <Text style={localStyles.textInfo}>
                {notification.body}
              </Text>
            </View>
            <View style={localStyles.blockIcon}>
              <Icon size={14} name="chevron-right" type='font-awesome' color={Colors.TEXT_SECONDARY} />
            </View>
          </View>
        </TouchableHighlight>
      </View>
    )
  }

  loadDetailNotification = (notification) => {
    const {navigate} = this.props.navigation
    const {userId} = this.state

    let appointment_id = notification.appointment_id
    let appointment_request_id = notification.appointment_request_id
    let status = notification.action
    let invite_id = notification.invite_id

    LocalStorage.removeKey(StorageKeys.NOTIFICATION)

    switch (status) {
      case 'DOCTOR_CONFIRM':
        navigate('AppointmentDetailPatient',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
      case 'DOCTOR_RESCHEDULE':
        navigate('AppointmentDetailPatient',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
      case 'DOCTOR_WAITING':
        navigate('AppointmentDetailPatient',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
      case 'DOCTOR_CANCEL':
        navigate('AppointmentDetailPatient',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
      case 'FAST_CONNECT_CONFIRM':
        navigate('AppointmentDetailPatient',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
      case 'FAST_CONNECT_INVITE':
        navigate('FastConnect',{idFastConnectDetails: invite_id});
        break
      /*case 'Past':
        navigate('SummaryAppointment',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
      case 'Patient No Show':
        navigate('SummaryAppointment',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
      case 'Both No Show':
        navigate('SummaryAppointment',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
      case 'Cancelled':
        navigate('AppointmentDetailPatient',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
      case 'Error':
        navigate('AppointmentDetailPatient',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break*/
      default:
        navigate('AppointmentDetailPatient',{user_id: userId, appointment_id: appointment_id, appointment_request_id: appointment_request_id});
        break
    }
  }

  render() {
    const {language, /*listAppointmentConfirmed*/} = this.props
    const {navigate, setParams} = this.props.navigation

    return (
      <View style={ localStyles.wrapper }>
        {this.renderLoadingIndicator()}

        <ScrollView>
          { this.state.totalFastConnect === 0 && this.state.listAppointmentReady.total === 0 && this.state.listAppointmentWaiting.total === 0 && !this.state.isDataLoading
            ? this.renderAlertAppointment()
            : this.renderListAppointment()
          }
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language,
  }
}

export default connect(mapStateToProps, null)(HomeScreen)
