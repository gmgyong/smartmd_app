import { StyleSheet } from 'react-native'
import { Colors, FontSizes } from '../../configs/constants'

const localStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#fff',
  },
  title: {
    paddingTop: 20,
    paddingLeft: 10,
    color: Colors.TEXT_SECONDARY,
  },
  blockDoctor: {
    backgroundColor: Colors.BACKGROUND_FIELD,
    marginTop: 10,
  },
  blockDetail: {
    flex: 1,
    flexDirection:'row',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  blockInfo: {
    paddingTop: 2,
    paddingRight: 5,
  },
  textInfo: {
    flexDirection: 'row',
    fontSize: FontSizes.NORMAL,
  },
  blockIcon: {
    paddingTop: 2,
    paddingRight: 10,
    justifyContent: 'center'
  },
})

/** export module */
module.exports = localStyles
