import React from 'react'
import {AsyncStorage} from 'react-native'
import LocalStorage from '../../components/localStorage'
import {StorageKeys} from '../../configs/constants'
import GMGClient from '../../configs/gmgHttpClient'
import {APIs, AccountAuth} from '../../configs/constants'
import {AccessToken} from 'react-native-fbsdk'
import { GoogleSignin } from 'react-native-google-signin'


import Loading from '../../components/loading'
import localStyles from './localStyles'


export default class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    // const token = await LocalStorage.getString(StorageKeys.TOKENDATA);
    //
    // if (token) {
    //   this._loadInitialState()
    // } else {
    //   GMGClient.getAccessTocken(this._loadInitialState)
    // }

    this._loadInitialState()
  }

  _loadInitialState = async () => {
    const {navigate} = this.props.navigation

    let typeLogin = await LocalStorage.getString(StorageKeys.TYPE_LOGIN)

    let bodyData = {
      "username": await LocalStorage.getString(StorageKeys.USERNAME),
      "password": await LocalStorage.getString(StorageKeys.PASSWORD),
      // 'api_token': await LocalStorage.getString(StorageKeys.TOKENDATA)
      // deviceUid: DeviceInfo.getUniqueID(),
      // timezone: moment.tz.guess(),
    }
    let bodyUpdate = {
      device_token: await LocalStorage.getString(StorageKeys.DEVICE_TOKEN),
      platform: await LocalStorage.getString(StorageKeys.DEVICE_OS),
      is_login: 1
    }
    console.log("============================================")
    console.log(bodyData)

    switch (typeLogin) {
      case 'FACEBOOK': {
        AccessToken.getCurrentAccessToken()
          .then( data => {
            if(data.accessToken) {
              console.log("=======LOGIN WITH FACEBOOK ACCOUNT")
              navigate('Authorized')
            } else {
              console.log("=======FAIL LOGIN WITH FACEBOOK")
              navigate('SignIn')
            }
          })
          .catch(error => {
            console.log(error)
            console.log("=======TOKEN FACEBOOK EXPIRED")

            navigate('SignIn')
          })
          .done()
        break
      }
      case 'GOOGLE': {

        navigate('Authorized')

        // GoogleSignin.getAccessTocken()
        //   .then( token => {
        //     if(token) {
        //       console.log("=======LOGIN WITH GOOGLE ACCOUNT")
        //       navigate('Authorized')
        //     } else {
        //       console.log("=======FAIL LOGIN WITH GOOGLE")
        //       navigate('SignIn')
        //     }
        //   })
        //   .catch(error => {
        //     console.log(error)
        //     console.log("=======TOKEN GOOGLE EXPIRED")
        //
        //     navigate('SignIn')
        //   })
        //   .done()
        break
      }
      default: {

        if (bodyData.password !== null) {
          GMGClient.postData(
            APIs.SIGN_IN,
            bodyData,
            json => {
              if (json.success) {
                // let intervalCheckSignIn = setInterval(() => {
                //   clearInterval(intervalCheckSignIn)
                //
                // }, 1000)
                LocalStorage.setString(StorageKeys.USERID, json.data.id.toString());
                LocalStorage.setString(StorageKeys.USERFULLNAME, json.data.name)
                LocalStorage.setString(StorageKeys.USEREMAIL, json.data.email)
                LocalStorage.setString(StorageKeys.USERURL, json.data.photo_url)

                // Update device informations
                let urlAPI = APIs.UPDATE_DEVICE_TOKEN + '/' + json.data.id.toString()

                GMGClient.postData(
                  urlAPI,
                  bodyUpdate,
                  res => {
                    console.log(res, 'Update Device Success')
                  },
                  error => {
                    console.log(error, 'Update Device Error')
                  }
                )

                navigate('Authorized')
              } else {
                // let intervalCheckSignIn = setInterval(() => {
                //   clearInterval(intervalCheckSignIn)
                // }, 1000)
                navigate('SignIn')
              }
            },
            error => {
              // let intervalCheckSignIn = setInterval(() => {
              //   clearInterval(intervalCheckSignIn)
              //
              // }, 3000)
              navigate('SignIn')
            }
          )
        } else {
          console.log('not save info login, then navigate to sign in')
          setTimeout(() => navigate('SignIn'), 2000)
        }
        break
      }
    }

  }

  render() {
    return (
      <Loading />
    );
  }
}
