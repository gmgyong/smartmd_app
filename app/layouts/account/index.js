import React, { Component, PropTypes } from 'react'
import { View, Text, TouchableOpacity, Button } from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import Card from '../../../app/components/Card'
import { navigate } from 'react-navigation'
import LocalStorage from '../../components/localStorage'
import Icons from '../../configs/icons'
import localStyles from './localStyles'
import {StorageKeys, APIs} from '../../configs/constants'
import GMGClient from '../../configs/gmgHttpClient'
import {LoginManager} from 'react-native-fbsdk'
import { GoogleSignin } from 'react-native-google-signin'

class AccountScreen extends Component {
  constructor(props) {
    super(props)
    this.state= {
      userFullName: '',
      userPhoto: '',
      userEmail: '',
      tokenDevice: '',
      osDevice: '',
      userId: ''
    }
  }

  static navigationOptions = ({ navigation }) => ({
    // title: I18n.t('account.title')
    header: null,
    tabBarIcon: Icons.Account,
    tabBarPosition: 'top'
  })

  async componentDidMount() {
    let userFullName = await LocalStorage.getString(StorageKeys.USERFULLNAME)
    let userPhoto = await LocalStorage.getString(StorageKeys.USERURL)
    let userEmail = await LocalStorage.getString(StorageKeys.USEREMAIL)
    let tokenDevice = await LocalStorage.getString(StorageKeys.DEVICE_TOKEN)
    let osDevice = await LocalStorage.getString(StorageKeys.DEVICE_OS)
    let userId = await LocalStorage.getString(StorageKeys.USERID)

    this.setState({
      userFullName,
      userPhoto,
      userEmail,
      tokenDevice,
      osDevice,
      userId
    })

    if(!userPhoto || userPhoto === null || userPhoto === '') {
      const urlAPI = APIs.USER_INFO + '/' + userId + '?'
      // this.setLoadingProgress(true);

      GMGClient.getData(
        urlAPI,
        json => {
          if (json.success) {
            // this.setLoadingProgress(false);
            this.setState({
              userPhoto: json.data.photo_url,
            })
          }
        },
        error => {
          alert(error)
        }
      )
    }
    
    // console.log(this.state)
  }

  render() {
    const {language} = this.props;
    const {navigate, setParams} = this.props.navigation;
    return (
      <View style={ localStyles.container }>
        <Card
          // backgroundImage={ require('../../images/bgDoctor.jpg')}
          userImage={ this.state.userPhoto}
          heading= { this.state.userFullName }
          subHeading= { this.state.userEmail }
        />
        <View style={ localStyles.wrapContent }>
          <View>
            <View style={ localStyles.wrapTitle }>
              <Text style={ localStyles.title }>
              {I18n.t("account.title").toUpperCase()}
              </Text>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => navigate('ChangeProfile')}
              style={ localStyles.wrapText }
            >
              <Text style={ localStyles.text }>
              {I18n.t("account.changeProfile.title")}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => navigate('Payment')}
              style={ localStyles.wrapText }
            >
              <Text style={ localStyles.text }>
              {I18n.t("account.payment.title")}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => navigate('ChangePassword')}
              style={ localStyles.wrapText }
            >
              <Text style={ localStyles.text }>
              {I18n.t("account.changePassword.title")}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.7}
              // onPress={() => navigate('Home')}
              style={ localStyles.wrapText }
              onPress={
                async () => {
                  // Update device informations
                  let bodyUpdate = {
                    device_token: this.state.tokenDevice,
                    platform: this.state.osDevice,
                    is_login: 0
                  }
                  let urlAPI = APIs.UPDATE_DEVICE_TOKEN + '/' + this.state.userId

                  const typeLogin = await LocalStorage.getString(StorageKeys.TYPE_LOGIN)
                  console.log("type login: " + typeLogin)

                  GMGClient.postData(
                    urlAPI,
                    bodyUpdate,
                    res => {
                      console.log(res, 'Update Device Success')
                    },
                    error => {
                      console.log(error, 'Update Device Error')
                    }
                  )

                  switch (typeLogin) {
                    case 'FACEBOOK': {
                      console.log("LOG OUT FACEBOOK SERVICE")
                      LoginManager.logOut()
                      LocalStorage.removeKey(StorageKeys.TYPE_LOGIN)
                      LocalStorage.removeKey(StorageKeys.USERNAME)
                      LocalStorage.removeKey(StorageKeys.PASSWORD)
                      break
                    }
                    case 'GOOGLE': {
                      console.log("LOG OUT GOOGLE SERVICE")

                      LocalStorage.removeKey(StorageKeys.TYPE_LOGIN)
                      LocalStorage.removeKey(StorageKeys.USERNAME)
                      LocalStorage.removeKey(StorageKeys.PASSWORD)

                      GoogleSignin.revokeAccess()
                        .then(
                          () => GoogleSignin.signOut()
                        )
                        .done()
                      break
                    }
                    default: {
                      console.log("LOG OUT NORMAL")
                      LocalStorage.removeKey(StorageKeys.PASSWORD)
                      LocalStorage.removeKey(StorageKeys.USERURL)
                      break
                    }
                  }

                  navigate('Unauthorized')
                }
              }
            >
              <Text style={ localStyles.text }>
                {I18n.t("account.signOut")}
              </Text>
            </TouchableOpacity>

          </View>
        </View>
        <View style={ localStyles.wrapContent }>
          <View>
            <View style={ localStyles.wrapTitle }>
              <Text style={ localStyles.title }>
              {I18n.t("account.setting").toUpperCase()}
              </Text>
            </View>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => navigate('SettingLanguages')}
              style={ localStyles.wrapText }
            >
              <Text style={ localStyles.text }>
              {I18n.t("account.language.titleLower")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

export default connect(mapStateToProps, null)(AccountScreen)
