import React from 'react'
import {View, Text, TextInput, ScrollView, TouchableOpacity, Image, ActivityIndicator, Keyboard, Platform} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {Button, Avatar} from 'react-native-elements'
import {RNS3} from 'react-native-aws3'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {navigate} from 'react-navigation'
import  TopLabelField from '../../../components/form/TopLabelField'
import {dataCC} from '../../../data/CountryCode'
import {dataTimezone} from '../../../data/Timezone'
import Picker from '../../../components/form/Picker'
import globalStyles from '../../../styles'
import {Label, ErrorMes, TextField } from '../../../components/form/Field'
import Environments from '../../../configs/environmentsAPIs'
import localStyles from './localStyles'
import {APIs, StorageKeys, Colors} from '../../../configs/constants'
import LocalStorage from '../../../components/localStorage'
import GMGClient from '../../../configs/gmgHttpClient'

const ImagePicker = require('react-native-image-picker')

let options = {
  title: 'Select Avatar',
  // customButtons: [
  //   {name: 'fb', title: 'Choose Photo from Facebook'},
  // ],
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

class ChangeProfileScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      user_id: '',
      name: '',
      photo_url: '',
      email: '',
      country_code: '',
      phone: '',
      timezone: '',
      drug_history: '',
      medical_history: '',
      drug_allergies: '',
      errorAPI: {
        value: '',
        error: false,
        mesType: 'SUCCESS'
      },
      file: {
        uri: '',
        name: '',
        type: ''
      },
      isDataLoading: false,
      isDataLoadingImage: false
    }
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle: I18n.t('account.changeProfile.title').toUpperCase(),
  })

  getAPIUserInfo = async () => {
    this.setLoadingProgress(true);
    const user_id = await LocalStorage.getString(StorageKeys.USERID)
    this.setState({user_id})

    const urlAPI = APIs.USER_INFO + '/' + user_id + '?'

    GMGClient.getData(
      urlAPI,
      json => {
        if (json.success) {
          this.setLoadingProgress(false);
          this.setState({
            name: json.data.name,
            photo_url: json.data.photo_url,
            email: json.data.email,
            country_code: json.data.country_code,
            phone: json.data.phone,
            timezone: json.data.timezone,
            drug_history: json.data.drug_history,
            medical_history: json.data.medical_history,
            drug_allergies: json.data.drug_allergies
          })
        }
      },
      error => {
        alert(error)
      }
    )
  }

  updateAPIUserInfo = () => {
    const state = this.state
    const urlAPI = APIs.USER_INFO + '/' + state.user_id
    const body = {
      user_id: state.user_id,
      name: state.name,
      photo_url: state.photo_url,
      email: state.email,
      country_code: state.country_code,
      phone: state.phone,
      timezone: state.timezone,
      drug_history: state.drug_history,
      medical_history: state.medical_history,
      drug_allergies: state.drug_allergies
    }

    this.setLoadingProgress(true)

    this.setState({
      errorAPI: {
        error: false
      }
    })

    Keyboard.dismiss()

    if(state.file.uri !== '') {
      this.handleUploadS3(
        state.file,
        (res) => {
          console.log(res, 'upload photo')

          this.setState({
            photo_url: res.body.postResponse.location
          })

          body.photo_url = res.body.postResponse.location

          GMGClient.putData(
            urlAPI,
            body,
            json => {
              this.setLoadingProgress(false)

              if(!json.success) {
                this.setState({
                  errorAPI: {
                    error: true,
                    value: json.errors.join('\n'),
                    mesType: 'ERROR'
                  }
                })
              } else {
                LocalStorage.setString(StorageKeys.USERURL, res.body.postResponse.location)
                LocalStorage.setString(StorageKeys.USERFULLNAME, state.name)

                this.setState({
                  errorAPI: {
                    error: true,
                    value: 'Success!',
                    mesType: 'SUCCESS'
                  }
                })
              }
            },
            error => {
              this.setLoadingProgress(false)
            }
          )
        },
        (err) => {
          this.setLoadingProgress(false)
          console.log(err)
        }
      )
    } else {
      GMGClient.putData(
        urlAPI,
        body,
        json => {
          this.setLoadingProgress(false)

          if(!json.success) {
            this.setState({
              errorAPI: {
                error: true,
                value: json.errors.join('\n'),
                mesType: 'ERROR'
              }
            })
          } else {
            LocalStorage.setString(StorageKeys.USERURL, state.photo_url)
            LocalStorage.setString(StorageKeys.USERFULLNAME, state.name)

            this.setState({
              errorAPI: {
                error: true,
                value: 'Success!',
                mesType: 'SUCCESS'
              }
            })
          }
        },
        error => {
          this.setLoadingProgress(false)
        }
      )
    }



  }

  setLoadingProgress = (isDataLoading) => {
    this.setState({isDataLoading});
  }

  renderLoadingIndicator = (isShowing) => {
    if (isShowing) {
      // console.log(isShowing, isTransparent, 'checking')
      return (
        <ActivityIndicator
          animating={isShowing}
          style={[globalStyles.indicatorPageLoading]}
          color={Colors.TEXT_SECONDARY}
          size='small'/>
      )
    } else {
      return null;
    }
  }

  renderLoadingIndicatorImage = (isShowing) => {
    if (isShowing) {
      // console.log(isShowing, isTransparent, 'checking')
      return (
        <ActivityIndicator
          animating={isShowing}
          style={[globalStyles.indicatorPageLoading, {backgroundColor: 'transparent'}]}
          color={Colors.TEXT_SECONDARY}
          size='small'/>
      )
    } else {
      return null;
    }
  }

  onChangeCC = async value => {
    let check = () => {
      this.setState({country_code: value});
    };
    await check();
  };

  onChangeTimezone = async value => {
    let check = () => {
      this.setState({timezone: value});
    };
    await check();
  };

  changeImage = () => {
    let {state} = this.state

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        // this.setLoadingProgress(true)

        let file = {}

        const contentType = (type) => {
          switch(type) {
            case 'jpg':
              return 'image/jpeg'
              break
            case 'jpeg':
              return 'image/jpeg'
              break
            case 'png':
              return 'image/png'
              break
            case 'gif':
              return 'image/gif'
              break
            case 'bmp':
              return 'image/bmp'
              break
            case 'tiff':
              return 'image/tiff'
              break
            case 'pdf':
              return 'application/pdf'
              break
            case 'doc':
              return 'application/msword'
              break
            case 'docx':
              return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
              break
            case 'xls':
              return 'application/vnd.ms-excel'
              break
            case 'ppt':
              return 'application/vnd.ms-powerpoint'
              break
            case 'pptx':
              return 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
              break
            case 'mp4':
              return 'application/mp4'
              break
            case 'H264':
              return 'video/H264'
              break
            case 'H265':
              return 'video/H265'
              break
            case '3gpp':
              return 'video/3gpp'
              break
          }
        }

        if(Platform.OS === 'ios') {
          let arr = response.uri.split('/')
          arrName = arr[arr.length - 1].split('.')
          file = {
            uri: response.uri,
            name: arr[arr.length - 1],
            type: contentType(arrName[1])
          }

        } else {
          arrName = response.fileName.split('.')
          file = {
            uri: response.uri,
            name: response.fileName,
            type: response.type
          }
        }

        this.setState({
          file
        })

        /*this.handleUploadS3(
          file,
          (res) => {
            console.log(res, 'upload photo')
            this.setState({
              photo_url: res.body.postResponse.location
            })
            this.setLoadingProgress(false)
          },
          (err) => {
            console.log(err)
          }
        )*/
      }
    });
  }

  handleUploadS3 = async (file, success, failure) => {
    const options = {
      keyPrefix: "/",
      bucket: Environments.AWS_S3_BUCKET,
      region: Environments.AWS_S3_REGION,
      accessKey: Environments.AWS_S3_KEY,
      secretKey: Environments.AWS_S3_SECRET_KEY,
      successActionStatus: 201
    }
    RNS3.put(file, options).then(res => {
      if (res.status !== 201) {
        console.log("Failed to upload image to S3", res)
        return failure(res)
      } else {
        console.log("Success to upload image to S3", res)
        return success(res)
      }
    })
  }

  // onChangeTimezone = (option) => {
  //   this.setState({timezone: {value: option.value, label: option.label}});
  // };

  renderImage = () => {
    const { file, photo_url } = this.state

    return (
      <View style={{position: 'relative'}}>
        <Image
          source={{uri: file.uri !== '' && file.uri !== null ? file.uri : photo_url}}
          style={ [localStyles.figureImage, localStyles.userImage] }
          onLoadStart={(e) => {
            this.setState({isDataLoadingImage: true})
          }}
          onLoadEnd={(e) => {
            this.setState({isDataLoadingImage: false})
          } } />

        { this.state.isDataLoadingImage ? this.renderLoadingIndicatorImage(this.state.isDataLoadingImage) : null }
      </View>
    )
  }

  componentDidMount() {
    this.getAPIUserInfo()
  }

  render() {
    const { goBack } = this.props.navigation
    const { file, photo_url } = this.state

    return (
      <View style={localStyles.wrapper}>
        <ScrollView>
          <KeyboardAwareScrollView keyboardShouldPersistTaps='handled' style={localStyles.container}>
            <View style={localStyles.header}>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.changeImage()}
              >
                <View style={localStyles.boxImage }>
                  <View style={ localStyles.figure }>
                    { photo_url !== '' ? this.renderImage() : null }
                  </View>
                </View>

                <Text style={localStyles.changeImg}>{ I18n.t("account.changeProfile.changeImage")}</Text>
              </TouchableOpacity>
            </View>

            <TopLabelField
              label={ I18n.t("account.changeProfile.fullName.label") }
              placeholder={ I18n.t("account.changeProfile.fullName.placeholder") }
              value={ this.state.name }
              onChangeText={ name => this.setState({name}) }

              returnKeyType="next"
              onSubmitEditing={ () => this.inputPhoneNumber.focus() }
            />

            <View>
              <Label label={ I18n.t("account.changeProfile.mobileNo.label") }/>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 2}}>
                  <Picker
                    data={ dataCC }
                    placeholder={ I18n.t("account.changeProfile.mobileNo.placeholderCC") }
                    onChange={(option) => this.onChangeCC(option.dial_code) }
                    value={ this.state.country_code }
                  />
                </View>
                <View style={{flex: 5, marginLeft: 10}}>
                  <TextField
                    placeholder={ I18n.t("account.changeProfile.mobileNo.placeholderNumber") }
                    returnKeyType="next"
                    value={ this.state.phone }
                    onChangeText={ phone => this.setState({phone}) }
                    inputRef={ node => this.inputPhoneNumber = node }
                    onSubmitEditing={ () => this.inputDrugHistory.focus() }
                  />
                </View>
              </View>
            </View>

            <Picker
              label={ I18n.t("account.changeProfile.timeZone.label") }
              data={ dataTimezone }
              placeholder={ I18n.t("account.changeProfile.timeZone.placeholder") }
              // error={ this.state.timezone.error }
              // errorMes={ I18n.t("signUp.timezone.errorMes") }
              value={ this.state.timezone}
              onChange={(option) => this.onChangeTimezone(option.value) }
            />

            <TopLabelField
              label={ I18n.t("account.changeProfile.desSymptoms.lable") }
              value={ this.state.drug_history }
              editable ={ true }
              multiline={ true }
              numberOfLines={ 5 }
              onChangeText={ drug_history => this.setState({drug_history}) }

              inputRef={ node => this.inputDrugHistory = node }
              returnKeyType="next"
              onSubmitEditing={ () => this.inputMedicalHistory.focus() }
            />

            <TopLabelField
              label={ I18n.t("account.changeProfile.medicalHistory.lable") }
              value={ this.state.medical_history }
              editable ={ true }
              multiline={ true }
              numberOfLines={ 5 }
              onChangeText={ medical_history => this.setState({medical_history}) }

              inputRef={ node => this.inputMedicalHistory = node }
              returnKeyType="next"
              onSubmitEditing={ () => this.inputDrugAllergies.focus() }
            />

            <TopLabelField
              label={ I18n.t("account.changeProfile.drugAllergies.lable") }
              value={ this.state.drug_allergies }
              editable ={ true }
              multiline={ true }
              numberOfLines={ 5 }
              onChangeText={ drug_allergies => this.setState({drug_allergies}) }

              inputRef={ node => this.inputDrugAllergies = node }
              returnKeyType="done"
            />

            { (this.state.errorAPI.error) ? <ErrorMes  mesType = { this.state.errorAPI.mesType } errorMes={ this.state.errorAPI.value } arrow={ false}/> : null}

            <Button
              title={ I18n.t("account.changeProfile.btnUpdate.btnText") }
              buttonStyle={ localStyles.buttonBlue }
              containerViewStyle={ localStyles.button }
              onPress={ this.updateAPIUserInfo }
            />

            <Button
              title={ I18n.t("account.changeProfile.btnCancel.btnText") }
              buttonStyle={ localStyles.buttonGray }
              containerViewStyle={ localStyles.button }
              onPress={() => goBack()}
            />

          </KeyboardAwareScrollView>
        </ScrollView>

        {this.renderLoadingIndicator(this.state.isDataLoading)}
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

export default connect(mapStateToProps, null)(ChangeProfileScreen)
