import {StyleSheet, Dimensions} from 'react-native'
import { MaxWidth, SPACE } from '../../../configs/constants'

const localStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  container: {
    marginLeft: SPACE,
    marginRight: SPACE
  },
  header: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
  },
  boxImage: {
    flexDirection: 'row'
  },
  figure: {
    width: 110,
    height: 110,
    backgroundColor: '#ccc',
    borderRadius: 55,
    overflow: 'hidden',
    marginHorizontal: 10,
  },
  figureImage: {
    width: 110,
    height: 110,
    borderRadius: 55,
    borderWidth: 1,
    borderColor: '#ffffff',
  },
  userImage: {
    resizeMode: 'cover',
  },
  showImg: {
    width: 110,
    height: 110,
    backgroundColor: 'silver',
    borderRadius: 55,
    overflow: 'hidden',
  },
  changeImg: {
    marginTop: 5,
    color: '#35c1cf',
    alignSelf: 'center'
  },
  button: {
    marginLeft: 0, marginRight: 0,
  },
  inputGroup: {
    marginBottom: 15
  }
  ,
  textField: {
    backgroundColor: '#f5f5f5',
    borderColor: '#959595',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 4,
    color: '#555',
    fontSize: 12,
    lineHeight: 20,
    paddingLeft: 15,
    paddingRight: 15,
  }
  ,
  buttonBlue: {
    backgroundColor: '#35c1cf',
    height: 35,
    borderRadius: 4,
    marginBottom: 10,
  },
  buttonGray: {
    backgroundColor: '#bcbcbc',
    height: 35,
    borderRadius: 4,
    marginBottom: 50,
  }
});

/** export module */
module.exports = localStyles;
