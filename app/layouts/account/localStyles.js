import { StyleSheet } from 'react-native'
import { Colors, SPACE } from '../../configs/constants'

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_MAIN_SCREEN
  },
  wrapContent: {
    marginLeft: SPACE,
    marginRight: SPACE,
    marginTop: 15
  },
  wrapTitle: {
    height: 35,
    borderBottomColor: Colors.BACKGROUND_BUTTON_GREY,
    borderBottomWidth: 2,
    paddingVertical: 5,
    marginBottom: 5,
  },
  title: {
    fontSize: 14,
    color: Colors.TEXT_SECONDARY,
  },
  wrapText: {
    height: 35,
    borderBottomColor: Colors.BACKGROUND_BUTTON_GREY,
    borderBottomWidth: 0.5,
    paddingVertical: 5,
    marginBottom: 5,
  },
  text: {
    fontSize: 14,
    color: Colors.TEXT_PRIMARY,
  }
})

/** export module */
module.exports = localStyles
