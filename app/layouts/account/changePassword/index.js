import React from 'react'
import {View, ScrollView, Text, Image, ActivityIndicator, Keyboard} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {Button} from 'react-native-elements'

import {navigate} from 'react-navigation'
import TopLabelField from '../../../components/form/TopLabelField'

import { Label, ErrorMes, TextField } from '../../../components/form/Field'
import globalStyles from '../../../styles'
import localStyles from './localStyles'
import images from '../../../configs/images'
import { APIs, StorageKeys, Colors } from '../../../configs/constants'
import LocalStorage from '../../../components/localStorage'
import GMGClient from '../../../configs/gmgHttpClient'

class ChangePasswordScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: I18n.t('account.changePassword.title').toUpperCase(),
  })
  constructor(props) {
    super(props);
    this.state = {
      switchMesNewPassword: false,
      isDataLoading: false,
      errorAPI: {
        value: '',
        mesType: '',
        error: false
      },
      currentPassword: {
        value: '',
        error: undefined
      },
      newPassword: {
        value: '',
        error: undefined
      },
      retypeNewPassword: {
        value: '',
        error: undefined
      }
    }
  }

  onChangeCurrentPassword = value => {
    if (value === '' || value === undefined) {
      this.setState({currentPassword: {error: true}});
      return false;
    } else {
      this.setState({currentPassword: {value: value, error: false}});
      return true;
    }
  }

  onChangeNewPassword = value => {
    if (value === '' || value === undefined) {
      this.setState({newPassword: {error: true}, switchMesNewPassword: false});
      return false;
    }else if ( value.length < 8) {
      this.setState({newPassword: {error: true}, switchMesNewPassword: true});
      return false;
    }
     else {
      this.setState({newPassword: {value: value, error: false}});
      return true;
    }
  }

  onChangeRetypePass = value => {
    let password = this.state.newPassword.value;
    if (value === '' || value === undefined || value !== password) {
      this.setState({retypeNewPassword: {error: true}});
      return false;
    } else {
      this.setState({retypeNewPassword: {value: value, error: false}});
      return true;
    }
  }

  handleChangePassword = () => {
    let validCurrentPassword = this.onChangeCurrentPassword(this.state.currentPassword.value);
    let validNewPassword = this.onChangeNewPassword(this.state.newPassword.value);
    let validRetypePassword = this.onChangeRetypePass(this.state.retypeNewPassword.value);

    console.log("fdsgsdgewqw");
    console.log(this.state.currentPassword);
    console.log(this.state.newPassword);
    console.log(this.state.retypeNewPassword);
    Keyboard.dismiss()

    this.setState({
      errorAPI: {error: false}
    });

    if (validCurrentPassword && validNewPassword && validRetypePassword) {

      this.callAPIChangePassword();
    }
  }

  callAPIChangePassword = async () => {

    this.setLoadingProgress(true);

    // Get userID from LocalStorage
    let userID = parseInt(await LocalStorage.getString('userID'));
    console.log(userID);

    const bodyData = {
      user_id: userID,
      current_password: this.state.currentPassword.value,
      new_password: this.state.newPassword.value,
    }

    const urlAPI = APIs.CHANGE_PASSWORD + '/' + userID;

    GMGClient.putData(
      urlAPI,
      bodyData,
      json => {
        this.handleChangePasswordAPI(json);
        this.setLoadingProgress(false);
      },
      error => {
        console.log('///////ERROR://///////');
        console.log(error);
        this.setLoadingProgress(false);
      }
    );
  }

  handleChangePasswordAPI = data => {
    switch (data.status_code) {
      case 200: {
        const {navigate} = this.props.navigation;

        //save email login to LocalStorage
        LocalStorage.setString('password', this.state.newPassword.value);
        // navigate('Account');
        this.setState({
          errorAPI: {
            error: true,
            value: 'Success!',
            mesType: 'SUCCESS'
          }
        })
        break;
      }
      case 402: {
        this.setState({ errorAPI: { value: data.message, error: true }});
        break;
      }
      default: {
        this.setState({ errorAPI: { value: data.status_code + '\n' + data.message, error: true }});
        break;
      }
    }
  }

  setLoadingProgress = (isDataLoading) => {
    this.setState({isDataLoading});
  }

  render() {
    const { goBack } = this.props.navigation;
    return (
      <View style={ localStyles.wrapper }>
        <ScrollView keyboardShouldPersistTaps='handled'>
          <View style={ localStyles.container }>
            <TopLabelField
              label={ I18n.t("account.changePassword.currentPassword.label") }
              placeholder={ I18n.t("account.changePassword.currentPassword.placeholder") }
              secureTextEntry={ true }
              error={ this.state.currentPassword.error }
              errorMes={ I18n.t("account.changePassword.currentPassword.errorMes") }
              returnKeyType="next"
              onSubmitEditing={ () => this.inputNewPass.focus() }
              onChangeText={ value => this.onChangeCurrentPassword(value) }
            />
            <TopLabelField
              label={ I18n.t("account.changePassword.newPassword.label") }
              placeholder={ I18n.t("account.changePassword.newPassword.placeholder") }
              secureTextEntry={ true }
              error={ this.state.newPassword.error }
              errorMes={ this.state.switchMesNewPassword ? I18n.t("account.changePassword.newPassword.switchMesNewPassword") : I18n.t("account.changePassword.newPassword.errorMes") }
              inputRef={ node => this.inputNewPass = node }
              returnKeyType="next"
              onSubmitEditing={ () => this.inputReTypePass.focus() }
              onChangeText={ value => this.onChangeNewPassword(value) }
            />
            <TopLabelField
              label={ I18n.t("account.changePassword.retypeNewPassword.label") }
              placeholder={ I18n.t("account.changePassword.retypeNewPassword.placeholder") }
              secureTextEntry={ true }
              error={ this.state.retypeNewPassword.error }
              errorMes={ I18n.t("account.changePassword.retypeNewPassword.errorMes") }
              inputRef={ node => this.inputReTypePass = node }
              returnKeyType="done"
              onChangeText={ value => this.onChangeRetypePass(value) }
            />

            { (this.state.errorAPI.error) ? <ErrorMes mesType = { this.state.errorAPI.mesType } errorMes={ this.state.errorAPI.value } arrow={ false} /> : null}

            <Button
              title={ I18n.t("account.changePassword.btnChangePassword") }
              onPress={ this.handleChangePassword }
              buttonStyle={ localStyles.buttonChangePassword }
              containerViewStyle={ localStyles.button }
            />
            <Button
              title={ I18n.t("account.changePassword.btnCancel") }
              onPress={ () => goBack() }
              buttonStyle={ localStyles.buttonCancel }
              containerViewStyle={ localStyles.button }
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

export default connect(mapStateToProps, null)(ChangePasswordScreen)
