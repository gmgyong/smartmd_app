import {StyleSheet, Dimensions} from 'react-native'
import { MaxWidth, SPACE } from '../../../configs/constants'

const localStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#FFF"
  },
  container: {
    // marginLeft: 30,
    // marginRight: 30,
    marginTop: 15,
    marginLeft: SPACE, marginRight: SPACE,
    // marginTop: -150,
  },
  button: {
    marginLeft: 0, marginRight: 0,
  },
  buttonChangePassword: {
    backgroundColor: '#35c1cf',
    height: 35,
    borderRadius: 4,
    marginBottom: 15,
    marginTop: 10
  },
  buttonCancel: {
    backgroundColor: '#bcbcbc',
    height: 35,
    borderRadius: 4,
  },
  wrapTextInput: {
    marginBottom: 10
  }
});

/** export module */
module.exports = localStyles;
