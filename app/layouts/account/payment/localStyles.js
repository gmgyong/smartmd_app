import {StyleSheet, Dimensions} from 'react-native'
import { MaxWidth, SPACE } from '../../../configs/constants'


const localStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  container: {
    marginLeft: SPACE,
    marginRight: SPACE,
    marginTop: 15
  },
  button: {
    marginLeft: 0, marginRight: 0,
    height: 35,
    borderRadius: 4,
    marginBottom: 10,
  },
  buttonBlue: {
    backgroundColor: '#35c1cf',
    height: 35,
    borderRadius: 4,
    marginBottom: 10,
  },
  buttonGray: {
    backgroundColor: '#bcbcbc',
    height: 35,
    borderRadius: 4,
    marginBottom: 10,
  }
});

/** export module */
module.exports = localStyles;
