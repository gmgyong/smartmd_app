import React from 'react'
import {View, Text, TextInput, ScrollView, ActivityIndicator} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {Button} from 'react-native-elements'

import {navigate} from 'react-navigation'
import  TopLabelField from '../../../components/form/TopLabelField'

import { Label, ErrorMes, TextField } from '../../../components/form/Field'
import globalStyles from '../../../styles'
import localStyles from './localStyles'

import { APIs, StorageKeys, Colors } from '../../../configs/constants'
import LocalStorage from '../../../components/localStorage'
import GMGClient from '../../../configs/gmgHttpClient'

class PaymentScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: I18n.t('account.payment.title').toUpperCase(),
  })

  constructor(props) {
    super(props);
      this.state = {
        updateCreditCard: false,
        previous: {
          name: {
            value: '',
            error: false,
          },
          number: {
            value: '',
            error: false,
          },
          expiration: {
            value: '',
            error: false,
          },
          CVV: {
            value: '',
            error: false
          }
        },
        name: {
          value: '',
          error: false,
        },
        number: {
          value: '',
          error: false,
        },
        expiration: {
          value: '',
          error: false,
        },
        CVV: {
          value: '',
          error: false
        },
        errorAPI: {
          value: '',
          error: false,
          mesType: 'SUCCESS'
        }
      }
  }

  onGetInfoCreditCard = async () => {
    console.log("GET INFO PAYMENT")
    this.setLoadingProgress(true);
    const userId = await LocalStorage.getString(StorageKeys.USERID)
    const urlAPI = APIs.CREDIT_CARD + '/' + userId + "?"
    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          this.setLoadingProgress(false);
          this.setState({
            previous:{
              name: {
                value: json.data.card_name,
                error: false
              },
              number: {
                value: "****-****-****-" + json.data.last4,
                error: false
              },
              expiration: {
                value: json.data.expiry,
                error: false
              },
              CVV: {
                value: "***",
                error: false
              }
            }
          })
        }
      },
      error => {
        console.log("ERROR OF STRIPE API")
      }
    )
  }


  onUpdateCreditCard = async () => {
    this.setLoadingProgress(true);
    const userId = await LocalStorage.getString(StorageKeys.USERID)
    const state = this.state

    let urlAPI = APIs.CREDIT_CARD + '/' + userId
    let bodyData = {
      "card_name": state.name.value,
      "card_number": state.number.value,
      "expiry": state.expiration.value,
      "cvv": state.CVV.value
    }

    console.log(bodyData)
    console.log(this.state)

    GMGClient.postData(
      urlAPI,
      bodyData,
      (json) => {
        if(!json.success) {
          this.setState({
            errorAPI: {
              error: true,
              value: json.status_code + json.message,
              mesType: 'ERROR'
            }
          })
          this.setLoadingProgress(false);
        } else {
          this.setState({
            previous:{
              name: {
                value: json.data.card_name,
                error: false
              },
                number: {
                  value: "****-****-****-" + json.data.last4,
                  error: false
                },
                expiration: {
                  value: json.data.expiry,
                  error: false
                },
                CVV: {
                  value: "***",
                  error: false
                }
              },
              errorAPI: {
                error: true,
                value: 'Success!',
                mesType: 'SUCCESS'
              }
          })
          this.setLoadingProgress(false);
        }
      },
      (error) => {
        this.setState({
          errorAPI: {
            error: true,
            value: "Please check info credit card",
            mesType: 'ERROR'
          }
        })
        this.setLoadingProgress(false);
      }
    )
  }

  handleUpdateCreditCard = () => {
    let checkValidate = this.validation()

    this.setState({
      errorAPI: { error: false }
    })

    if (checkValidate) {
      this.onUpdateCreditCard()
    }
  }

  validation = () => {
    let check = true;

    if( this.state.name.value === '' || this.state.name.value === undefined ) {
      this.setState({ name: { error: true, value: this.state.name.value }});
      check = false;
    } else {
      this.setState({ name: { error: false, value: this.state.name.value }});
    }

    if( this.state.number.value === '' || this.state.number.value === undefined || this.state.number.value.length < 19 ) {
      this.setState({ number: { error: true, value: this.state.number.value }});
      check = false;
    } else {
      this.setState({ number: { error: false, value: this.state.number.value }});
    }

    if( this.state.expiration.value === '' || this.state.expiration.value === undefined || this.state.expiration.value.length < 7 ) {
      this.setState({ expiration: { error: true, value: this.state.expiration.value }});
      check = false;
    } else {
      this.setState({ expiration: { error: false, value: this.state.expiration.value }});
    }

    if( this.state.CVV.value === '' || this.state.CVV.value === undefined || this.state.CVV.value.length < 3 ) {
      this.setState({ CVV: { error: true, value: this.state.CVV.value }});
      check = false;
    } else {
      this.setState({ CVV: { error: false, value: this.state.CVV.value }});
    }

    return check;
  }

  handleCheckValue = (value, state) => {
    let error = value === ""

    switch(state) {
      case 'name':
        this.setState({
          name: {
            value,
            error
          }
        })
        break
      case 'number':
        value = value.split("-").join("").slice(0,16).replace(/\D/g,"")

        if (value.length > 0) {
          value = value.match(/\d{1,4}/g).join("-")
        }

        this.setState({
          number: {
            value,
            error
          }
        })
        break
      case 'expiration':
        value = this.updateFormatExpiration(value)

        this.setState({
          expiration: {
            value,
            error
          }
        })
        break
      case 'CVV':
        value = value.split("/").join("").slice(0,3).replace(/\D/g,"")

        this.setState({
          CVV: {
            value,
            error
          }
        })
        break
    }
  }

  updateFormatExpiration = (value) => {
    value = value.split("/").join("").slice(0,6).replace(/\D/g,"")
    if (value.length > 0) {
      value = value.match(new RegExp('.{1,4}$|.{1,2}', 'g')).join("/")
    }

    return value
  }

  setLoadingProgress = (isDataLoading) => {
    this.setState({isDataLoading});
  }

  renderLoadingIndicator = (isShowing) => {
    if (isShowing) {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={globalStyles.indicatorPageLoading}
          color={Colors.TEXT_SECONDARY}
          size='small'/>
      )
    } else {
      return null;
    }
  }


  componentWillMount() {
    this.onGetInfoCreditCard();
  }

  componentDidMount () {

  }
  confirmUpdate = () => {
    this.setState({
      updateCreditCard : true,
      name: {
        value: '',
        error: false,
      },
      number: {
        value: '',
        error: false,
      },
      expiration: {
        value: '',
        error: false,
      },
      CVV: {
        value: '',
        error: false
      },
      errorAPI: {
        value: '',
        error: false,
        mesType: 'SUCCESS'
      }
    })
  }

  showCreditCardInfo = () => {
    const { goBack } = this.props.navigation;
    return (
      <View style={{ marginTop: 5, }}>
        <Label label={ I18n.t("appointments.make.stepThree.card.name.title") + ' *' } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText, { paddingBottom: 10 }] }>
          {this.state.previous.name.value}
        </Text>
        <Label label={ I18n.t("appointments.make.stepThree.card.number.title") + ' *' } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText, { paddingBottom: 10 }] }>
          {this.state.previous.number.value}
        </Text>
        <Label label={ I18n.t("appointments.make.stepThree.card.validAndCVV.title") + ' *' } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText, { paddingBottom: 10 }] }>
          {this.state.previous.expiration.value}  -  {this.state.previous.CVV.value}
        </Text>

        <Button
          title= { I18n.t("buttons.btnUpdateCreditCard").toUpperCase() }
          buttonStyle={ localStyles.buttonBlue }
          containerViewStyle={ localStyles.button }
          onPress= {this.confirmUpdate}
          />

        <Button
          title={ I18n.t("account.payment.btnBack.btnText") }
          buttonStyle={ localStyles.buttonGray }
          containerViewStyle={ localStyles.button }
          onPress={ () => goBack() }
        />

      </View>
    )
  }

  genUpdateCreditCard = () => {
    const { goBack } = this.props.navigation;
    return(
      <View>
      <TopLabelField
        label={ I18n.t("account.payment.cardName.label") }
        placeholder={ I18n.t("account.payment.cardName.placeholder") }
        error={ this.state.name.error }
        errorMes={ I18n.t("appointments.make.stepThree.card.name.errorMes") }
        value= {this.state.name.value}
        onChangeText={ (value) => this.handleCheckValue(value, 'name') }
        returnKeyType="next"
        onSubmitEditing={ () => this.inputNumber.focus() }
      />
      <TopLabelField
        label={ I18n.t("account.payment.cardNumber.label") }
        placeholder={ I18n.t("account.payment.cardNumber.placeholder") }
        error={ this.state.number.error }
        errorMes={ I18n.t("appointments.make.stepThree.card.number.errorMes") }
        value= {this.state.number.value}
        onChangeText={ (value) => this.handleCheckValue(value, 'number') }
        returnKeyType="next"
        inputRef={ node => this.inputNumber = node }
        onSubmitEditing={ () => this.inputExpiration.focus() }
      />
      <View style={{marginBottom: 20}}>
        <Label label={ I18n.t("account.payment.validCvv.lable") } error={ this.state.expiration.error || this.state.CVV.error } />
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 3}}>
            <TextField
              placeholder={ I18n.t("account.payment.validCvv.placeholderDate") }
              error={ this.state.expiration.error }
              value= {this.state.expiration.value}
              onChangeText={ (value) => this.handleCheckValue(value, 'expiration') }
              returnKeyType="next"
              inputRef={ node => this.inputExpiration = node }
              onSubmitEditing={ () => this.inputCvv.focus() }
            />
          </View>
          <View style={{flex: 2, marginLeft: 10}}>
            <TextField
              placeholder={ I18n.t("account.payment.validCvv.placeholderCvv") }
              error={ this.state.CVV.error }
              value= {this.state.CVV.value}
              onChangeText={ (value) => this.handleCheckValue(value, 'CVV') }
              returnKeyType="done"
              inputRef={ node => this.inputCvv = node }
            />
          </View>
          <View style={{flex: 3, marginLeft: 10}}>
          </View>
        </View>
        { (this.state.expiration.error || this.state.CVV.error) ? <ErrorMes errorMes={ I18n.t("appointments.make.stepThree.card.validAndCVV.errorMes") } styleContainer={{marginTop: 15, marginBottom: 0}} /> : null }
      </View>
      { (this.state.errorAPI.error) ? <ErrorMes  mesType = { this.state.errorAPI.mesType } errorMes={ this.state.errorAPI.value } arrow={ false}/> : null}

      <Button
        title={ I18n.t("account.payment.btnUpdate.btnText") }
        buttonStyle={ localStyles.buttonBlue }
        containerViewStyle={ localStyles.button }
        onPress={this.handleUpdateCreditCard }
      />
      { (this.state.previous.name.value !=='') ?
      <Button
        title={ I18n.t("buttons.btnCancel").toUpperCase() }
        buttonStyle={ localStyles.buttonGray }
        containerViewStyle={ localStyles.button }
        onPress={ () => this.setState({updateCreditCard : false}) }
      /> : null }
      <Button
        title={ I18n.t("account.payment.btnBack.btnText") }
        buttonStyle={ localStyles.buttonGray }
        containerViewStyle={ localStyles.button }
        onPress={ () => goBack() }
      />
      </View>
    )
  }

  render(){

    return(
      <View style={localStyles.wrapper}>
        <ScrollView keyboardShouldPersistTaps='handled'>
          <View style={localStyles.container}>
            { this.state.updateCreditCard ? this.genUpdateCreditCard() : (this.state.previous.name.value !=='' ? this.showCreditCardInfo() : this.genUpdateCreditCard()) }
          </View>
        </ScrollView>
        {this.renderLoadingIndicator(this.state.isDataLoading)}
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

export default connect(mapStateToProps, null)(PaymentScreen)
