import React, { Component, PropTypes } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import { navigate } from 'react-navigation'

import LocalStorage from '../../components/localStorage'
import GMGClient from '../../configs/gmgHttpClient'
import ListBillings from '../../components/listView/billings'
import { Colors, StorageKeys, APIs } from '../../configs/constants'
import Icons from '../../configs/icons'
import localStyles from './localStyles'
import globalStyles from '../../styles'

class BillingScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isDataLoading: false,
      userId: '',
      listInvoices: [],
    }
  }

  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarIcon: Icons.Billing,
    tabBarPosition: 'top'
  })

  async componentDidMount() {
    const userId = await LocalStorage.getString(StorageKeys.USERID)
    this.setState({
      userId,
    })
    this.getListInvoices()
  }

  getListInvoices = () => {
    const urlAPI = APIs.LIST_INVOICES
      + "?filter_by_user_id=" + this.state.userId

    this.setState({ isDataLoading: true })

    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          // console.log('json.data: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', json.data)
          this.setState(
            {
              isDataLoading: false,
              listInvoices: json.data
            }
          )
        }
      },
      error => {
        this.setState({
          isDataLoading: false,
        })
        alert(error)
      }
    )
  }

  renderListInvoice = () => (
    <View>
      <Text style={localStyles.title}>
        { I18n.t('billing.title').toUpperCase() }
      </Text>
      <ListBillings
        navigation = { this.props.navigation }
        listRef='billingsList'
        listInvoices = {this.state.listInvoices} />
    </View>
  )

  render() {
    const {language} = this.props
    const {navigate, setParams} = this.props.navigation
    return (
      <View style={ localStyles.wrapper }>
        <ScrollView>
          { this.renderListInvoice() }
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

export default connect(mapStateToProps, null)(BillingScreen)
