import React from 'react'
import {View, ActivityIndicator, ScrollView, Image, Text} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'

import LocalStorage from '../../components/localStorage'
import GMGClient from '../../configs/gmgHttpClient'
import images from '../../configs/images'
import { Colors, StorageKeys, APIs, FontSizes } from '../../configs/constants'
import Icons from '../../configs/icons'
import localStyles from './localStyles'
import globalStyles from '../../styles'

class BillingDetailPatientScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isDataLoading: true,
      invoiceDetail: {}
    }
  }

  static navigationOptions = ({navigation}) => ({
    title: I18n.t('billing.detail.title')
  })

  componentDidMount() {
    this.handleGetBillingAPI()
  }

  handleGetBillingAPI = () => {
    const {params} = this.props.navigation.state
    console.log(params)

    let urlAPI = APIs.LIST_INVOICES + '/' + params.invoice_id + '?'
    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          this.setState(
            {
              isDataLoading: false,
              invoiceDetail: json.data
            }
          )
        }
      },
      error => {
        this.setState({
          isDataLoading: false,
        })
        alert(error)
      }
    )
  }

  renderLoadingIndicator = () => {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={[globalStyles.indicatorPageLoading, {backgroundColor: '#FFF'}]}
          color={Colors.TEXT_SECONDARY}
          size='large'
        />
      )
  }

  renderDescriptions = () => {
    let {invoiceDetail} = this.state

    return (
      <View style={localStyles.borderNormal}>
        {invoiceDetail.invoiceLineItems.map((item, index) => {
          if(item.after_items !== 1) {
            return (
              <View key={index} style={[globalStyles.blockInline, {paddingBottom: 10,}]}>
                <View style={{flex: 1}}>
                  <Text>
                    {item.description}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <Text style={{textAlign: 'right',}}>
                    {item.subtotal} {invoiceDetail.currency}
                  </Text>
                </View>
              </View>
            )
          }
        })}
      </View>
    )
  }

  renderContainer = () => {
    let {invoiceDetail} = this.state
    let GST = {}

    invoiceDetail.invoiceLineItems.map((item) => {
      if(item.after_items === 1) {
        GST = item
      }
    })

    if (typeof invoiceDetail !== "undefined" || invoiceDetail !== null) {

      return (
        <ScrollView>
          <View style={localStyles.container}>
            <View style={[globalStyles.blockInline, {marginBottom: 10,}]}>
              <View>
                <Image source={images.logo} style={{width: 72, height: 72}} />
              </View>
              <View style={{flex: 1}}></View>
              <View style={{justifyContent: 'center'}}>
                <Text style={localStyles.titleBillingDetail}>
                  {I18n.t('billing.detail.title').toUpperCase()}
                </Text>
              </View>
            </View>
            <View style={[globalStyles.blockInline, {marginBottom: 3,}]}>
              <View style={{flex: 2}}>
                <Text>
                  {I18n.t('billing.detail.invoiceTo').toUpperCase()}:
                </Text>
              </View>
              <View style={{flex: 3}}>
                <Text>
                  {invoiceDetail.invoice_to.toUpperCase()}
                </Text>
              </View>
            </View>
            <View style={[globalStyles.blockInline, {marginBottom: 3,}]}>
              <View style={{flex: 2}}>
                <Text>
                  {I18n.t('billing.invoiceNo').toUpperCase()}:
                </Text>
              </View>
              <View style={{flex: 3}}>
                <Text>
                  {invoiceDetail.invoice_no.toUpperCase()}
                </Text>
              </View>
            </View>
            <View style={[globalStyles.blockInline, {marginBottom: 20,}]}>
              <View style={{flex: 2}}>
                <Text>
                  {I18n.t('billing.detail.invoiceDate').toUpperCase()}:
                </Text>
              </View>
              <View style={{flex: 3}}>
                <Text>
                  {invoiceDetail.invoice_date.toUpperCase()}
                </Text>
              </View>
            </View>
            <View style={[globalStyles.blockInline, localStyles.borderBold]}>
              <View>
                <Text style={{color: Colors.TEXT_SECONDARY, fontSize: FontSizes.BIG}}>
                  {I18n.t('billing.detail.description').toUpperCase()}
                </Text>
              </View>
              <View style={{flex: 1}}></View>
              <View>
                <Text style={{color: Colors.TEXT_SECONDARY, fontSize: FontSizes.BIG, textAlign: 'right',}}>
                  {I18n.t('billing.detail.subtotal').toUpperCase()}
                </Text>
              </View>
            </View>

            {this.renderDescriptions()}

            <View style={[globalStyles.blockInline,]}>
              <View>
                <Text style={{color: Colors.TEXT_SECONDARY, fontSize: FontSizes.BIG}}>
                  {I18n.t('billing.detail.subtotal').toUpperCase()}
                </Text>
              </View>
              <View style={{flex: 1}}></View>
              <View>
                <Text style={{fontSize: FontSizes.BIG, textAlign: 'right',}}>
                  {invoiceDetail.subtotal} {invoiceDetail.currency}
                </Text>
              </View>
            </View>
            <View style={[globalStyles.blockInline,]}>
              <View>
                <Text style={{color: Colors.TEXT_SECONDARY, fontSize: FontSizes.BIG}}>
                  {GST.description}
                </Text>
              </View>
              <View style={{flex: 1}}></View>
              <View>
                <Text style={{fontSize: FontSizes.BIG, textAlign: 'right',}}>
                  {GST.subtotal} {invoiceDetail.currency}
                </Text>
              </View>
            </View>
            <View style={localStyles.blockTotal}>
              <Text style={{fontSize: FontSizes.NORMAL, color: Colors.TEXT_TABS_ICON, textAlign: 'right'}}>
                {I18n.t('billing.detail.total').toUpperCase()}
              </Text>
              <Text style={{fontSize: FontSizes.SUPER_HUGE, color: Colors.TEXT_TABS_ICON, textAlign: 'right'}}>
                {invoiceDetail.total} {invoiceDetail.currency}
              </Text>
            </View>
            <Text style={{fontSize: FontSizes.BIG}}>
              {I18n.t('billing.detail.terms')}
            </Text>
          </View>
        </ScrollView>
      )
    } else {
      return (<View/>)
    }


  }

  render() {
    return (
      <View style={ localStyles.wrapper }>
        { this.state.isDataLoading ? this.renderLoadingIndicator() : this.renderContainer() }
      </View>
    )
  }
}

const mapStateToProps = state => ({
  language: state.settings.language
})

export default connect(mapStateToProps, null)(BillingDetailPatientScreen)
