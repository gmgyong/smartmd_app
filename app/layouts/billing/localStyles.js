import { StyleSheet } from 'react-native'
import { Colors, FontSizes } from '../../configs/constants'

const localStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: Colors.BACKGROUND_FIELD,
  },
  title: {
    paddingTop: 20,
    paddingLeft: 10,
    color: Colors.TEXT_SECONDARY,
  },
  titleBillingDetail: {
    color: Colors.TEXT_SECONDARY,
    fontWeight: 'bold',
    textAlign: 'right',
    fontSize: FontSizes.LARGE,
  },
  borderNormal: {
    borderBottomWidth: 1,
    borderColor: Colors.BORDER,
    paddingBottom: 5,
    marginBottom: 10,
  },
  borderBold: {
    borderBottomWidth: 2,
    borderColor: Colors.BORDER,
    paddingBottom: 5,
    marginBottom: 10,
  },
  blockTotal: {
    backgroundColor: Colors.BACKGROUND_STACKS,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 20,
    marginBottom: 20,
  },
})

/** export module */
module.exports = localStyles
