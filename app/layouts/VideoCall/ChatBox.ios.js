import React from 'react'
import { View, Text, StyleSheet, Dimensions, Alert, ScrollView, TouchableOpacity, TextInput } from 'react-native'
import { Icon } from 'react-native-elements'
import attachFile from './attachfile'
import Communications from 'react-native-communications'
import I18n from 'react-native-i18n'
import {connect} from 'react-redux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const dimensions = Dimensions.get('window')

class ChatBox extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      text: ""
    }
  }

  renderMessage = (data) => {
    return data.map( (item, index) => {
     switch (item.type) {
       case "theirs": {
         return (
           <View style={ styles.subscriberMes } key={index}>
             {/*<View style={ styles.figureAvatar }>*/}
               {/*<Image*/}
                 {/*style={ styles.avatar }*/}
                 {/*source={{uri: 'https://facebook.github.io/react/img/logo_og.png'}}*/}
               {/*/>*/}
             {/*</View>*/}
             <View style={ styles.figureMes }>
               <View style={ styles.eachMes }>
                 <Text style={ styles.subMes }>{ item.message }</Text>
               </View>
             </View>
           </View>
         )
       }
       case 'mine':
         return (
           <View style={ styles.publisherMes } key={index}>
             <View style={ styles.figureMes }>
               <View style={ [styles.eachMes, styles.eachMesPublisher] }>
                 {/*<Text style={ styles.pubMes }>{ item.message }</Text>*/}
                 {item.typeMes === 'chat'
                   ? <Text style={ styles.pubMes }>{ item.message }</Text>
                   : <TouchableOpacity onPress={()=> this.handlePressLink(item.url)}>
                     <Text style={ styles.pubMes }>
                       Upload file: <Text style={{color: '#FFF'}}>{ item.name }</Text>
                     </Text>
                   </TouchableOpacity>
                 }
               </View>
             </View>
           </View>
         )
       default:
         return null
     }
    })
  }

  handlePressLink = (url) => {
    Alert.alert(
      I18n.t('videoCall.titleAlertOpenLink'),
      I18n.t('videoCall.bodyAlertOpenLink'),
      [
        {text: I18n.t('videoCall.btnCancelAlertOpenLink'), onPress: () => console.log("Cancel open link") },
        {text: I18n.t('videoCall.btnOpenAlertOpenLink'), onPress: () => Communications.web(url)},
      ],
      { cancelable: false }
    )
  }

  handlePressEnter = () => {
    let text = this.state.text.trim()
    console.log("text: ", text)
    this.setState({text:""})

    if(text !== "") {
      this.props.handlePressEnterChat(text)
    }
  }

  render() {

    const { onPressBack, data, userId, appointment_request_id, handlePressEnterChat } = this.props

    return(
      <View style={ styles.container }>
        <View style={ styles.containerHeader }>
          <Icon
            name='keyboard-arrow-left'
            color='#FFF'
            size={30}
            onPress={ onPressBack }
          />
          <Text style={ styles.textHeader }>CHAT</Text>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps='handled' style={{flex: 1}}>
          <ScrollView
            style={ styles.containerMessages }
            ref={ node => this.scrollView = node }
            onContentSizeChange={() => {
              this.viewRenderMessage.measure(
                (ox, oy, width, height) => {
                  console.log('height content chat: ' + height + ' ' + dimensions.height)
                  if(height > (dimensions.height - 150)) {
                    this.scrollView.scrollToEnd({animated: true})
                  }
                }
              )
            }}
          >
            <View ref={ node => this.viewRenderMessage = node }>
              { this.renderMessage(data) }
            </View>
          </ScrollView>
          <View style={ styles.containerChat }>
            <Icon
              name='attachment'
              color='#bcbcbc'
              size={30}
              onPress={() => attachFile(
                userId,
                appointment_request_id,
                text => handlePressEnterChat(text,'uploadFile')
              )}
            />
            <TextInput
              style={ styles.inputChat }
              value={this.state.text}
              onChangeText={ text => this.setState({text})}
              returnKeyType="send"
              onSubmitEditing={ this.handlePressEnter }
            />
            <Icon
              name='send'
              color='#35c1cf'
              size={30}
              onPress={ this.handlePressEnter }
            />
          </View>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    position: 'absolute',
    zIndex: 990,
    width: dimensions.width,
    height: dimensions.height
  },
  containerMessages: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: '#FFF'
  },
  containerHeader: {
    height: 60,
    backgroundColor: '#35c1cf',
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 10,
  },
  containerChat: {
    height: 70,
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    flexDirection: 'row'
  },
  subscriberMes: {
    marginVertical: 5,
    paddingRight: dimensions.width*0.3,
    flexDirection: 'row'
  },
  publisherMes: {
    marginVertical: 5,
    paddingLeft: dimensions.width*0.3,
  },
  figureAvatar: {
    marginRight: 10,
    width: 30, height: 30,
    borderRadius: 15,
    backgroundColor: '#ccc',
    overflow: 'hidden',
  },
  figureMes: {
    flexDirection: 'column',
  },
  avatar: {
    width: 30, height: 30,
    borderRadius: 15,
  },
  eachMes: {
    flexDirection: 'row',
  },
  eachMesPublisher: {
    justifyContent: 'flex-end',
  },
  subMes: {
    backgroundColor:'#d7d7d7',
    fontSize: 12,
    lineHeight: 20,
    paddingTop: 3,
    paddingBottom: 7,
    paddingHorizontal: 10,
    marginBottom: 2,
    borderRadius: 15,
    overflow: 'hidden',
  },
  pubMes: {
    backgroundColor:'#35c1cf',
    color: '#fff',
    fontSize: 12,
    lineHeight: 20,
    paddingTop: 3,
    paddingBottom: 7,
    paddingHorizontal: 10,
    marginBottom: 2,
    borderRadius: 15,
    overflow: 'hidden',
  },
  inputChat: {
    backgroundColor: '#FFF',
    height: 40,
    borderRadius: 20,
    borderColor: '#ccc',
    borderWidth: 1,
    flex: 1,
    marginHorizontal: 10,
    paddingHorizontal: 20,
  },
  textHeader: {
    fontSize: 18,
    lineHeight: 26,
    color: '#FFF',
    marginHorizontal: 10,
    fontWeight: 'bold'
  }
})

export default connect()(ChatBox)