import React from 'react'
import {View, Text, StyleSheet, Dimensions, ActivityIndicator, Modal, Alert} from 'react-native'
import {Icon, Button} from 'react-native-elements'
import PropTypes from 'prop-types'

import ChatBox from './ChatBox'
import {PublisherView, SubscriberView, Session} from 'react-native-opentok'
import globalStyles from '../../styles'
import GMGClient from '../../configs/gmgHttpClient'
import { APIs, Colors } from '../../configs/constants'
import I18n from 'react-native-i18n'
import {connect} from 'react-redux'
import KeepAwake from 'react-native-keep-awake'

const dimensions = Dimensions.get('window')

let intervalCheckClientConnect

class VideoCallScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      status: 'WAITING', //WAITING, CALLING, ENDCALL
      isFetching: true,
      isBoxTool: true,
      isChatBox: false,
      isChatButton: true,
      isEndButton: false,
      isPublisher: true,
      isSubscriber: false,
      isClientConnect: false,
      isAvailableTokbox: true,
      publisherType: 'SMALL', //LARGE, SMALL, HIDDEN
      subscriberType: 'HIDDEN', //LARGE, SMALL, HIDDEN
      dataMessage: [],
      chatlog: [],
      user_id: '',
      tokbox: {
        opentok_api_key: "",
        tokbox_session_id: "",
        opentok_token: ""

      },
    }
  }

  static navigationOptions = ({navigation}) => ({
    header: null,
    title: "CONSULTATION"
  })

  onPublishStart = () => {
    console.log('publisher start')
    this.setState({
        publisherType: 'LARGE',
        isSubscriber: true,
      },
      () => console.log(this.state))
  }

  onSubscribeStart = () => {
    console.log('subscriber start')
    this.setState({
        publisherType: 'LARGE',
        subscriberType: 'LARGE',
      },
      () => setTimeout(() => this.setState({publisherType: 'SMALL'}), 500))
  }

  onSubscribeStop = () => {
    console.log('subscriber stop')

    //stop get heartbeat
    clearInterval(intervalCheckClientConnect)

    //disconnect chat
    Session.disconnect()

    //check room available
    this.getAPIcheckTokboxAvailable()
    this.setState({
      publisherType: 'HIDDEN',
      subscriberType: 'HIDDEN',
      isEndButton: true,
      isBoxTool: true,
      status: 'ENDCALL'
    })

  }

  handleOpenChatBox = () => {
    this.setState({
        isChatBox: true
      },
      () => console.log(this.state))
  }

  handleCloseChatBox = () => {
    this.setState({
      isChatBox: false
    })
  }

  handleEndCall = () => {
    console.log('press end call')
    this.props.navigation.goBack()
  }

  handleStyleContainerPublisher = () => {
    switch (this.state.publisherType) {
      case 'LARGE':
        return styles.wrapperLarge
      case 'SMALL':
        return styles.wrapperSmall
      case 'HIDDEN':
        return styles.wrapperHidden
      default:
        return styles.wrapperLarge
    }
  }

  handleStyleContainerSubscriber = () => {
    switch (this.state.subscriberType) {
      case 'LARGE':
        return styles.wrapperLarge
      case 'SMALL':
        return styles.wrapperSmall
      case 'HIDDEN':
        return styles.wrapperHidden
      default:
        return styles.wrapperLarge
    }
  }

  handlePressEnterChat = async (message, type="chat") => {

    let data = {
      text: message,
      user_id: this.state.user_id
    }

    let dataToString = await JSON.stringify(data)

    //send message to opentok
    Session.sendMessage(dataToString, type)

    //send message to server(save log)
    const urlAPI =
      APIs.TOKBOX
      + '/' + this.props.navigation.state.params.appointment_request_id
      + '/consultation/chat'

    GMGClient.postData(
      urlAPI,
      data,
      json => {
        console.log("Save log chat: " + message)
      },
      error => {
        alert(error)
      }
    )
  }

  handleGetUploadFile = text => {

    // get all index of char {"} in message
    let listChar1 = []
    let listChar2 = []
    let lengthMes = text.length

    for (let i = 0; i < lengthMes; i++) {
      text[i] === '"' && listChar1.push(i)
      text[i] === '>' && listChar2.push(i)
    }

    let url = text.slice((listChar1[2] +1), listChar1[3])
    let name = text.slice((listChar2[0] +1), (lengthMes - 4))
    // console.log(url, name)
    return (
      {
        url,
        name
      }
    )
  }

  getAPICheckHeartbeat = () => {
    const {params} = this.props.navigation.state
    const urlAPI = APIs.TOKBOX
      + "/" + params.appointment_request_id
      + "/consultation/heartbeat"
    const bodyData = {
      "user_id" : params.user_id
    }

    intervalCheckClientConnect = setInterval(() => {
      GMGClient.postData(
        urlAPI,
        bodyData,
        json => {
          if(json.success) {
            if(json.data.ready) {
              if(this.state.status === 'WAITING') {
                this.setState({
                  isClientConnect: true,
                  status: 'CALLING'
                })

                this.getAPIChatLog()
                this.onConnectChat()
              } else {
                this.setState({
                  isClientConnect: true,
                })
              }

              //stop sent api heartbeat
              // clearInterval(intervalCheckClientConnect)
            } else {
              this.setState({isClientConnect: false})
            }
          }
        },
        error => {
          // alert(error)
        }
      )
    }, 3000)

  }

  getAPITokboxInfo = (getHeartbeat = true) => {
    const {params} = this.props.navigation.state
    const urlAPI = APIs.TOKBOX
      + "/" + params.appointment_request_id
      + "/consultation"
      + "?user_id=" + params.user_id

    this.setState({ isFetching: true })

    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          this.setState({
              isFetching: false,
              tokbox: {
                opentok_api_key: json.data.opentok_api_key,
                tokbox_session_id: json.data.tokbox_session_id,
                opentok_token: json.data.opentok_token
              }
            },
            () => { getHeartbeat && this.getAPICheckHeartbeat()})
        } else {
          this.props.navigation.goBack()
          alert(I18n.t('videoCall.mesErrorExistRoom'))
        }
      },
      error => {
        this.setState({ isFetching: false })
      }
    )
  }

  getAPIcheckTokboxAvailable = () => {
    const {params} = this.props.navigation.state
    const urlAPI = APIs.TOKBOX
      + "/" + params.appointment_request_id
      + "/consultation"
      + "?user_id=" + params.user_id

    this.setState({ isFetching: true })
    GMGClient.getData(
      urlAPI,
      json => {
        if(json.status_code === 200) {
          this.setState({isAvailableTokbox: true, isFetching: false})
        } else {
          this.setState({isAvailableTokbox: false, isFetching: false})
        }
      },
      error => {
        this.setState({ isFetching: false })
      }
    )

  }

  getAPIChatLog = () => {
    const urlAPI =
      APIs.APPOINTMENT_INFO
      + '?user_id=' + this.props.navigation.state.params.user_id
      + '&appointment_id=' + this.props.navigation.state.params.appointment_id

    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          // this.setState({chatlog: json.data.chatlog})
          return json.data.chatlog.map( async chat => {
            if(chat.text.indexOf('<a target="_blank" ') === -1) {
              //type = chat
              if(chat.user_id === this.state.user_id) {
                return this.setState( prevState => ({
                  dataMessage: [
                    ...prevState.dataMessage,
                    {
                      type: 'mine',
                      typeMes: 'chat',
                      atTime: chat.created_at,
                      message: chat.text
                    }
                  ]
                }))
              } else {
                return this.setState( prevState => ({
                  dataMessage: [
                    ...prevState.dataMessage,
                    {
                      type: 'theirs',
                      typeMes: 'chat',
                      atTime: chat.created_at,
                      message: chat.text
                    }
                  ]
                }))
              }
            } else {
              //type = upload file
              let file = await this.handleGetUploadFile(chat.text)

              return this.setState( prevState => ({
                dataMessage: [
                  ...prevState.dataMessage,
                  {
                    type: 'mine',
                    typeMes: 'uploadFile',
                    atTime: chat.created_at,
                    url: file.url,
                    name: file.name
                  }
                ]
              }))
            }

          })
        }
        console.log(this.state)
      },
      error => {
        console.log(error)
      }
    )
  }

  onConnectChat = () => {
    console.log("connected chat")
    Session.connect(this.state.tokbox.opentok_api_key, this.state.tokbox.tokbox_session_id, this.state.tokbox.opentok_token)
    Session.onMessageRecieved(async event => {
      console.log('event: ', event)
      const data = JSON.parse(event.data)
      const content = {
        ...event,
        data
      }
      console.log(content)
      if(content.data.text.indexOf('<a target="_blank" ') === -1) {
        // type = chat
        if(content.from !== content.to) {
          this.setState( prevState => ({
            dataMessage: [
              ...prevState.dataMessage,
              {
                type: 'theirs',
                typeMes: 'chat',
                atTime: '',
                message: content.data.text
              }
            ]
          }))
        } else {
          this.setState( prevState => ({
              dataMessage: [
                ...prevState.dataMessage,
                {
                  type: 'mine',
                  typeMes: 'chat',
                  atTime: '',
                  message: content.data.text
                }
              ]
            }),
            () => console.log(this.state))
        }
      } else {
        //type = upload file
        let file = await this.handleGetUploadFile(content.data.text)
        console.log ('file: ', file)
        this.setState( prevState => ({
          dataMessage: [
            ...prevState.dataMessage,
            {
              type: 'mine',
              typeMes: 'uploadFile',
              atTime: '',
              url: file.url,
              name: file.name
            }
          ]
        }))
      }
    })
  }

  renderVideoCall =() => {
    return (
      <View style={ styles.container }>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.isChatBox}
          onRequestClose={this.handleCloseChatBox}
        >
          <ChatBox
            onPressBack={ this.handleCloseChatBox }
            data = { this.state.dataMessage }
            userId = { this.state.user_id }
            appointment_request_id = {this.props.navigation.state.params.appointment_request_id}
            handlePressEnterChat={ this.handlePressEnterChat }
          />
        </Modal>
        <View style={ styles.boxTool }>
          { this.state.isChatButton &&
          <Icon
            raised
            name='forum'
            color='#35c1cf'
            onPress={ this.handleOpenChatBox }
            reverse
          />
          }
          { this.state.isEndButton &&
          <Icon
            raised
            name='call-end'
            color='#f23d2c'
            onPress={ this.handleEndCall }
            reverse
          />
          }
        </View>
        {this.state.isPublisher &&
        <PublisherView
          apiKey={this.state.tokbox.opentok_api_key}
          sessionId={this.state.tokbox.tokbox_session_id}
          token={this.state.tokbox.opentok_token}
          style={ this.handleStyleContainerPublisher() }
          onPublishStart={ this.onPublishStart }
        />
        }
        { this.state.isSubscriber &&
        <SubscriberView
          apiKey={this.state.tokbox.opentok_api_key}
          sessionId={this.state.tokbox.tokbox_session_id}
          token={this.state.tokbox.opentok_token}
          style={ this.handleStyleContainerSubscriber() }
          onSubscribeStart={ this.onSubscribeStart }
          onSubscribeStop={ this.onSubscribeStop }
        />
        }
      </View>
    )
  }

  renderWaiting = (text = "Waiting") => {
    return (
      <View style={ styles.containerEndCall }>
        <ActivityIndicator
          animating= {true}
          style={[globalStyles.indicatorPageLoading, {backgroundColor: '#FFF'}]}
          color={Colors.TEXT_SECONDARY}
          size='large'
        />
        <Text style={{paddingTop: 120}}>{ text }</Text>
        <Button
          title={ I18n.t('videoCall.btnGoBack') }
          onPress={ () => {
            clearInterval(intervalCheckClientConnect)
            this.props.navigation.goBack()
            KeepAwake.deactivate()
          }}
          buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
          containerViewStyle={ [globalStyles.buttonView, { marginTop: 10, minWidth: 300 }] }
        />
      </View>
    )
  }

  renderEndCall = () => {
    if(this.state.isFetching) {
      return this.renderWaiting()
    } else {
      KeepAwake.deactivate()

      if(this.state.isAvailableTokbox) {
        this.props.navigation.goBack()
        return alert(I18n.t('videoCall.mesUserDisconnect'))
      } else {
        return(
          <View style={ styles.containerEndCall }>
            <Text>{ I18n.t('videoCall.textAlertEndCall') }</Text>
            <Button
              title={ I18n.t('videoCall.btnGoSummary') }
              onPress={ async () => {
                clearInterval(intervalCheckClientConnect)
                // this.props.navigation.goBack()
                this.props.navigation.navigate(
                  'SummaryAppointment',
                  {
                    user_id: this.state.user_id,
                    appointment_id: await this.props.navigation.state.params.appointment_id,
                    appointment_request_id: await this.props.navigation.state.params.appointment_request_id,
                    navigate_home : true
                  })
              }}
              buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
              containerViewStyle={ [globalStyles.buttonView, { marginTop: 25, minWidth: 300 }] }
            />
          </View>
        )
      }
    }
  }

  handleRender = () => {
    switch (this.state.status) {
      case 'WAITING':
        return this.renderWaiting(I18n.t('videoCall.mesWaitingUser'))
      case 'CALLING':
        return this.renderVideoCall()
      case 'ENDCALL':
        return this.renderEndCall()
    }
  }

  componentDidMount() {
    // this.getAPICheckHeartbeat()
    this.getAPITokboxInfo()
    this.setState({user_id: this.props.navigation.state.params.user_id})

    //keep awake
    KeepAwake.activate()
  }

  render() {
    return (
      <View style={ styles.container }>
        { this.handleRender() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    position: 'relative'
  },
  containerEndCall: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  boxTool: {
    // backgroundColor: '#35c1cf',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    zIndex: 9,
    minHeight: 70,
  },
  wrapperLarge: {
    width: dimensions.width,
    height: dimensions.height - 100,
  },
  wrapperSmall: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 2,
    width: dimensions.width * 0.3,
    height: dimensions.height * 0.3,
    borderWidth: 1,
    borderColor: '#ccc',
  },
  wrapperHidden: {
    width: 0,
    height: 0,
  }
})

export default connect()(VideoCallScreen)
