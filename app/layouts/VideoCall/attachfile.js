import React from 'react'
import ImagePicker from 'react-native-image-picker'
import {Platform} from 'react-native'
import Environments from '../../configs/environmentsAPIs'
import {RNS3} from 'react-native-aws3'
import {APIs} from '../../configs/constants'
import GMGClient from "../../configs/gmgHttpClient";

const options = {
  title: 'Select Image',
  takePhotoButtonTitle: null,
  // customButtons: [
  //   {name: 'fb', title: 'Choose Photo from Facebook'},
  // ],
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}


const contentType = (type) => {
  switch(type) {
    case 'jpg':
      return 'image/jpeg'
      break
    case 'jpeg':
      return 'image/jpeg'
      break
    case 'png':
      return 'image/png'
      break
    case 'gif':
      return 'image/gif'
      break
    case 'bmp':
      return 'image/bmp'
      break
    case 'tiff':
      return 'image/tiff'
      break
    case 'pdf':
      return 'application/pdf'
      break
    case 'doc':
      return 'application/msword'
      break
    case 'docx':
      return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      break
    case 'xls':
      return 'application/vnd.ms-excel'
      break
    case 'ppt':
      return 'application/vnd.ms-powerpoint'
      break
    case 'pptx':
      return 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      break
    case 'mp4':
      return 'application/mp4'
      break
    case 'H264':
      return 'video/H264'
      break
    case 'H265':
      return 'video/H265'
      break
    case '3gpp':
      return 'video/3gpp'
      break
  }
}

const uploadFileToS3 = (file, success, failure) => {
  const options = {
    keyPrefix: "/",
    bucket: Environments.AWS_S3_BUCKET,
    region: Environments.AWS_S3_REGION,
    accessKey: Environments.AWS_S3_KEY,
    secretKey: Environments.AWS_S3_SECRET_KEY,
    successActionStatus: 200
  }

  RNS3.put(file, options)
    .then(res => success(res))
    .catch(error => failure(error))
}

const uploadFileToGMG = (appointment_request_id, body) => {
  let urlAPI = `${APIs.UPLOAD_FILE_APPOINTMENT_REQUEST_STEP1}/${appointment_request_id}`

  // Upload file underground
  GMGClient.postData(
    urlAPI,
    body,
    json => {
      if(json.success) {
        console.log('success upload file to S3: ', json.data)
      }
    },
    error => {
      console.log(error)
    }
  )
}

const attachFile = (userId, appointment_request_id, callbackFunc) => {
  return (ImagePicker.showImagePicker(options, async (response) => {
    console.log(response)

    if (response.didCancel) {
      console.log('User cancelled image picker')
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error)
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton)
    }
    else {
      let arrName = []
      let file = {}
      const file_size = response.fileSize
      if(Platform.OS === 'ios') {
        console.log("run case IOS")
        console.log(response)
        console.log(response.uri)
        console.log("logged")
        let arr = response.uri.split('/')
        console.log(arr)
        arrName = arr[arr.length - 1].split('.')
        console.log(arrName)
        const type = contentType(arrName[1])
        console.log(type)
        file = {
          uri: response.uri,
          name: arr[arr.length - 1],
          type: type
        }
      } else {
        arrName = response.fileName.split('.')
        file = {
          uri: response.uri,
          name: response.fileName,
          type: response.type
        }
      }

      console.log(file)

      uploadFileToS3(
        file,
        response => {
          console.log("response: ", response)
          if (response.status === 200) {
            const bodyOptions = {
              user_id: userId,
              file_url: response.body.postResponse.location || response.headers.Location,
              file_name: arrName[0] + '.' + arrName[1],
              file_type: arrName[1],
              file_size: file_size
            }
            console.log(bodyOptions)

            const textChat = `Uploaded File: <a target="_blank" href="${bodyOptions.file_url}">${bodyOptions.file_name}</a>`
            //send url to tokbox
            callbackFunc(textChat)

            //save info uploaded file to database GMG
            uploadFileToGMG(appointment_request_id, bodyOptions)
          } else {
            alert("Failed to upload image to Amazone service. Please contact us!")
          }
        },
        error => {
          console.log(error)
        }
      )
    }
  }))
}

export default attachFile