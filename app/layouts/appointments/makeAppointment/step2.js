import React, { Component, PropTypes } from 'react'
import { View, Text, Keyboard } from 'react-native'
import I18n from 'react-native-i18n'
import { Button } from 'react-native-elements'
import  TopLabelField from '../../../components/form/TopLabelField'
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment-timezone'

import GMGClient from '../../../configs/gmgHttpClient'
import {  APIs, TextStatic } from '../../../configs/constants'
import localStyles from './localStyles'
import globalStyles from '../../../styles'

export default class MakeAppointmentStepTwo extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      dateSlot1: this.props.dateSlot1,
      dateSlot2: this.props.dateSlot2,
      dateSlot3: this.props.dateSlot3,
      isDatePickerVisible: false,
      isTimePickerVisible: false,
      currentChecked: 0,
      currentDateTimeValue: new Date()
    }
  }

  validation = () => {
      let slot1 = this.state.dateSlot1
      let slot2 = this.state.dateSlot2
      let slot3 = this.state.dateSlot3
      let check = true;

      if( slot1.value === '' || slot1.value === undefined ) {
          slot1.error = true;
          this.setState({ dateSlot1: slot1 });
          check = false;
      } else {
          slot1.error = false;
          this.setState({ dateSlot1: slot1 });
      }

      if( slot2.value === '' || slot2.value === undefined ) {
          slot2.error = true;
          this.setState({ dateSlot2: slot2 });
          check = false;
      } else {
          slot2.error = false;
          this.setState({ dateSlot2: slot2 });
      }

      if( slot3.value === '' || slot3.value === undefined ) {
          slot3.error = true;
          this.setState({ dateSlot3: slot3 });
          check = false;
      } else {
          slot3.error = false;
          this.setState({ dateSlot3: slot3 });
      }

      return check;
  }

  handleNextStep = (position) => {
    Keyboard.dismiss()

    let checkValidate = this.validation()

    if (checkValidate) {
      console.log(this.state)
      this.updateAppointment()
      this.props.setStep2(this.state.dateSlot1, this.state.dateSlot2, this.state.dateSlot3)
      this.props.setCurrentPage(position)
    }
  }

  updateAppointment = () => {
    const state = this.state
    const urlAPI = APIs.UPDATE_APPOINTMENT_REQUEST_STEP2 + '/' + this.props.appointment_request_id
    const body = {
      timeslot1: moment(state.dateSlot1.convert).utc().format('DD-MM-YYYY HH:mm'),
      timeslot2: moment(state.dateSlot2.convert).utc().format('DD-MM-YYYY HH:mm'),
      timeslot3: moment(state.dateSlot3.convert).utc().format('DD-MM-YYYY HH:mm'),
    }

    GMGClient.putData(
      urlAPI,
      body,
      json => {
        if(!json.success) {
          //navigation to step 2
          this.props.setCurrentPage(1)
          alert("error " + json.status_code + "\n" + json.message)
        }
      },
      error => {
        // navigation to step 2
        this.props.setCurrentPage(1)
        alert(error)
      }
    )
  }
  handleFocus = (currentChecked) => {
    Keyboard.dismiss()
    
    this.setState({ currentChecked })

    this._handleCheckValueEachSlots(currentChecked)
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  _handleDateTimePicked = (date) => {
    let slot1 = this.state.dateSlot1
    let slot2 = this.state.dateSlot2
    let slot3 = this.state.dateSlot3

    switch(this.state.currentChecked) {
      case 1:
        slot1.value = moment(date).format('DD-MM-YYYY HH:mm')
        slot1.convert = this._handleFormatNewDate(date)
        slot1.error = false
        
        this.setState({ dateSlot1: slot1 })
        break
      case 2:
        slot2.value = moment(date).format('DD-MM-YYYY HH:mm')
        slot2.convert = this._handleFormatNewDate(date)
        slot2.error = false
        
        this.setState({ dateSlot2: slot2 })
        break
      case 3:
        slot3.value = moment(date).format('DD-MM-YYYY HH:mm')
        slot3.convert = this._handleFormatNewDate(date)
        slot3.error = false
        
        this.setState({ dateSlot3: slot3 })
        break
    }

    // this.validation()
    
    this._hideDateTimePicker()
    // this._showTimePicker()
  }

  _handleFormatNewDate = (date) => {
    let arrDate = [
      moment(date).year(),
      moment(date).month(),
      moment(date).date(),
      moment(date).hour(),
      moment(date).minute(),
      moment(date).second()
    ]

    let format = new Date(moment(arrDate))

    return format
  }

  _handleCheckValueEachSlots = (currentChecked) => {
    switch(currentChecked) {
      case 1:
        this.setState({ currentDateTimeValue: this.state.dateSlot1.value === '' ? new Date() : this.state.dateSlot1.convert })
        break
      case 2:
        this.setState({ currentDateTimeValue: this.state.dateSlot2.value === '' ? new Date() : this.state.dateSlot2.convert })
        break
      case 3:
        this.setState({ currentDateTimeValue: this.state.dateSlot3.value === '' ? new Date() : this.state.dateSlot3.convert })
        break
    }

    this._showDateTimePicker()
  }

  render() {
    let timezone = moment.tz.guess() + ' ' + moment.tz(moment.tz.guess()).format('Z')

    return (
      <View style={ [localStyles.wrapper] }>
        <Text style={ [globalStyles.normalText, globalStyles.textBold, globalStyles.textBig] }>
          { I18n.t("appointments.make.stepTwo.note") }
        </Text>
        <Text style={ globalStyles.normalText }>
          { I18n.t("appointments.make.stepTwo.timezone") }: {timezone}
        </Text>
        <View style={ [{ marginTop: 10, }] }>
          <TopLabelField
            value={this.state.dateSlot1.value}
            label={ I18n.t("appointments.make.stepTwo.titleSlots") + ' 1 *' }
            calendarIcon={ true }
            placeholder={ TextStatic.DATE_TIME_PLACEHOLDER.toUpperCase() }
            onFocus={ () => this.handleFocus(1) }
            errorMes={ I18n.t("appointments.make.stepTwo.errorMes") }
            error={ this.state.dateSlot1.error }
          />
          <TopLabelField
            value={this.state.dateSlot2.value}
            label={ I18n.t("appointments.make.stepTwo.titleSlots") + ' 2 *' }
            calendarIcon={ true }
            placeholder={ TextStatic.DATE_TIME_PLACEHOLDER.toUpperCase() }
            onFocus={ () => this.handleFocus(2) }
            errorMes={ I18n.t("appointments.make.stepTwo.errorMes") }
            error={ this.state.dateSlot2.error }
          />
          <TopLabelField
            value={this.state.dateSlot3.value}
            label={ I18n.t("appointments.make.stepTwo.titleSlots") + ' 3 *' }
            calendarIcon={ true }
            placeholder={ TextStatic.DATE_TIME_PLACEHOLDER.toUpperCase() }
            onFocus={ () => this.handleFocus(3) }
            errorMes={ I18n.t("appointments.make.stepTwo.errorMes") }
            error={ this.state.dateSlot3.error }
          />
        </View>

        <DateTimePicker
          mode='datetime'
          date={this.state.currentDateTimeValue}
          minimumDate={new Date()}
          isVisible={this.state.isDateTimePickerVisible}
          is24Hour={true}
          onConfirm={this._handleDateTimePicked}
          onCancel={this._hideDateTimePicker}
        />

        <Button
          title={ I18n.t("buttons.btnNext").toUpperCase() }
          onPress={ () => this.handleNextStep(2) }
          buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
          containerViewStyle={ globalStyles.buttonView }
        />
      </View>
    )
  } 
}
