import React, { Component, PropTypes } from 'react'
import { View, Text, Keyboard, ScrollView } from 'react-native'
import I18n from 'react-native-i18n'
import { Button } from 'react-native-elements'
import  TopLabelField from '../../../components/form/TopLabelField'
import { Label, TextField, ErrorMes } from '../../../components/form/Field'

import GMGClient from '../../../configs/gmgHttpClient'
import { APIs } from '../../../configs/constants'
import localStyles from './localStyles'
import globalStyles from '../../../styles'

export default class MakeAppointmentStepThree extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: this.props.name,
      number: this.props.number,
      expiration: this.props.expiration,
      CVV: this.props.CVV,
      updateCreditCard: this.props.updateCreditCard,
      newCredit: {
        name: {
          value: '',
          error: false
        },
        number: {
          value: '',
          error: false
        },
        expiration: {
          value: '',
          error: false
        },
        CVV: {
          value: '',
          error: false
        }
      },
      falseUpdate: false
    }
  }

  validation = () => {
      let newCredit = this.state.newCredit
      let check = true;

      if( newCredit.name.value === '' || newCredit.name.value === undefined ) {
          newCredit.name.error = true;
          this.setState({ newCredit });
          check = false;
      } else {
          newCredit.name.error = false;
          this.setState({ newCredit });
      }

      if( newCredit.number.value === '' || newCredit.number.value === undefined ) {
          newCredit.number.error = true;
          this.setState({ newCredit });
          check = false;
      } else {
          newCredit.number.error = false;
          this.setState({ newCredit });
      }

      if( newCredit.expiration.value === '' || newCredit.expiration.value === undefined ) {
          newCredit.expiration.error = true;
          this.setState({ newCredit });
          check = false;
      } else {
          newCredit.expiration.error = false;
          this.setState({ newCredit });
      }

      if( newCredit.CVV.value === '' || newCredit.CVV.value === undefined ) {
          newCredit.CVV.error = true;
          this.setState({ newCredit });
          check = false;
      } else {
          newCredit.CVV.error = false;
          this.setState({ newCredit });
      }

      return check;
  }

  handleCheckValue = (value, state) => {
    let error = value !== '' ? false : true
    let newCredit = this.state.newCredit

    switch(state) {
      case 'name':
        newCredit.name.value = value
        newCredit.name.error = error

        this.setState({ newCredit })
        break
      case 'number':
        value = value.split("-").join("").slice(0,16).replace(/\D/g,"")

        if (value.length > 0) {
          value = value.match(/\d{1,4}/g).join("-")
        }

        newCredit.number.value = value
        newCredit.number.error = error

        this.setState({ newCredit })
        break
      case 'expiration':
        value = this.updateFormatExpiration(value)

        newCredit.expiration.value = value
        newCredit.expiration.error = error

        this.setState({ newCredit })
        break
      case 'CVV':
        value = value.split("/").join("").slice(0,3).replace(/\D/g,"")

        newCredit.CVV.value = value
        newCredit.CVV.error = error

        this.setState({ newCredit })
        break
    }
  }

  updateFormatExpiration = (value) => {
    value = value.split("/").join("").slice(0,6).replace(/\D/g,"")
    if (value.length > 0) {
      value = value.match(new RegExp('.{1,4}$|.{1,2}', 'g')).join("/")
    }

    return value
  }

  handleNextStep = (position) => {
    Keyboard.dismiss()

    this.props.setCurrentPage(position)

    // if(this.state.updateCreditCard) {
    //   let checkValidate = this.validation()

    //   if (checkValidate) {
    //     this.props.setStep3(this.state.newCredit.name, this.state.newCredit.number, this.state.newCredit.expiration, this.state.newCredit.CVV)
    //     this.props.setCurrentPage(position)
    //   }
    // } else {
    //   this.props.setCurrentPage(position)
    // }
  }

  createCreaditCard = () => {
    let urlAPI = APIs.CREDIT_CARD + '/' + this.props.user_id
    let bodyData = {
      "card_name": this.state.newCredit.name.value,
      "card_number": this.state.newCredit.number.value,
      "expiry": this.state.newCredit.expiration.value,
      "cvv": this.state.newCredit.CVV.value
    }

    GMGClient.postData(
      urlAPI,
      bodyData,
      (json) => {
        // this.setLoadingProgress(false);
        if(!json.success) {
          // this.props.setCurrentPage(2)
          // alert("error " + json.status_code + "\n" + json.message)
          this.setState({ falseUpdate: true })
        } else {
          this.props.setStep3(this.state.newCredit.name, this.state.newCredit.number, this.state.newCredit.expiration, this.state.newCredit.CVV, this.state.updateCreditCard)
          this.setState({ updateCreditCard: !this.state.updateCreditCard })
        }
      },
      (error) => {
        // this.setLoadingProgress(false);
        // alert(error);
        this.setState({ falseUpdate: true })
      }
    )
  }

  handleUpdateCreditCard = () => {
    let checkValidate = this.validation()
    this.setState({ falseUpdate: false })

    Keyboard.dismiss()

    if (checkValidate) {
      this.createCreaditCard()
    }
  }

  updateCreditCardInfo = () => {
    return (
      <ScrollView keyboardShouldPersistTaps='handled'>
        <View style={{ marginTop: 10, }}>
          <TopLabelField
            inputRef={ node => this.inputNameCard = node }
            value={this.state.newCredit.name.value}
            label={ I18n.t("appointments.make.stepThree.card.name.title") + ' *' }
            placeholder={ I18n.t("appointments.make.stepThree.card.name.placeholder") }
            onChangeText={ (value) => this.handleCheckValue(value, 'name') }
            returnKeyType="next"
            onSubmitEditing={ () => this.inputNumberCard.focus() }
            errorMes={ I18n.t("appointments.make.stepThree.card.name.errorMes") }
            error={ this.state.newCredit.name.error }
          />
          <TopLabelField
            inputRef={ node => this.inputNumberCard = node }
            value={this.state.newCredit.number.value}
            label={ I18n.t("appointments.make.stepThree.card.number.title") + ' *' }
            placeholder={ I18n.t("appointments.make.stepThree.card.number.placeholder") }
            onChangeText={ (value) => this.handleCheckValue(value, 'number') }
            returnKeyType="next"
            onSubmitEditing={ () => this.inputExpirationCard.focus() }
            errorMes={ I18n.t("appointments.make.stepThree.card.number.errorMes") }
            error={ this.state.newCredit.number.error }
          />
          <View>
            <Label label={ I18n.t("appointments.make.stepThree.card.validAndCVV.title") + ' *' }  error={ this.state.expiration.error || this.state.CVV.error } />
            <View style={ [globalStyles.blockInline, {marginBottom: 15}] }>
              <View style={{flex: 3}}>
                <TextField
                  inputRef={ node => this.inputExpirationCard = node }
                  value={ this.state.newCredit.expiration.value !== undefined ? this.updateFormatExpiration(this.state.newCredit.expiration.value) : this.state.newCredit.expiration.value }
                  placeholder={ I18n.t("appointments.make.stepThree.card.validAndCVV.placeholder.expiration") }
                  onChangeText={ (value) => this.handleCheckValue(value, 'expiration') }
                  returnKeyType="next"
                  onSubmitEditing={ () => this.inputCVVCard.focus() }
                  error={ this.state.newCredit.expiration.error }
                />
              </View>
              <View style={{flex: 2, marginLeft: 10}}>
                <TextField
                  inputRef={ node => this.inputCVVCard = node }
                  value={this.state.newCredit.CVV.value}
                  placeholder={ I18n.t("appointments.make.stepThree.card.validAndCVV.placeholder.CVV") }
                  onChangeText={ (value) => this.handleCheckValue(value, 'CVV') }
                  returnKeyType="send"
                  onSubmitEditing={ () => this.handleUpdateCreditCard() }
                  error={ this.state.newCredit.CVV.error }
                />
              </View>
            </View>
            { (this.state.newCredit.expiration.error || this.state.newCredit.CVV.error) ? <ErrorMes errorMes={ I18n.t("appointments.make.stepThree.card.validAndCVV.errorMes") } /> : null }
          </View>
          { this.state.falseUpdate ? <ErrorMes mesType='ERROR' errorMes = { I18n.t("appointments.make.stepThree.card.errorMes") } arrow = { false } styleContainer={{marginTop: 10, marginBottom: 0}}/> : null }
        </View>
        <Button
          title={ I18n.t("buttons.btnSaveCreditCard").toUpperCase() }
          onPress={ () => this.handleUpdateCreditCard() }
          buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
          containerViewStyle={ globalStyles.buttonView }
        />
        { (this.props.name.value !== '') ?
          <Button
          title={ I18n.t("buttons.btnCancel").toUpperCase() }
          onPress={ () => this.setState({updateCreditCard: false}) }
          buttonStyle={ [globalStyles.buttonGrey, globalStyles.button] }
          containerViewStyle={ globalStyles.buttonView }
          /> : null }
      </ScrollView>
    )
  }

  showCreditCardInfo = () => {
    return (
      <View style={{ marginTop: 10, }}>
        <Label label={ I18n.t("appointments.make.stepThree.card.name.title") + ' *' } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText, { paddingBottom: 10 }] }>
          { this.props.name.value }
        </Text>
        <Label label={ I18n.t("appointments.make.stepThree.card.number.title") + ' *' } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText, { paddingBottom: 10 }] }>
          { this.props.number.value }
        </Text>
        <Label label={ I18n.t("appointments.make.stepThree.card.validAndCVV.title") + ' *' } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText, { paddingBottom: 10 }] }>
          {this.props.expiration.value !== undefined ? this.updateFormatExpiration(this.props.expiration.value) : this.props.expiration.value} - { this.props.CVV.value }
        </Text>
        <Button
          title={ I18n.t("buttons.btnUpdateCreditCard").toUpperCase() }
          onPress={ () => this.setState({updateCreditCard: true}) }
          buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
          containerViewStyle={ globalStyles.buttonView }
        />
        <Button
          title={ I18n.t("buttons.btnNext").toUpperCase() }
          onPress={ () => this.handleNextStep(3) }
          buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
          containerViewStyle={ globalStyles.buttonView }
        />
      </View>
    )
  }

  render() {
    return (
        <View style={ [localStyles.wrapper] }>
          <Text style={ [globalStyles.normalText, globalStyles.textBold, globalStyles.textBig] }>
            { I18n.t("appointments.make.stepThree.kind") }
          </Text>
          { this.state.updateCreditCard ? this.updateCreditCardInfo() : this.showCreditCardInfo() }
        </View>
    )
  }
}
