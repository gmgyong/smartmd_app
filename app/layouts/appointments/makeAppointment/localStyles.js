import { StyleSheet, Dimensions } from 'react-native'
import { Colors, FontSizes, SPACE } from '../../../configs/constants'

// const DEVICE_WIDTH = Dimensions.get('window').width;

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_MAIN_SCREEN,
    paddingLeft: SPACE,
    paddingRight: SPACE,
  },
  paddingContainer: {
    paddingLeft: SPACE,
    paddingRight: SPACE,
  },
  wrapper: {
    // alignItems: 'center',
    backgroundColor: Colors.BACKGROUND_MAIN_SCREEN,
  },
  stepIndicator: {
    paddingTop: 20,
    paddingLeft: SPACE/10,
    paddingRight: SPACE/10,
  },
  stepTitle: {
    fontSize: FontSizes.LARGE,
    fontWeight: 'bold',
    color: Colors.TEXT_PRIMARY,
    textAlign:'center',
    marginTop: 10,
  },
  doctorTitle: {
    fontSize: FontSizes.LARGE,
    color: Colors.TEXT_PRIMARY,
  },
  lineTitleDoctor: {
    width: 25,
    alignSelf: 'center',
  },
  attachFilesBox: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#f5f5f5',
    borderColor: '#959595',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 4,
    overflow: 'hidden',
  },
  iconRemove: {
    position: 'absolute',
    right: 10,
    top: 0 
  }
})

/** export module */
module.exports = localStyles
