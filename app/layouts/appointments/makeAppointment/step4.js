import React, { Component, PropTypes } from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import { navigate } from 'react-navigation'
import { Button } from 'react-native-elements'
import { Label, } from '../../../components/form/Field'
import FormatDateTime from '../../../configs/formatDate'
import GMGClient from '../../../configs/gmgHttpClient'
import {  APIs, SPACE, Colors } from '../../../configs/constants'
import localStyles from './localStyles'
import globalStyles from '../../../styles'

export default class MakeAppointmentStepFour extends Component {
  constructor(props) {
    super(props);
    this.state= {
      isDataLoading: false,
    }
  }

  setLoadingProgress = (isDataLoading) => {
    this.setState({isDataLoading});
  }

  renderLoadingIndicator = (isShowing) => {
    if (isShowing) {
      // console.log(isShowing, isTransparent, 'checking')
      return (
        <ActivityIndicator
          animating={isShowing}
          style={[globalStyles.indicatorPageLoadingNew]}
          color={Colors.TEXT_SECONDARY}
          size='small'/>
      )
    } else {
      return null;
    }
  }

  componentDidMount() {
    console.log(this.props.state)
  }

  renderDocuments = () => {
    return (
      <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer] }>
          <Label label={ I18n.t("appointments.make.stepOne.documents").toUpperCase() } style={{ marginBottom: 0 }} />
          { this.props.state.documents.map((item, keyItem) => {
            return (
              <Text style={ [globalStyles.normalText] } key={ keyItem }>
                {item.name}.{item.type}
              </Text>
            )
          }) }
      </View>
    )
  }

  renderContentStep1 = (content, label) => {
    return (
      <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer] }>
        <Label label={ label.toUpperCase() } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText] }>
          {content}
        </Text>
      </View>
    )
  }

  updateAppointment = () => {
    console.log("aaaaa");
    const {navigate} = this.props.navigation
    const urlAPI = APIs.UPDATE_APPOINTMENT_REQUEST_STEP4 + '/' + this.props.appointment_request_id
    this.setLoadingProgress(true);
    GMGClient.putData(
      urlAPI,
      null,
      json => {
        if(!json.success) {
          // this.props.setCurrentPage(3)
          alert("error " + json.status_code + "\n" + json.message)
        } else {
          // this.setLoadingProgress(true);
          navigate('Home')
        }
      },
      error => {
        //navigation to step 2
        // this.props.setCurrentPage(3)
        alert(JSON.stringify(error))
      }
    )
  }

  render() {
    const {state} = this.props

    return (
      <View style={ [localStyles.wrapper] }>
        <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer]}>
          <Label label={ I18n.t("appointments.make.stepFour.timeslots").toUpperCase() } style={{ marginBottom: 0 }} />
          <Text style={ [globalStyles.normalText] }>
            {FormatDateTime.changeFormat(state.dateSlot1.convert)}
          </Text>
          <Text style={ [globalStyles.normalText] }>
            {FormatDateTime.changeFormat(state.dateSlot2.convert)}
          </Text>
          <Text style={ [globalStyles.normalText] }>
            {FormatDateTime.changeFormat(state.dateSlot3.convert)}
          </Text>
        </View>

        { state.symptoms !== '' ? this.renderContentStep1(state.symptoms, I18n.t("appointments.make.stepOne.symptoms")) : <View/> }

        { state.medicalHistory !== '' ? this.renderContentStep1(state.medicalHistory, I18n.t("appointments.make.stepOne.medicalHistory")) : <View/> }

        { state.drugHistory !== '' ? this.renderContentStep1(state.drugHistory, I18n.t("appointments.make.stepOne.drugHistory")) : <View/> }

        { state.drugHistory !== '' ? this.renderContentStep1(state.allergies, I18n.t("appointments.make.stepOne.allergies")) : <View/> }

        { state.documents.length > 0 ? this.renderDocuments() : <View/> }

        <View style={ localStyles.paddingContainer }>
          <Button
            title={ I18n.t("buttons.btnSendRequest").toUpperCase() }
            onPress={ this.updateAppointment  }
            buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
            containerViewStyle={ globalStyles.buttonView }
          />
        </View>
        {this.renderLoadingIndicator(this.state.isDataLoading)}
      </View>
    )
  }
}
