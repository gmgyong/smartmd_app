import React, {Component, PropTypes} from 'react'
import {View, Text, Dimensions, ScrollView} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {NavigationActions} from 'react-navigation'
import {Button} from 'react-native-elements'
import StepIndicator from 'react-native-step-indicator';

import MakeAppointmentStepOne from './step1'
import MakeAppointmentStepTwo from './step2'
import MakeAppointmentStepThree from './step3'
import MakeAppointmentStepFour from './step4'

import GMGClient from '../../../configs/gmgHttpClient'
import {Colors, APIs} from '../../../configs/constants'
import localStyles from './localStyles'
import globalStyles from '../../../styles'

const DEVICE_WIDTH = Dimensions.get('window').width;

const indicatorStyles = {
  stepIndicatorSize: 16,
  currentStepIndicatorSize: 20,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 1,
  stepStrokeCurrentColor: Colors.BACKGROUND_MAIN_SCREEN,
  separatorFinishedColor: Colors.BACKGROUND_STACKS,
  separatorUnFinishedColor: Colors.BACKGROUND_BUTTON_GREY,
  stepIndicatorFinishedColor: Colors.BACKGROUND_STACKS,
  stepIndicatorUnFinishedColor: Colors.BACKGROUND_BUTTON_GREY,
  stepIndicatorCurrentColor: Colors.BACKGROUND_STACKS,
  stepIndicatorLabelFontSize: 8,
  currentStepIndicatorLabelFontSize: 10,
  stepIndicatorLabelCurrentColor: Colors.TEXT_TABS_ICON,
  stepIndicatorLabelFinishedColor: Colors.TEXT_TABS_ICON,
  stepIndicatorLabelUnFinishedColor: Colors.TEXT_TABS_ICON,
}

class MakeAppointmentScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      appointment_request_id: '',
      info: {},
      currentPage: 0,
      // for step 1
      symptoms: '',
      medicalHistory: '',
      drugHistory: '',
      allergies: '',
      documents: [],
      // for step 2
      dateSlot1: {
        value: '',
        convert: new Date(),
        error: false,
      },
      dateSlot2: {
        value: '',
        convert: new Date(),
        error: false,
      },
      dateSlot3: {
        value: '',
        convert: new Date(),
        error: false,
      },
      // for step 3
      name: {
        value: '',
        error: false
      },
      number: {
        value: '',
        error: false
      },
      expiration: {
        value: '',
        error: false
      },
      CVV: {
        value: '',
        error: false
      },
      updateCreditCard: false
    }
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle: I18n.t("doctors.btnMakeAppointment"),
  })

  setCurrentPage = (position) => {
    this.setState({
      currentPage: position
    })

    console.log(this.state)
  }

  setStep1 = (symptoms, medicalHistory, drugHistory, allergies, documents) => {
    this.setState({
      symptoms,
      medicalHistory,
      drugHistory,
      allergies,
      documents
    })
  }

  setStep2 = (dateSlot1, dateSlot2, dateSlot3) => {
    this.setState({
      dateSlot1,
      dateSlot2,
      dateSlot3
    })
  }

  setStep3 = (name, number, expiration, CVV, updateCreditCard) => {
    this.setState({
      name,
      number,
      expiration,
      CVV,
      updateCreditCard
    })
  }

  onCreateAppointmentAPI = () => {
    console.log("CREATE APPOINTMENT REQUEST")
    const {info} = this.props.navigation.state.params
    const urlAPI = APIs.CREATE_APPOINTMENT_REQUEST
    const body = {
      user_id: info.user_id,
      doctor_id: info.doctor_id
    }

    GMGClient.postData(
      urlAPI,
      body,
      json => {
        if (json.success) {
          this.setState({
            appointment_request_id: json.appointment_request_id,
          })
        }
        console.log(this.state)
      },
      error => {
        alert(error)
      }
    )
  }

  onGetInfoCreaditCard = () => {
    console.log("GET INFO PAYMENT")
    const urlAPI = APIs.CREDIT_CARD + '/' + this.props.navigation.state.params.info.user_id + "?"

    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          if(json.data == "undefined" || json.data == null) {
            console.log('json.data null >>>>>>>>>>')
            this.setState({
              updateCreditCard: true
            })
          } else {
            console.log('json.data not null >>>>>>>>>>')
            this.setState({
              name: { value: json.data.card_name },
              number: { value: "****-****-****-" + json.data.last4 },
              expiration: { value: json.data.expiry },
              CVV: { value: "***" },
            })
          }
        }
      },
      error => {
        console.log("ERROR OF STRIPE API")
      }
    )
  }

  renderPrice = (prices = []) => {
    return prices.map( (item,index) => (
      <Text key={index} style={ [globalStyles.normalText, globalStyles.textCenter] }>{item.description}: {item.price}</Text>
    ))
  }

  componentWillMount() {
    const {info} = this.props.navigation.state.params
    this.setState({info})
  }

  componentDidMount() {
    this.onCreateAppointmentAPI()
    this.onGetInfoCreaditCard()
  }

  render() {
    const switchStep = (pageID) => {
      switch (pageID) {
        case 0: {
          return (
            <MakeAppointmentStepOne
              appointment_request_id={this.state.appointment_request_id}
              symptoms={this.state.symptoms}
              medicalHistory={this.state.medicalHistory}
              drugHistory={this.state.drugHistory}
              allergies={this.state.allergies}
              documents={this.state.documents}
              setStep1={this.setStep1.bind(this)}
              setCurrentPage={this.setCurrentPage.bind(this)}
            />
          )
        }
        case 1:
          return (
            <MakeAppointmentStepTwo
              appointment_request_id={this.state.appointment_request_id}
              dateSlot1={this.state.dateSlot1}
              dateSlot2={this.state.dateSlot2}
              dateSlot3={this.state.dateSlot3}
              setStep2={this.setStep2.bind(this)}
              setCurrentPage={this.setCurrentPage.bind(this)}
            />
          )
        case 2:
          return (
            <MakeAppointmentStepThree
              appointment_request_id={this.state.appointment_request_id}
              user_id={ this.state.info.user_id }
              name={this.state.name}
              number={this.state.number}
              expiration={this.state.expiration}
              CVV={this.state.CVV}
              updateCreditCard={this.state.updateCreditCard}
              setStep3={this.setStep3.bind(this)}
              setCurrentPage={this.setCurrentPage.bind(this)}
            />
          )
        case 3:
          return (
            <MakeAppointmentStepFour
              appointment_request_id={this.state.appointment_request_id}
              state={this.state}
              navigation={this.props.navigation}
            />
          )
          break
        default: {
          return (
            <MakeAppointmentStepOne
              appointment_request_id={this.state.appointment_request_id}
              symptoms={this.state.symptoms}
              medicalHistory={this.state.medicalHistory}
              drugHistory={this.state.drugHistory}
              allergies={this.state.allergies}
              documents={this.state.documents}
              setStep1={this.setStep1.bind(this)}
              setCurrentPage={this.setCurrentPage.bind(this)}
            />
          )
        }
      }
    }
    const currentStepTitle = (pageID) => {
      switch (pageID) {
        case 0:
          return (
            <Text style={ localStyles.stepTitle }>
              { I18n.t("appointments.make.stepOne.title").toUpperCase() }
            </Text>
          )
          break
        case 1:
          return (
            <Text style={ localStyles.stepTitle }>
              { I18n.t("appointments.make.stepTwo.title").toUpperCase() }
            </Text>
          )
          break
        case 2:
          return (
            <Text style={ localStyles.stepTitle }>
              { I18n.t("appointments.make.stepThree.title").toUpperCase() }
            </Text>
          )
          break
        case 3:
          return (
            <Text style={ localStyles.stepTitle }>
              { I18n.t("appointments.make.stepFour.title").toUpperCase() }
            </Text>
          )
          break
        default:
          return (
            <Text style={ localStyles.stepTitle }>
              { I18n.t("appointments.make.stepOne.title").toUpperCase() }
            </Text>
          )
          break
      }
    }

    return (

      <ScrollView
        keyboardShouldPersistTaps='handled'
        style={ [localStyles.container, this.state.currentPage === 3 ? {paddingLeft: 0, paddingRight: 0} : null] }>
        <View style={ localStyles.stepIndicator }>
          <StepIndicator
            customStyles={indicatorStyles}
            currentPosition={this.state.currentPage}
            stepCount={4}
            onPress={
              (position) => {
                position < this.state.currentPage ? this.setState({currentPage: position}) : null
              }
            }/>
        </View>

        <View style={ [localStyles.wrapper] }>
          { currentStepTitle(this.state.currentPage) }

          <View
            style={ [localStyles.lineTitleDoctor, globalStyles.line] }
          />
          <Text style={ [localStyles.doctorTitle, globalStyles.textCenter] }>
            {this.state.info.name}
          </Text>
          <View style={{paddingBottom: 15}}>
            { this.renderPrice(this.state.info.price_breakdown) }
          </View>
        </View>

        { switchStep(this.state.currentPage) }


        <View style={ this.state.currentPage === 3 ? localStyles.paddingContainer : null }>
          <Button
            title={ this.state.currentPage > 0 ? I18n.t("buttons.btnBack").toUpperCase() : I18n.t("buttons.btnCancel").toUpperCase() }
            onPress={ () => {
              if (this.state.currentPage > 0) {
                this.setCurrentPage(this.state.currentPage - 1)
              } else {
                this.props.navigation.dispatch(backAction)
              }

            } }
            buttonStyle={ [globalStyles.buttonGrey, globalStyles.button, {marginBottom: 20}] }
            containerViewStyle={ globalStyles.buttonView }
          />
        </View>
      </ScrollView>
    )
  }
}

const backAction = NavigationActions.back()

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}


export default connect(mapStateToProps, null)(MakeAppointmentScreen)
