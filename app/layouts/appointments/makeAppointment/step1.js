import React, { Component, PropTypes } from 'react'
import { View, Text,TextInput } from 'react-native'
import I18n from 'react-native-i18n'
import { Button } from 'react-native-elements'
import  TopLabelField from '../../../components/form/TopLabelField'
import  AttachFiles from './attachFiles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import GMGClient from '../../../configs/gmgHttpClient'
import { APIs } from '../../../configs/constants'
import localStyles from './localStyles'
import globalStyles from '../../../styles'

export default class MakeAppointmentStepOne extends Component {
  constructor(props) {
    super(props);
    console.log('..........................................')
    console.log(props)

    this.state = {
      appointment_request_id: '',
      symptoms: '',
      medicalHistory: '',
      drugHistory: '',
      allergies: '',
      documents: []
    }
  }

  updateAppointment = () => {
    const state = this.state
    const urlAPI = APIs.UPDATE_APPOINTMENT_REQUEST_STEP1 + '/' + this.props.appointment_request_id
    const body = {
      symptoms: state.symptoms,
      drug_history: state.drugHistory,
      drug_allergies: state.allergies,
      medical_history: state.medicalHistory
    }

    GMGClient.putData(
      urlAPI,
      body,
      json => {
        if(!json.success) {
          this.props.setCurrentPage(0)
          alert("error " + json.status_code + "\n" + json.message)
        }
      },
      error => {
        //navigation to step 1
        this.props.setCurrentPage(0)
        alert(error)
      }
    )
  }

  handleNextStep = (position) => {
    if(
      this.state.medicalHistory !== ''
      || this.state.drugHistory !== ''
      || this.state.symptoms !== ''
      || this.state.allergies !== ''
    ) {
      this.updateAppointment()
    }

    this.props.setStep1(this.state.symptoms, this.state.medicalHistory, this.state.drugHistory, this.state.allergies, this.state.documents)
    this.props.setCurrentPage(position)
  }

  setDocuments = (documents) => {
    this.setState({
      documents
    })
    console.log('??????????? ', documents)
    console.log(this.state.documents.filter((item) => item.isDataLoading === true), 'this.state.documents')
  }

  updateState = async (value, state) => {
    switch(state) {
      case 'symptoms':
        this.setState({
          symptoms: value
        })
        break
      case 'medicalHistory':
        this.setState({
          medicalHistory: value
        })
        break
      case 'drugHistory':
        this.setState({
          drugHistory: value
        })
        break
      case 'allergies':
        this.setState({
          allergies: value
        })
        break
    }
  }

  componentWillMount() {
    this.setState({
      appointment_request_id: this.props.appointment_request_id,
      symptoms: this.props.symptoms,
      medicalHistory: this.props.medicalHistory,
      drugHistory: this.props.drugHistory,
      allergies: this.props.allergies,
      documents: this.props.documents
    })
  }

  render() {
    return (
      <KeyboardAwareScrollView keyboardShouldPersistTaps='handled' style={ [localStyles.wrapper] }>
        <TopLabelField
          label={ I18n.t("appointments.make.stepOne.symptoms") }
          value={ this.state.symptoms }
          editable ={ true }
          multiline={ true }
          numberOfLines={ 4 }
          onChangeText={ (value) => this.updateState(value, 'symptoms') }

          inputRef={ node => this.inputsymptoms = node }
          returnKeyType="next"
          onSubmitEditing={ () => this.inputMedicalHistory.focus() }
        />
        <TopLabelField
          label={ I18n.t("appointments.make.stepOne.medicalHistory") }
          value={ this.state.medicalHistory }
          editable ={ true }
          multiline={ true }
          numberOfLines={ 4 }
          onChangeText={ (value) => this.updateState(value, 'medicalHistory') }

          inputRef={ node => this.inputMedicalHistory = node }
          returnKeyType="next"
          onSubmitEditing={ () => this.inputDrugHistory.focus() }
        />
        <TopLabelField
          label={ I18n.t("appointments.make.stepOne.drugHistory") }
          value={ this.state.drugHistory }
          editable ={ true }
          multiline={ true }
          numberOfLines={ 4 }
          onChangeText={ (value) => this.updateState(value, 'drugHistory') }

          inputRef={ node => this.inputDrugHistory = node }
          returnKeyType="next"
          onSubmitEditing={ () => this.inputAllergies.focus() }
        />
        <TopLabelField
          label={ I18n.t("appointments.make.stepOne.allergies") }
          value={ this.state.allergies }
          editable ={ true }
          multiline={ true }
          numberOfLines={ 4 }
          onChangeText={ (value) => this.updateState(value, 'allergies') }

          inputRef={ node => this.inputAllergies = node }
        />
        <AttachFiles
          label={ I18n.t("appointments.make.stepOne.documents") }
          documents={this.state.documents}
          setDocuments={ this.setDocuments.bind(this) }
          appointment_request_id={this.props.appointment_request_id}
        />

        <Button
          title={ I18n.t("buttons.btnNext").toUpperCase() }
          onPress={ () => this.state.documents.filter((item) => item.isDataLoading === true).length > 0 && this.state.documents.length > 0 ? null : this.handleNextStep(1) }
          buttonStyle={ [this.state.documents.filter((item) => item.isDataLoading === true).length > 0 && this.state.documents.length > 0 ? globalStyles.buttonBlueDisable : globalStyles.buttonBlue, globalStyles.button] }
          containerViewStyle={ globalStyles.buttonView }
        />
      </KeyboardAwareScrollView>
    )
  } 
}
