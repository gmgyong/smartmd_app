import React, { Component, PropTypes } from 'react'
import { View, Text,TextInput, Dimensions, TouchableHighlight, Image, Alert, ActivityIndicator, Platform } from 'react-native'
import I18n from 'react-native-i18n'
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker'
import { Button, Icon } from 'react-native-elements'
import { Label } from '../../../components/form/Field'
import Environments from '../../../configs/environmentsAPIs'
import {RNS3} from 'react-native-aws3'

import LocalStorage from '../../../components/localStorage'
import GMGClient from '../../../configs/gmgHttpClient'
import { Colors, FontSizes, APIs, StorageKeys } from '../../../configs/constants'
import localStyles from './localStyles'
import globalStyles from '../../../styles'

const DEVICE_WIDTH = Dimensions.get('window').width
const ImagePicker = require('react-native-image-picker')
// const FilePickerManager = require('NativeModules').FilePickerManager;

let options = {
  title: 'Select Avatar',
  // customButtons: [
  //   {name: 'fb', title: 'Choose Photo from Facebook'},
  // ],
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

export default class AttachFiles extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      filesAttached: this.props.documents.length > 0 ? this.props.documents.map((item, index) => {
        let arrName = item.name.split('.')
        let fileName = {
          name: arrName[0],
          type: arrName[1],
          uri: item.url,
          url: item.url,
          id: item.id,
          userId: item.user_id,
        }

        let source = this.optimizeFile(fileName, false)

        source.isDataLoading = false
        source.isDataLoadingOnLoading = true
        source.url = item.url
        source.id = item.id
        source.userId = item.user_id

        return source
      }) : this.props.documents,
      userId: ''
    }
  }

  async componentDidMount() {
    const userId = await LocalStorage.getString(StorageKeys.USERID)
    this.setState({userId})
  }

  checkType = (type) => {
    let acceptType = [ 'pdf', 'doc', 'docx', 'txt', '7z', 'tar', 'rar', 'gz', 'xlsx' ]

    return acceptType.indexOf(type)
  }

  optimizeFile = (file, checkImage) => {
    // let arrName = file.fileName.split('.')
    
    let newFile = {
      name: file.name.length > 7 ? file.name.substring(0,5) + '..' : file.name,
      type: file.type.toLowerCase(),
      path: file.uri,
      isDataLoading: true,
    }

    // return checkImage ? newFile : this.checkType(newFile.type) > -1 ? newFile : null
    return newFile
  }

  handleUploadS3 = async (file, success, failure) => {
    const options = {
      keyPrefix: "/",
      bucket: Environments.AWS_S3_BUCKET,
      region: Environments.AWS_S3_REGION,
      accessKey: Environments.AWS_S3_KEY,
      secretKey: Environments.AWS_S3_SECRET_KEY,
      successActionStatus: 201
    }
    RNS3.put(file, options).then(res => {
      if (res.status !== 201) {
        console.log("Failed to upload image to S3", res)
        return failure(res)
      } else {
        console.log("Success to upload image to S3", res)
        return success(res)
      }
    })
  }

  handleUploadFile = (bodyOptions, success) => {
    let urlAPI = APIs.UPLOAD_FILE_APPOINTMENT_REQUEST_STEP1 + '/' + this.props.appointment_request_id

    // Upload file underground
    GMGClient.postData(
      urlAPI,
      bodyOptions,
      json => {
        if(json.success) {
          console.log('json.data Upload file: ', json.data)
          return success(json.data)
        }
      },
      error => {
        alert(error)
      }
    )
  }

  handleDeleteFile = (key) => {
    console.log(this.state.filesAttached, '>>> key')
    let filesAttached = this.state.filesAttached
    let urlAPI = 
      APIs.DELETE_FILE_APPOINTMENT_REQUEST_STEP1
      + '/'
      + this.props.appointment_request_id
      + '?user_id='
      + filesAttached[key].userId
      + '&id='
      + filesAttached[key].id

    // Delete file underground
    GMGClient.deleteData(
      urlAPI,
      json => {
        if(json.success) {
          console.log('json.data Delete file: ', json.data)
          filesAttached.splice(key, 1)

          this.setState({
            filesAttached
          })

          this.props.setDocuments(this.state.filesAttached)
        }
      },
      error => {
        alert(error)
      }
    )
  }

  onShowImagePicker = () => {
    const {userId} = this.state

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let arrName = []
        let file = {}
        const contentType = (type) => {
          switch(type) {
            case 'jpg':
              return 'image/jpeg'
              break
            case 'jpeg':
              return 'image/jpeg'
              break
            case 'png':
              return 'image/png'
              break
            case 'gif':
              return 'image/gif'
              break
            case 'bmp':
              return 'image/bmp'
              break
            case 'tiff':
              return 'image/tiff'
              break
            case 'pdf':
              return 'application/pdf'
              break
            case 'doc':
              return 'application/msword'
              break
            case 'docx':
              return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
              break
            case 'xls':
              return 'application/vnd.ms-excel'
              break
            case 'ppt':
              return 'application/vnd.ms-powerpoint'
              break
            case 'pptx':
              return 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
              break
            case 'mp4':
              return 'application/mp4'
              break
            case 'H264':
              return 'video/H264'
              break
            case 'H265':
              return 'video/H265'
              break
            case '3gpp':
              return 'video/3gpp'
              break
          }
        }

        if(Platform.OS === 'ios') {
          let arr = response.uri.split('/')
          arrName = arr[arr.length - 1].split('.')
          file = {
            uri: response.uri,
            name: arr[arr.length - 1],
            type: contentType(arrName[1]),
            file_size: response.fileSize
          }

        } else {
          arrName = response.fileName.split('.')
          file = {
            uri: response.uri,
            name: response.fileName,
            type: response.type,
            file_size: response.fileSize
          }
        }

        // let arrName = response.fileName.split('.')
        let fileName = {
          name: arrName[0],
          type: arrName[1],
          uri: response.uri,
        }

        let source = this.optimizeFile(fileName, true)
        let filesAttached = this.state.filesAttached
        
        if (source) {
          filesAttached.push(source);

          this.setState({
            filesAttached
          })

          this.props.setDocuments(this.state.filesAttached)
        } else {
          console.log('not accepted type')
        }

        this.handleUploadS3(
          file,
          (res) => {
            let bodyOptions = {
              user_id: userId,
              file_url: res.body.postResponse.location,
              file_name: arrName[0] + '.' + arrName[1],
              file_type: arrName[1],
              file_size: file.file_size
            }
            
            this.handleUploadFile(
              bodyOptions,
              json => {
                source.url = json.url
                source.id = json.id
                source.isDataLoading = false
                source.appointment_request_id = json.teleconsult_request_id
                source.userId = json.user_id

                // filesAttached.find(el => el.fileId === fileName.fileId) = source

                this.setState({
                  filesAttached
                })

                this.props.setDocuments(this.state.filesAttached)
              }
            )
          },
          (err) => {
            console.log(err)
          }
        )
      }
    });
  }

  onUploadFile = () => {
    const {userId} = this.state

    DocumentPicker.show({
      filetype: [DocumentPickerUtil.allFiles()],
    },(error,response) => {
      if(response !== null) {
        let arrName = response.fileName.split('.')
        let fileName = {
          name: arrName[0],
          type: arrName[1],
          uri: response.uri,
        }
        let file = {
          uri: response.uri,
          name: response.fileName,
          type: response.type,
          file_size: response.fileSize
        }

        let source = this.optimizeFile(fileName, false)
        let filesAttached = this.state.filesAttached
        
        if (source) {
          filesAttached.push(source);

          this.setState({
            filesAttached
          })

          this.props.setDocuments(this.state.filesAttached)
        } else {
          console.log('not accepted type')
        }

        this.handleUploadS3(
          file,
          (res) => {
            let bodyOptions = {
              user_id: userId,
              file_url: res.body.postResponse.location,
              file_name: response.fileName,
              file_type: response.type,
              file_size: file.file_size
            }
            
            this.handleUploadFile(
              bodyOptions,
              json => {
                source.url = json.url
                source.id = json.id
                source.isDataLoading = false
                source.appointment_request_id = json.teleconsult_request_id
                source.userId = json.user_id

                // filesAttached.find(el => el.fileId === fileName.fileId) = source

                this.setState({
                  filesAttached
                })

                this.props.setDocuments(this.state.filesAttached)
              }
            )
          },
          (err) => {
            console.log(err)
          }
        )
      } else {
        console.log('Cancel upload file')
      }
    });
  }

  renderLoadingIndicator = (isDataLoading) => {
    if (isDataLoading) {
      return (
        <ActivityIndicator
          animating={isDataLoading}
          style={[globalStyles.indicatorPageLoading,]}
          color={Colors.TEXT_SECONDARY}
          size='small'
        />
      )
    } else {
      return null
    }
  }

  renderFilesAttached = () => {
    let filesAttached = this.state.filesAttached
    let contentWidth = DEVICE_WIDTH - DEVICE_WIDTH*0.16 - 30
    let filesEachRow = Math.floor(contentWidth/65)
    let numberRows = Math.ceil(this.state.filesAttached.length/filesEachRow)
    let arrRows =[]
    
    for (let i = 0; i < numberRows; i++) {
      let arr = this.state.filesAttached.slice(filesEachRow*i, filesEachRow*i + filesEachRow)
      arrRows.push(arr)
    }

    return (
      <View>
      {
        arrRows.map((arr, key) => {
          return (
            <View key={ key } style={ [globalStyles.blockInline, {paddingTop: 20}] }>
              {
                arr.map((item, keyItem) => {
                  return (
                    <TouchableHighlight
                      onPress={ () => item.isDataLoading ? null : this.handleDeleteFile(keyItem + key * filesEachRow) }
                      key={ keyItem }
                    >
                      <View style={ [localStyles.blockAction, { width: contentWidth/filesEachRow, position: 'relative' }] }>
                        <Icon size={10} name='remove' type='font-awesome' color={Colors.TEXT_PRIMARY} style={ localStyles.iconRemove } />
                        { item.type !== 'jpg' && item.type !== 'png' && item.type !== 'gif' ? <Icon size={30} name='file-o' type='font-awesome' color={Colors.TEXT_PRIMARY} /> : <Image source={{uri: item.path}} style={{width: 30, height: 30, alignSelf: 'center',}} onLoadStart={(e) => {
                            if(item.isDataLoadingOnLoading) {
                              filesAttached[(filesEachRow * key) + keyItem].isDataLoading = true

                              this.setState({filesAttached})

                              console.log(this.state.filesAttached, 'start load image')
                            }
                          }}
                          onLoadEnd={(e) => { 
                            if(item.isDataLoadingOnLoading) {
                              filesAttached[(filesEachRow * key) + keyItem].isDataLoading = false
                              filesAttached[(filesEachRow * key) + keyItem].isDataLoadingOnLoading = false

                              this.setState({filesAttached})

                              console.log(this.state.filesAttached, 'end load image')
                            }
                          } } /> }
                        
                        <Text style={ [globalStyles.normalText, globalStyles.textCenter] }>
                          {item.name}.{item.type}
                        </Text>
                        {item.isDataLoading ? this.renderLoadingIndicator(item.isDataLoading) : null}
                      </View>
                    </TouchableHighlight>
                  )
                })
              }
            </View>
          )
        })
      }
      </View>
    )
  }

  renderButtonUploadFile = () => {
    return (
      <View style={ [localStyles.blockAction, {flex: 1}] }>
        <Icon
          size={30}
          name="file-text"
          type='font-awesome'
          color={Colors.TEXT_PRIMARY}
          onPress={ () => this.onUploadFile() } />
      </View>
    )
  }

  render() {
    const { label } = this.props

    return (
      <View>
        <Label label={ label } />
        <View style={ [localStyles.attachFilesBox] }>
          <View style={ [globalStyles.blockInline] }>
            <View style={ [localStyles.blockNote, {flex: 4}] }>
              <Text style={ [globalStyles.normalText, {paddingTop: 0}] }>{ I18n.t("appointments.make.stepOne.noteDocuments") }</Text>
            </View>
            <View style={ [localStyles.blockAction, {flex: 1}] }>
              <Icon
                size={30}
                name="camera"
                type='font-awesome'
                color={Colors.TEXT_PRIMARY}
                onPress={ () => this.onShowImagePicker() } />
            </View>
            {Platform.OS === 'android' ? this.renderButtonUploadFile() : null}

          </View>


          { this.state.filesAttached.length > 0 ? this.renderFilesAttached() : <View/> }
        </View>
      </View>
    )
  } 
}
