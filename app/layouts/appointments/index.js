import React, { Component, PropTypes } from 'react'
import { View, Text, ActivityIndicator, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import { navigate } from 'react-navigation'
import moment from 'moment-timezone'

import LocalStorage from '../../components/localStorage'
import ListAppointments from '../../components/listView/appointments'
import GMGClient from '../../configs/gmgHttpClient'

import FormatDate from '../../configs/formatDate'
import { Colors, StorageKeys, APIs } from '../../configs/constants'
import Icons from '../../configs/icons'
import localStyles from './localStyles'
import globalStyles from '../../styles'

class AppointmentsScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isDataLoading: false,
      userId:'',
      listAppointmentConfirmed: [],
    }
  }

  static navigationOptions = ({ navigation }) => ({
    // title: I18n.t('appointments.title'),
    header: null,
    tabBarIcon: Icons.Appointments,
    tabBarPosition: 'top'
  })

  getListAppointments = () => {
    const urlAPI =
      APIs.LIST_APPOINTMENT_READY
      + "?user_id=" + this.state.userId + "&per_page=100000"

    this.setState({ isDataLoading: true })

    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          console.log('json.data: ', json.data)
          this.setState(
            {
              isDataLoading: false,
              listAppointmentConfirmed: json.data.map(item => {
                item.scheduled_time = FormatDate.switchToLocalTime(item.scheduled_time)
                return item
              })
            }
          )
        }
        console.log(this.state)
      },
      error => {
        this.setState({
          isDataLoading: false,
        })
        alert(error)
      }
    )
  }

  renderLoadingIndicator = () => {
    if (this.state.isDataLoading) {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={[globalStyles.indicatorPageLoading, {backgroundColor: '#FFF'}]}
          color={Colors.TEXT_SECONDARY}
          size='large'
        />
      )
    } else {
      return null
    }
  }

  renderAlertAppointment = () => (
    <View style={{paddingTop: 5, paddingLeft:5, paddingRight: 5}}>
      <Text>{ I18n.t('home.isNotAppointment') }</Text>
    </View>
  )

  renderListAppointments = () => (
    <View>
      <Text style={localStyles.title}>
        { I18n.t('appointments.title').toUpperCase() }
      </Text>
      <ListAppointments
        navigation = { this.props.navigation }
        listRef='confirmedList'
        listAppointments = {this.state.listAppointmentConfirmed} />
    </View>
  )

  async componentDidMount() {
    const userId = await LocalStorage.getString(StorageKeys.USERID)
    console.log('>>>>>>>>>>>>>>>>>>>>>>> Appointments')
    this.setState({userId})
    this.getListAppointments()


  }

  /*componentWillReceiveProps(nextProps) {
    console.log('>>>>>>>>> ' + this.state.checkIn ? 'IN' : 'OUT')
  }*/

  render() {
    const {language, /*listAppointmentConfirmed*/} = this.props
    const {navigate, setParams} = this.props.navigation
    // console.log('>>>>>>>>>>>>>>>>><<<<<<<<<<<< ', listAppointmentConfirmed)

    return (
      <View style={ localStyles.wrapper }>
        {this.renderLoadingIndicator()}

        <ScrollView>
          { this.state.listAppointmentConfirmed.length === 0 && !this.state.isDataLoading
            ? this.renderAlertAppointment()
            : this.renderListAppointments()
          }
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language,
    // listAppointmentConfirmed: state/*.updateAPIs.listAppointmentConfirmed*/
  }
}

export default connect(mapStateToProps, null)(AppointmentsScreen)
