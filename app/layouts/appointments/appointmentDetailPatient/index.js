import React from 'react'
import {View, ActivityIndicator, StyleSheet, ScrollView, Text, BackHandler} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {Button} from 'react-native-elements'
import { NavigationActions, HeaderBackButton } from 'react-navigation'
import moment from 'moment-timezone'
import FormatDate from '../../../configs/formatDate'
import GMGClient from '../../../configs/gmgHttpClient'
import { APIs, Colors, SPACE, FontSizes } from '../../../configs/constants'
import Card from '../../../components/Card'
import  TopLabelField from '../../../components/form/TopLabelField'
import { ErrorMes, Label } from '../../../components/form/Field'
import  AttachFiles from '../makeAppointment/attachFiles'
import  PopupCancelAppointment from '../../../components/popup/cancelAppointment'
import globalStyles from '../../../styles'

const backAction = NavigationActions.back()

class AppointmentDetailPatient extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      navigateHome: false,
      isDataLoading: true,
      typeAppointment: "",
      checkInitDocumentsEmpty: false,
      data: {
        doctor_id: 0,
        doctor_name: "",
        doctor_image: "",
        status: "",
        doctor_response_type: "",
        doctor_speciality: "",
        symptoms: "",
        drug_history: "",
        drug_allergies: "",
        medical_history: "",
        report_and_document:[]
      },
      describeYourSymptoms: "",
      medicalHistory: "",
      drugAllergies: "",
      drugHistory: "",
      documents: [],
      isShowMes: false,
      mesType: "",
      errorMes: "",
      warningMes: "Please type something to field you want update",
      successMes: "Appointment information updated",
      modalVisible: false
    }
    this._handleBackPress = this._handleBackPress.bind(this);
  }

  static navigationOptions = ({navigation}) => {
    const {state, setParams} = navigation;
    const navigateHome = state.params.navigateHome;
    console.log(navigateHome);
    return {
      title: I18n.t('appointmentDetailPatient.title'),
      headerLeft: <HeaderBackButton onPress={() => {
        if (!navigateHome) {
          // alert(navigateHome)
          navigation.dispatch(backAction)
        }else {
          navigation.navigate('Home')
          // alert(navigateHome)
        }
      }}
      tintColor = '#FFFFFF'
      />
    };
  }

  setDocuments = (documents) => {
    this.setState({
      documents
    })

    console.log(this.state.documents, '||||||', this.state.data.report_and_document)
  }

  handleGetAppointmentInfo = () => {
    const {params} = this.props.navigation.state
    // this.setState({navigateHome: params.navigateHome})
    if (params.navigateHome === undefined) {
      this.setState({navigateHome: false})
    }else {
      this.setState({navigateHome: params.navigateHome})
    }
    console.log(params)
    let urlAPI =  APIs.LIST_APPOINTMENT_REQUEST
      + '/' + params.appointment_request_id
      + '?user_id=' + params.user_id

    this.getAppointmentWaiting(urlAPI)
    // console.log(urlAPI)
  }

  handleUpdateInfo = () => {
    const state = this.state

    if(state.describeYourSymptoms !== state.data.symptoms
      || state.drugAllergies !== state.data.drug_allergies
      || state.drugHistory !== state.data.drug_history
      || state.medicalHistory !== state.data.medical_history)
    {
      this.onChangeAppointment()
    } else {
      if((this.state.checkInitDocumentsEmpty && state.documents.length > 0)
        || (!this.state.checkInitDocumentsEmpty && state.documents !== state.data.report_and_document)) {
        this.onChangeAppointment()
      } else {
        console.log("NOT CHANGE DATA =>> DON'T UPDATE")
        this.setState({
          isShowMes: true,
          mesType: "WARNING"
        })
      }

    }
  }

  handleMes = () => {
    console.log("HANDLE CONTENT MESSAGE")
    switch (this.state.mesType) {
      case "SUCCESS":
        return this.state.successMes
      case "WARNING":
        return this.state.warningMes
      default:
        return this.state.errorMes
    }
  }


  onPressBtnStart = async () => {
    console.log("PRESS BTN START!!!!!!!!!!!!")
    console.log(this.props.navigation.state.params)
    this.props.navigation.navigate("VideoCall", {
      appointment_request_id: this.state.data.appointment_request_id,
      user_id: this.state.data.user_id,
      appointment_id: this.state.data.appointment.id,
    })
  }

  onPressBtnCancel = () => {
    this.setModalVisible(true)
  }

  setModalVisible(visible) {
    this.setState({
        modalVisible: visible
    });
  }

  callAPICancelAppointment = (reason) => {
    let urlAPI = APIs.CANCEL_APPOINTMENT_REQUEST
      + '/'
      + this.state.data.appointment_request_id
    let bodyData = {
      user_id: this.state.data.user_id,
      reason: reason
    }

    GMGClient.postData(
      urlAPI,
      bodyData,
      (json) => {
        if(json.success) {
          this.props.navigation.navigate("Home")
          // console.log(json, ' >>>>>>>>>> Cancel success')
        }
      },
      (error) => {
        console.log(error)
      }
    )
  }

  getAppointmentWaiting = (urlAPI) => {
    this.setState({isDataLoading: true})
    GMGClient.getData(
      urlAPI,
      json => {
        this.setState(
          {
            isDataLoading: false,
            typeAppointment: json.data.doctor_response_type,
            data: json.data,
            describeYourSymptoms: json.data.symptoms,
            medicalHistory: json.data.medical_history,
            drugAllergies: json.data.drug_allergies,
            drugHistory: json.data.drug_history,
            documents: json.data.report_and_document,
            checkInitDocumentsEmpty: json.data.report_and_document.length === 0 ? true : false
          }
        )

        // console.log(this.state, 'length')
      },
      error => {
        alert(error)
      }
    )
  }

  onChangeAppointment = () => {
    console.log('update appointment waiting')
    const {params} = this.props.navigation.state
    const state = this.state
    const urlAPI = APIs.UPDATE_APPOINTMENT_REQUEST_STEP1 + '/' + params.appointment_request_id
    const body = {
      symptoms: state.describeYourSymptoms,
      drug_history: state.drugHistory,
      drug_allergies: state.drugAllergies,
      medical_history: state.medicalHistory
    }

    this.setState({isDataLoading: true, isShowMes: false})

    GMGClient.putData(
      urlAPI,
      body,
      json => {
        if(json.success) {
          // this.setState({isDataLoading: false})
          // this.setState( prevState =>
          // {
          //     isDataLoading: false,
          //     data: {
          //       ...prevState.data,
          //       symptoms: this.state.describeYourSymptoms,
          //       drug_history: this.state.drugHistory,
          //       drug_allergies: this.state.drugAllergies,
          //       medical_history: this.state.medicalHistory
          //     },
          //     isShowMes: true,
          //     mesType: "SUCCESS"
          //   }
          // )

          // console.log(this.state, 'this.state')

          if(this.state.navigateHome){
            this.props.navigation.navigate('Home')
          }else {
            this.props.navigation.dispatch(backAction)
          }
        }
      },
      error => {
        this.setState({isDataLoading: false})
        alert(error)
      }
    )
  }

  renderLoadingIndicator = () => {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={[globalStyles.indicatorPageLoading, {backgroundColor: '#FFF'}]}
          color={Colors.TEXT_SECONDARY}
          size='large'
        />
      )
  }

  renderTimeSchedule = () => {
    const state = this.state

    switch (state.typeAppointment) {
      case "Accepted":
        return (
          <View style={{ paddingLeft: 10 }}>
            <Text style={ [globalStyles.normalText]}>
              {FormatDate.switchToLocalTime(state.data.appointment.scheduled_time)}
            </Text>
          </View>
        )
        break
      case "Awaiting Response":
        return (
          <View style={{ paddingLeft: 10 }}>
            <Text style={ [globalStyles.normalText] }>
              {FormatDate.switchToLocalTime(state.data.timeslot1.date)}
            </Text>
            <Text style={ [globalStyles.normalText] }>
              {FormatDate.switchToLocalTime(state.data.timeslot2.date)}
            </Text>
            <Text style={ [globalStyles.normalText] }>
              {FormatDate.switchToLocalTime(state.data.timeslot3.date)}
            </Text>
          </View>
        )
        break
      default:
        return console.log("===================CHECK TYPE APPOINTMENT===================")
    }
  }

  renderContainer = () => {
    const state = this.state
    const {params} = this.props.navigation.state
    let titleTime = ''

    switch(state.data.doctor_response_type || state.data.status) {
      case 'Awaiting Response':
        titleTime = I18n.t('appointments.summary.status.chosenTimeslot').toUpperCase()
        break
      case 'Accepted':
        titleTime = I18n.t('appointments.summary.status.scheduledTime').toUpperCase()
        break
      case 'Past':
        titleTime = I18n.t('appointments.summary.status.duration').toUpperCase()
        break
      default:
        titleTime = I18n.t('appointments.summary.status.duration').toUpperCase()
        break
    }

    return (
      <ScrollView keyboardShouldPersistTaps='handled'>
        <PopupCancelAppointment
          modalVisible={this.state.modalVisible}
          setModalVisible={this.setModalVisible.bind(this)}
          callAPICancelAppointment={this.callAPICancelAppointment.bind(this)} />
        <Card
          userImage={ state.data.doctor_image }
          heading={ state.data.doctor_name }
          subHeading={ state.data.doctor_speciality }
          btnStart={ state.data.doctor_response_type === "Accepted" }
          onPressBtnStart={ this.onPressBtnStart }
          btnCancel={ state.data.doctor_response_type === "Awaiting Response" }
          onPressBtnCancel = { this.onPressBtnCancel }
        />
        <View>
          {state.typeAppointment === 'Awaiting Response'
            ? <View style={{paddingTop: 10, paddingHorizontal: SPACE}}>
            <Text style={globalStyles.redText}>
            { I18n.t("appointmentDetailPatient.noted") }
            </Text></View>
            : null
          }

          <View style={ [globalStyles.blockgreyDetail, { paddingHorizontal: SPACE }] }>
            <Label label={ titleTime } style={{ marginBottom: 0, paddingLeft: 10, paddingRight: 10, fontSize: FontSizes.BIG }} />
            {this.renderTimeSchedule()}
          </View>
        </View>
        <View style={ localStyles.container }>
          <TopLabelField
            label={ I18n.t("appointmentDetailPatient.describeYourSymptoms") }
            value={ this.state.describeYourSymptoms }
            editable={ true }
            multiline={ true }
            numberOfLines={ 4 }
            onChangeText={ describeYourSymptoms => this.setState({describeYourSymptoms}) }
            inputRef={ node => this.inputSymptoms = node }
            returnKeyType="next"
            onSubmitEditing={ () => {
            } }
          />
          <TopLabelField
            label={ I18n.t("appointmentDetailPatient.medicalHistory") }
            value={ this.state.medicalHistory }
            editable={ true }
            multiline={ true }
            numberOfLines={ 4 }
            onChangeText={ medicalHistory => this.setState({medicalHistory}) }
            inputRef={ node => this.medicalHistory = node }
            returnKeyType="next"
            onSubmitEditing={ () => {
            } }
          />
          <TopLabelField
            label={ I18n.t("appointmentDetailPatient.drugHistory") }
            value={ this.state.drugHistory }
            editable={ true }
            multiline={ true }
            numberOfLines={ 4 }
            onChangeText={ drugHistory => this.setState({drugHistory}) }
            inputRef={ node => this.drugHistory = node }
            returnKeyType="next"
            onSubmitEditing={ () => {
            } }
          />
          <TopLabelField
            label={ I18n.t("appointmentDetailPatient.drugAllergies") }
            value={ this.state.drugAllergies }
            editable={ true }
            multiline={ true }
            numberOfLines={ 4 }
            onChangeText={ drugAllergies => this.setState({drugAllergies}) }
            inputRef={ node => this.drugAllergies = node }
            returnKeyType="next"
            onSubmitEditing={ () => {
            } }
          />
          <AttachFiles
            label={ I18n.t("appointmentDetailPatient.reportsAndDocument") }
            documents={ this.state.documents }
            setDocuments={ this.setDocuments.bind(this) }
            appointment_request_id={state.data.appointment_request_id}
          />
          { this.state.isShowMes ? <ErrorMes mesType = { this.state.mesType } errorMes = { this.handleMes() } arrow = { false } styleContainer={{marginTop: 25, marginBottom: 0}}/> : null }
          <Button
            title={ I18n.t("appointmentDetailPatient.btnUpdate").toUpperCase() }
            onPress={ () => this.handleUpdateInfo() }
            buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
            containerViewStyle={ [globalStyles.buttonView, { marginTop: 25 }] }
          />
          <Button
            title={ I18n.t("appointmentDetailPatient.btnBack").toUpperCase() }
            onPress={ this.state.navigateHome ? () => this.props.navigation.navigate('Home') : () => this.props.navigation.dispatch(backAction) }
            buttonStyle={ [globalStyles.buttonGrey, globalStyles.button] }
            containerViewStyle={ [globalStyles.buttonView, { marginBottom: 15 }] }
          />
        </View>
      </ScrollView>
    )
  }
  _handleBackPress = () =>{
    if (this.state.navigateHome) {
        this.props.navigation.navigate('Home');
        BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
        return true;
      }
      return false;
  }

  componentDidMount() {
    console.log("PARAMS:")
    console.log(this.props.navigation.state.params)

    this.handleGetAppointmentInfo();
    BackHandler.addEventListener('hardwareBackPress', this._handleBackPress);
  }

  // componentWillUnmount() {
  //   BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
  // }

  render() {
    const {params} = this.props.navigation.state

    return (
      <View style={ localStyles.wrapper }>
        { this.state.isDataLoading ? this.renderLoadingIndicator() : this.renderContainer() }
      </View>
    )
  }
}

const localStyles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  container: {
    paddingHorizontal: SPACE,
    paddingVertical: 15
  }
})

const mapStateToProps = state => ({
  language: state.settings.language
})

export default connect(mapStateToProps, null)(AppointmentDetailPatient)
