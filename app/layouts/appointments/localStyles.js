import { StyleSheet } from 'react-native'
import { Colors, SPACE, FontSizes } from '../../configs/constants'

const localStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_MAIN_SCREEN,
  },
  title: {
    paddingTop: 20,
    paddingLeft: 10,
    color: Colors.TEXT_SECONDARY,
  },
  paddingContainer: {
    paddingLeft: 30,
    paddingRight: 30,
  },
  container: {
    // paddingHorizontal: SPACE,
    // paddingVertical: 15,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 15
  },
  thankyouContainer: {
    paddingHorizontal: SPACE,
    marginTop: 15,
  },
  figure: {
      alignItems: 'center',
      marginBottom: 30,
  },
  logo: {
      width: 120,
      height: 45,
      resizeMode: 'contain',
  },
  content: {
    alignItems: 'center',
  },
  textBig:{
    fontSize: FontSizes.SUPER_HUGE,
  },
  textSmall: {
    marginBottom: 30,
    marginTop: 30,
    textAlign: 'center'
  },
  button: {
    marginLeft: 0, marginRight: 0,
  },
  buttonBlue: {
    backgroundColor: Colors.BACKGROUND_BUTTON_BLUE,
    height: 35,
    borderRadius: 4,
    marginBottom: 10,
  },
  buttonGray: {
    backgroundColor: Colors.BACKGROUND_BUTTON_GREY,
    height: 35,
    borderRadius: 4,
    marginBottom: 50,
  }
})

/** export module */
module.exports = localStyles
