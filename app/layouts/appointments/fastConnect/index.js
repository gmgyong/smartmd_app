import React, {Component} from 'react'
import {View, Text, Dimensions, ScrollView} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {NavigationActions} from 'react-navigation'
import  TopLabelField from '../../../components/form/TopLabelField'
import {Button} from 'react-native-elements'
import { Label, ErrorMes, TextField } from '../../../components/form/Field'
import StepIndicator from 'react-native-step-indicator';
import ConfirmPayment from './confirmPayment'
import ThankYou from './thankYou'

import GMGClient from '../../../configs/gmgHttpClient'
import {Colors, APIs} from '../../../configs/constants'
import localStyles from '../localStyles'
import globalStyles from '../../../styles'

const DEVICE_WIDTH = Dimensions.get('window').width;


class FastConnectScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentPage: 0,
    }
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle: I18n.t('appointments.fastConnect.header').toUpperCase(),
  })

  componentWillMount() {

  }

  componentDidMount() {

  }
  setCurrentPage = (position) => {
    this.setState({currentPage: position})
  }

  render() {
    const switchStep = (pageID) => {
      switch (pageID) {
        case 0:
          return (
            <ConfirmPayment
              setCurrentPage={this.setCurrentPage.bind(this)}
              navigation={ this.props.navigation}
            />
          )
          break
        case 1:
          return (
            <ThankYou
              setCurrentPage={this.setCurrentPage.bind(this)}
              navigation={ this.props.navigation}
            />
          )
          break
      }
    }
    return (
      <View style={localStyles.wrapper}>
        { switchStep(this.state.currentPage) }
      </View>
    )
  }
}

const backAction = NavigationActions.back()

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}


export default connect(mapStateToProps, null)(FastConnectScreen)
