import React, {Component, PropTypes} from 'react'
import {View, Text, Dimensions, ScrollView, Image, BackHandler} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {NavigationActions} from 'react-navigation'
import  TopLabelField from '../../../components/form/TopLabelField'
import {Button} from 'react-native-elements'
import { Label, ErrorMes, TextField } from '../../../components/form/Field'
import images from '../../../configs/images'

import GMGClient from '../../../configs/gmgHttpClient'
import {Colors, APIs} from '../../../configs/constants'
import localStyles from '../localStyles'
import globalStyles from '../../../styles'

const DEVICE_WIDTH = Dimensions.get('window').width;

export default class ThankYouScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle: I18n.t('appointments.fastConnect.header').toUpperCase(),
    headerLeft: null
  })

  detailAppointment = () =>{
      // navigate('AppointmentDetailPatient',{user_id: data.patient_id, appointment_request_id: data.appointment_request_id});
        // const {navigate} = this.props.navigation
      const {params} = this.props.navigation.state;
      this.props.navigation.navigate("AppointmentDetailPatient", {user_id: params.patient_id, appointment_request_id: params.appointment_request_id, navigateHome: true})
  }

  handleBackPressHome = () =>{
      this.props.navigation.navigate('Home');
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPressHome);
      return true;
  }

  // componentWillMount() {
  //   BackHandler.removeEventListener('hardwareBackPress', this.handleBackPressHome);
  // }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPressHome);
  }


  render() {
    return (
        <View style={ localStyles.wrapper }>
          <ScrollView keyboardShouldPersistTaps='handled'>
            <View style={ localStyles.thankyouContainer }>
              <View style={ localStyles.figure }>
                <Image source={ images.logo } style={ localStyles.logo }/>
              </View>
              <View style={ localStyles.content }>
                <Text style={ localStyles.textBig }>
                  { I18n.t("appointments.fastConnect.thanksText") }
                </Text>
                <Text style={ localStyles.textSmall }>
                  { I18n.t("appointments.fastConnect.thanksDesc") }
                </Text>
              </View>

              <Button
                title={ I18n.t("appointments.fastConnect.thanksBtn").toUpperCase() }
                buttonStyle={ localStyles.buttonBlue }
                containerViewStyle={ localStyles.button }
                onPress={ this.detailAppointment }
              />
            </View>
          </ScrollView>
        </View>
    )
  }
}

const backAction = NavigationActions.back()

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

// export default connect(mapStateToProps, null)(ThankYouScreen)
