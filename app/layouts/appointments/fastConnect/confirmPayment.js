import React, {Component, PropTypes} from 'react'
import {View, Text, Dimensions, ScrollView, ActivityIndicator, Alert, Keyboard} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {NavigationActions} from 'react-navigation'
import  TopLabelField from '../../../components/form/TopLabelField'
import {Button} from 'react-native-elements'
import { Label, ErrorMes, TextField } from '../../../components/form/Field'

import GMGClient from '../../../configs/gmgHttpClient'
import {Colors, APIs, StorageKeys} from '../../../configs/constants'
import FormatDate from '../../../configs/formatDate'
import LocalStorage from '../../../components/localStorage'
import localStyles from '../localStyles'
import globalStyles from '../../../styles'

const DEVICE_WIDTH = Dimensions.get('window').width;


class ConfirmPaymentScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isSwitchBtn: false,
      isUpdate: undefined,
      id : undefined,
      patientName: '',
      doctorName: '',
      appointmentTime: '',
      patient_id:'',
      appointment_request_id: '',
      previous: {
        name: {
          value: '',
          error: false
        },
        number: {
          value: '',
          error: false
        },
        expiration: {
          value: '',
          error: false
        },
        CVV: {
          value: '',
          error: false
        }
      },
      name: {
        value: '',
        error: false
      },
      number: {
        value: '',
        error: false
      },
      expiration: {
        value: '',
        error: false
      },
      CVV: {
        value: '',
        error: false
      },
      errorAPI: {
        value: '',
        error: false,
        mesType: 'SUCCESS'
      }
    }
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle: I18n.t('appointments.fastConnect.header').toUpperCase(),
  })

  handleNextStep = (position) => {
    this.props.setCurrentPage(position)
  }

  componentWillMount() {

  }

  componentDidMount() {
    this.isGetFastConnect()
    this.onGetInfoCreditCard()
  }

  setLoadingProgress = (isDataLoading) => {
    this.setState({isDataLoading});
  }

  renderLoadingIndicator = (isShowing) => {
    if (isShowing) {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={globalStyles.indicatorPageLoading}
          color={Colors.TEXT_SECONDARY}
          size='small'/>
      )
    } else {
      return null;
    }
  }

  isGetFastConnect = () => {
    const {params} = this.props.navigation.state
    console.log(params);
    // let urlAPI = APIs.FAST_CONNECT + '/' + 207 + '?'
    let urlAPI = APIs.FAST_CONNECT + '/' + params.idFastConnectDetails + '?'
    GMGClient.getData(
      urlAPI,
      json => {
          if(json.success){
            this.setState({
              patientName: json.data.patient_name,
              doctorName : json.data.doctor_name,
              appointmentTime : json.data.appointment_time,
            })
          }
      },
      (error) => {
        console.log("error");
      }
    )
  }

  onGetInfoCreditCard = async () =>{
    this.setLoadingProgress(true);
    const userId = await LocalStorage.getString(StorageKeys.USERID)
    this.setState({id : userId})
    const urlAPI = APIs.CREDIT_CARD + '/' + userId + '?'
    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success){
          this.setLoadingProgress(false);
          this.setState({
            isUpdate: false,
            previous:{
              name : {
                value: json.data.card_name,
                error: false
              },
              number : {
                value: '****-****-****-' + json.data.last4,
                error: false
              },
              expiration : {
                value: json.data.expiry,
                error: false
              },
              CVV : {
                value: '***',
                error: false
              }
            }
          })
          console.log("isUpdate:", this.state.isUpdate, "/ (isUpdate) ? renderFormInputCardInfo : renderCardView ")
        }
      },
      (error) => {
        this.setState({isUpdate: true})
        console.log("isUpdate:", this.state.isUpdate, "/ (isUpdate) ? renderFormInputCardInfo : renderCardView ")
      }
    )
  }

  onAcceptFastConnect = async () => {
    this.setLoadingProgress(true);
    const userId = await LocalStorage.getString(StorageKeys.USERID)
    const {params} = this.props.navigation.state
    let urlAPI = APIs.ACCEPT_FAST_CONNECT + "/" + params.idFastConnectDetails
    let bodyData = {
      "user_id" : userId
    }
    console.log(bodyData);
    GMGClient.postData(
      urlAPI,
      bodyData,
      (json) => {
        if(!json.success) {
          this.setState({
            errorAPI: {
              error: true,
              value: json.status_code + " " + json.message,
              mesType: 'ERROR'
            }
          })
          this.setLoadingProgress(false);
        } else {
          this.setLoadingProgress(false);
          this.props.navigation.navigate("ThankYou", {patient_id: json.data.patient_id, appointment_request_id: json.data.id})
        }
      },
      (error) => {
        this.setState({
          errorAPI: {
            error: true,
            value: "Request timeout, Please try again",
            mesType: 'ERROR'
          }
        })
        this.setLoadingProgress(false);
      }
    )
  }

  onUpdateCreditCard = async () => {
    Keyboard.dismiss()
    this.setLoadingProgress(true);
    const state = this.state
    let urlAPI = APIs.CREDIT_CARD + '/' + state.id
    let bodyData = {
      "card_name": state.name.value,
      "card_number": state.number.value,
      "expiry": state.expiration.value,
      "cvv": state.CVV.value
    }

    console.log(bodyData)
    console.log(this.state)

    GMGClient.postData(
      urlAPI,
      bodyData,
      (json) => {
        if(!json.success) {
          this.setState({
            errorAPI: {
              error: true,
              value: json.status_code + " " + json.message,
              mesType: 'ERROR'
            }
          })
          this.setLoadingProgress(false);
        }else if (this.state.isSwitchBtn) {
          this.setState({
            isUpdate: false,
            isSwitchBtn: false,
            previous:{
            name : {
              value: json.data.card_name,
              error: false
            },
            number : {
              value: '****-****-****-' + json.data.last4,
              error: false
            },
            expiration : {
              value: json.data.expiry,
              error: false
            },
            CVV : {
              value: '***',
              error: false
            }
          }
          })
          this.setLoadingProgress(false);
        }
        else {
          this.onAcceptFastConnect();
        }
      },
      (error) => {
        this.setState({
          errorAPI: {
            error: true,
            value: "Unvalid card credit information",
            mesType: 'ERROR'
          }
        })
        this.setLoadingProgress(false);
      }
    )
  }

  handleCancelFastConnect = () => {
    // this.props.navigation.dispatch(backAction)
    Alert.alert(
    I18n.t("appointments.fastConnect.confirmation").toUpperCase(),
    I18n.t("appointments.fastConnect.deleteInvitation") + "?",
    // 'Delete this invitation?',
    [ {text: I18n.t("appointments.fastConnect.alertYesBtn").toUpperCase(), onPress: this.callAPIDeleteFastConnect },
      {text: I18n.t("appointments.fastConnect.alertNoBtn").toUpperCase(), onPress: () => console.log('No Pressed'), style: 'cancel'}
    ],
    { cancelable: false }
    )
  }

  callAPIDeleteFastConnect = async () => {
    this.setLoadingProgress(true);
    const {params} = this.props.navigation.state
    console.log("id>>>>>>>>>>>>>", params.idFastConnectDetails);
    let urlAPI = APIs.CANCEL_FAST_CONNECT + "/" + params.idFastConnectDetails + "?"
    GMGClient.deleteData(
      urlAPI,
      (json) => {
        if(!json.success) {
          this.setState({
            errorAPI: {
              error: true,
              value: json.status_code + " " + json.message,
              mesType: 'ERROR'
            }
          })
          this.setLoadingProgress(false);
        } else {
          this.setLoadingProgress(false);
          Alert.alert(
          I18n.t("appointments.fastConnect.successfully").toUpperCase(),
          I18n.t("appointments.fastConnect.invitationSuccess") + "!",
          // 'Delete invitation successfully!',
          [
            {text: I18n.t("appointments.fastConnect.alertOkBtn").toUpperCase(), onPress: () => this.props.navigation.navigate('Home')}
          ],
          { cancelable: false }
          )
        }
      },
      (error) => {
        this.setState({
          errorAPI: {
            error: true,
            value: "System error, please try again",
            mesType: 'ERROR'
          }
        })
        this.setLoadingProgress(false);
      }
    )
  }

  handleUpdateCreditCard = () => {

    const {navigate} = this.props.navigation
    let checkValidate = this.validation()

    if (checkValidate) {
      this.onUpdateCreditCard()
      // this.onAcceptFastConnect()
      console.log("***** Call function UpdateCreaditCardInfo (Post API) in here *****");
    }
  }

  validation = () => {
    let check = true;

    if( this.state.name.value === '' || this.state.name.value === undefined ) {
      this.setState({ name: { error: true, value: this.state.name.value }});
      check = false;
    } else {
      this.setState({ name: { error: false, value: this.state.name.value }});
    }

    if( this.state.number.value === '' || this.state.number.value === undefined || this.state.number.value.length < 19) {
      this.setState({ number: { error: true, value: this.state.number.value }});
      check = false;
    } else {
      this.setState({ number: { error: false, value: this.state.number.value }});
    }

    if( this.state.expiration.value === '' || this.state.expiration.value === undefined || this.state.expiration.value.length < 7 ) {
      this.setState({ expiration: { error: true, value: this.state.expiration.value }});
      check = false;
    } else {
      this.setState({ expiration: { error: false, value: this.state.expiration.value }});
    }

    if( this.state.CVV.value === '' || this.state.CVV.value === undefined || this.state.CVV.value.length < 3 ) {
      this.setState({ CVV: { error: true, value: this.state.CVV.value }});
      check = false;
    } else {
      this.setState({ CVV: { error: false, value: this.state.CVV.value }});
    }

    return check;
  }

  updateFormatExpiration = (value) => {
    value = value.split("/").join("").slice(0,6).replace(/\D/g,"")
    if (value.length > 0) {
      value = value.match(new RegExp('.{1,4}$|.{1,2}', 'g')).join("/")
    }
    return value
  }

  handleCheckValue = (value, state) => {
    let error = value !== '' ? false : true
    switch (state) {
      case 'name':
        this.setState({
          name: {
            value,
            error
          }
        })
        break;
      case 'number':
        value = value.split("-").join("").slice(0,16).replace(/\D/g,"")
        if(value.length > 0) {
          value = value.match(/\d{1,4}/g).join("-")
        }
        this.setState({
          number: {
            value,
            error
          }
          })
          break;
        case 'expiration':
          value = this.updateFormatExpiration(value)
          this.setState({
            expiration: {
              value,
              error
            }
          })
          break;
        case 'CVV':
          value = value.split("/").join("").slice(0,3).replace(/\D/g,"")
          this.setState({
            CVV: {
              value,
              error
            }
          })
          break;
    }
  }

  renderCardView = () => {
    return(
      <View style={{ marginTop: 5, }}>
        <Label label={ I18n.t("appointments.make.stepThree.card.name.title") + ' *' } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText, { paddingBottom: 10 }] }>
          {this.state.previous.name.value}
        </Text>
        <Label label={ I18n.t("appointments.make.stepThree.card.number.title") + ' *' } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText, { paddingBottom: 10 }] }>
          {this.state.previous.number.value}
        </Text>
        <Label label={ I18n.t("appointments.make.stepThree.card.validAndCVV.title") + ' *' } style={{ marginBottom: 0 }} />
        <Text style={ [globalStyles.normalText, { paddingBottom: 20 }] }>
          {this.state.previous.expiration.value}  -  {this.state.previous.CVV.value}
        </Text>
      </View>
    )
  }

  renderFormInputCardInfo = () => {
    return(
      <View>
      <TopLabelField
        label={ I18n.t("appointments.make.stepThree.card.name.title") + ' *' }
        placeholder={ I18n.t("appointments.make.stepThree.card.name.placeholder") }
        errorMes={ I18n.t("appointments.make.stepThree.card.name.errorMes") }
        returnKeyType="done"
        error={ this.state.name.error }
        value = {this.state.name.value}
        onChangeText= {(value) => this.handleCheckValue(value, 'name')}
      />

      <TopLabelField
        label={ I18n.t("appointments.make.stepThree.card.number.title") + ' *' }
        placeholder={ I18n.t("appointments.make.stepThree.card.number.placeholder") }
        errorMes={ I18n.t("appointments.make.stepThree.card.number.errorMes") }
        error={ this.state.number.error }
        value = {this.state.number.value}
        onChangeText= {(value) => this.handleCheckValue(value, 'number')}
      />

      <View style={{marginBottom: 20}}>
        <Label label={ I18n.t("appointments.make.stepThree.card.validAndCVV.title") + ' *' } error={ this.state.expiration.error || this.state.CVV.error } />
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 3}}>
            <TextField
              placeholder={ I18n.t("appointments.make.stepThree.card.validAndCVV.placeholder.expiration") }
              error={ this.state.expiration.error }
              value = {this.state.expiration.value}
              onChangeText= {(value) => this.handleCheckValue(value, 'expiration')}
            />
          </View>
          <View style={{flex: 2, marginLeft: 10}}>
            <TextField
              placeholder={ I18n.t("appointments.make.stepThree.card.validAndCVV.placeholder.CVV") }
              error={ this.state.CVV.error }
              value = {this.state.CVV.value}
              onChangeText= {(value) => this.handleCheckValue(value, 'CVV')}
            />
          </View>
          <View style={{flex: 3, marginLeft: 10}}>
          </View>
        </View>
        { (this.state.expiration.error || this.state.CVV.error) ? <ErrorMes errorMes={ I18n.t("appointments.make.stepThree.card.validAndCVV.errorMes") } styleContainer={{marginTop: 15, marginBottom: 0}} /> : null }
      </View>

      </View>
    )
  }

  handeleShowHideBtn = () => {
    this.setState({
      isUpdate: true,
      isSwitchBtn: true,
      name: {
        value: '',
        error: false
      },
      number: {
        value: '',
        error: false
      },
      expiration: {
        value: '',
        error: false
      },
      CVV: {
        value: '',
        error: false
      },
      errorAPI: {
        value: '',
        error: false,
        mesType: 'SUCCESS'
      }
    })
  }
  handleBack = () => {
    this.setState({isUpdate: false, isSwitchBtn: false, errorAPI:{error: false}})
  }


  render() {
    return (
      <View style={ localStyles.wrapper }>
        <ScrollView keyboardShouldPersistTaps='handled'>
          <View style={ localStyles.thankyouContainer }>
            <Text>
              <Text>{ I18n.t("appointments.fastConnect.helloGuys") }</Text>
              <Text> {this.state.patientName},</Text>
            </Text>
            <Text>
              { I18n.t("appointments.fastConnect.invited") }
            </Text>
            <Text>
              <Text>{ I18n.t("appointments.fastConnect.doctorName") }</Text>
              <Text style={{fontWeight: 'bold'}}> {this.state.doctorName}</Text>
            </Text>
            <Text>
              <Text>{ I18n.t("appointments.fastConnect.onDate") }</Text>
              <Text style={{fontWeight: 'bold'}}> { FormatDate.switchToLocalTime(this.state.appointmentTime)}</Text>
            </Text>
            <Text style={{marginTop: 10, marginBottom: 10}}>
              { I18n.t("appointments.fastConnect.description") }
            </Text>

            { this.state.isUpdate ? this.renderFormInputCardInfo() : this.renderCardView() }
            { (this.state.errorAPI.error) ? <ErrorMes  mesType = { this.state.errorAPI.mesType } errorMes={ this.state.errorAPI.value } arrow={ false}/> : null}

            { this.state.isUpdate ? null :
            <Button
              title= { I18n.t("buttons.btnUpdateCreditCard").toUpperCase() }
              buttonStyle={ localStyles.buttonBlue }
              containerViewStyle={ localStyles.button }
              onPress={ this.handeleShowHideBtn }
            />
            }

            <Button
              title={ this.state.isUpdate ? I18n.t("account.payment.btnUpdate.btnText") : I18n.t("appointments.fastConnect.btnAccept").toUpperCase() }
              buttonStyle={ localStyles.buttonBlue }
              containerViewStyle={ localStyles.button }
              onPress={ this.state.isUpdate ?
              () => this.handleUpdateCreditCard() :
              () => this.onAcceptFastConnect()
              }
            />

            <Button
              title={ this.state.isSwitchBtn ? I18n.t("buttons.btnBack").toUpperCase() : I18n.t("appointments.fastConnect.btnCancel").toUpperCase() }
              buttonStyle={ localStyles.buttonGray }
              containerViewStyle={ localStyles.button }
              onPress={ this.state.isSwitchBtn ? this.handleBack : this.handleCancelFastConnect }
            />
          </View>
        </ScrollView>
        {this.renderLoadingIndicator(this.state.isDataLoading)}
      </View>
    )
  }
}

const backAction = NavigationActions.back()

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

export default connect(mapStateToProps, null)(ConfirmPaymentScreen)
