import React, { Component } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import { navigate } from 'react-navigation'
import {Button} from 'react-native-elements'

import Icons from '../../../configs/icons'
import localStyles from './localStyles'
import globalStyles from '../../../styles'

const videoAPI = {
  apiKey: '45914602',
  sessionId: '2_MX40NTkxNDYwMn5-MTUwMDQzMzc0MTQxMn5teGNtRGpqVmUrYXBNRlc5UmJkRTVHWnd-fg',
  tokenPublisher:  'T1==cGFydG5lcl9pZD00NTkxNDYwMiZzaWc9OWVmMmVmZDM1MWZhZWYzNTcyOWFhYjI0NjM0NDQwMzNjNzA1ZDc4NzpzZXNzaW9uX2lkPTJfTVg0ME5Ua3hORFl3TW41LU1UVXdNRFF6TXpjME1UUXhNbjV0ZUdOdFJHcHFWbVVyWVhCTlJsYzVVbUprUlRWSFduZC1mZyZjcmVhdGVfdGltZT0xNTAwNDMzODgzJm5vbmNlPTAuNzEwMjE2OTA4NDEwNTM5OSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNTAwNTIwMjgyJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9',
  tokenSubscriber: 'T1==cGFydG5lcl9pZD00NTkxNDYwMiZzaWc9MjEzZTFlYTYxNWM2NzEwZjU1MGJiYmZhZjc4ZjA2Njc2MmE5OGFmYjpzZXNzaW9uX2lkPTJfTVg0ME5Ua3hORFl3TW41LU1UVXdNRFF6TXpjME1UUXhNbjV0ZUdOdFJHcHFWbVVyWVhCTlJsYzVVbUprUlRWSFduZC1mZyZjcmVhdGVfdGltZT0xNTAwNDM0MDA2Jm5vbmNlPTAuNzc0MTA4MzkzNTY4NzgyNSZyb2xlPXN1YnNjcmliZXImZXhwaXJlX3RpbWU9MTUwMDUyMDQwNSZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ=='
}

class ChatToolScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isPublisher: true,
    }
  }

  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Chat Tool',
  })

  componentWillMount() {
    // const { state, setParams } = this.props.navigation;
    // const { params } = state
    // const { isPublisher } = params;

    // Session.connect(videoAPI.apiKey, videoAPI.sessionId, videoAPI.tokenPublisher || videoAPI.tokenSubscriber);
    // Session.onMessageRecieved((e) => console.log(e));
  }

  render() {
    const {language} = this.props

    const { navigation } = this.props;
    // const { state, setParams } = navigation;
    // const { params } = state;

    // const { isPublisher } = params;

    return (
      <ScrollView>
        <View style={ localStyles.container }>
          <Text onPress={() => { Session.sendMessage('test'); }}>
            {isPublisher ? 'Publisher' : 'Subscriber'}
          </Text>
        </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

export default connect(mapStateToProps, null)(ChatToolScreen)
