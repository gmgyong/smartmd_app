import { StyleSheet } from 'react-native'

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

/** export module */
module.exports = localStyles
