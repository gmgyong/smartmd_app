import React from 'react'
import {View, ActivityIndicator, ScrollView, Image, Text } from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {Button} from 'react-native-elements'
import moment from 'moment-timezone'
import { NavigationActions, HeaderBackButton } from 'react-navigation'
import { Label } from '../../../components/form/Field'
import Card from '../../../components/Card'
import LocalStorage from '../../../components/localStorage'
import GMGClient from '../../../configs/gmgHttpClient'
import images from '../../../configs/images'
import ListChatLog from '../../../components/listView/chatlog'
import { Colors, StorageKeys, APIs, FontSizes, SPACE } from '../../../configs/constants'
import FormatDate from '../../../configs/formatDate'
import Icons from '../../../configs/icons'
import localStyles from '../localStyles'
import globalStyles from '../../../styles'

const backAction = NavigationActions.back()

class SummaryAppointmentScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      navigate_home : false,
      isDataLoading: true,
      appointmentDetail: {},
      enableScrollViewScroll: true,
    }
  }

  // static navigationOptions = ({navigation}) => ({
  //   title: I18n.t('appointments.summary.title')
  // })

  static navigationOptions = ({navigation}) => {
    const {state, setParams} = navigation;
    const navigateHome = state.params.navigate_home;
    console.log(navigateHome);
    return {
      title: I18n.t('appointments.summary.title'),
      headerLeft: <HeaderBackButton onPress={() => {
        if (!navigateHome) {
          navigation.dispatch(backAction)
        }else {
          navigation.navigate('Home')
        }
      }}
      tintColor = '#FFFFFF'
      />
    };
  }

  async componentDidMount() {
    const userId = await LocalStorage.getString(StorageKeys.USERID)
    this.handleGetSummaryAppointment(userId)
  }

  handleGetSummaryAppointment = (userId) => {
    const {params} = this.props.navigation.state

    let urlAPI = APIs.APPOINTMENT_INFO
    + '?user_id=' + userId
    + '&appointment_id=' + params.appointment_id

    GMGClient.getData(
      urlAPI,
      json => {
        if(json.success) {
          console.log('json.data: ', json.data)
          this.setState(
            {
              isDataLoading: false,
              appointmentDetail: json.data
            }
          )
        }
      },
      error => {
        this.setState({
          isDataLoading: false,
        })
        alert(error)
      }
    )
  }

  renderLoadingIndicator = () => {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={[globalStyles.indicatorPageLoading, {backgroundColor: '#FFF'}]}
          color={Colors.TEXT_SECONDARY}
          size='large'
        />
      )
  }

  renderChatLog = () => {
    const {appointmentDetail} = this.state

    return (
      <View
        style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer] }
        onStartShouldSetResponderCapture={ () => this.setState({ enableScrollViewScroll: false })}
      >
        <Label label={ I18n.t('appointments.summary.chatLog').toUpperCase() } style={{ marginBottom: 0, fontSize: FontSizes.BIG }} />
        <ListChatLog
          navigation = { this.props.navigation }
          listRef='chatLogList'
          listChatLog = {appointmentDetail.chatlog}
          doctorId = {appointmentDetail.doctor_id} />
      </View>
    )
  }

  onStartShouldSetResponderCapture = (e) => {
    // const target = e.nativeEvent.target;

// console.log(e, 'target')
    /*if (this.refs.chatLogList.scrollProperties.offset === 0 && this.state.enableScrollViewScroll === false) {
      this.setState({ enableScrollViewScroll: true })
    }*/
  }

  renderInvoice = () => {
    const {appointmentDetail} = this.state

    return (
      <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer] }>
        <Label label={ I18n.t('appointments.summary.paymentInformation').toUpperCase() } style={{ marginBottom: 0, fontSize: FontSizes.BIG }} />
        <Button
          title={ I18n.t('buttons.btnViewInvoice').toUpperCase() }
          onPress={ () => this.handleViewInvoice(appointmentDetail.invoice_id) }
          buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
          containerViewStyle={ globalStyles.buttonView }
        />
      </View>
    )
  }

  renderDuration = (data) => {
    let titleTime = ''
    let contentTime = ''

    switch(data.doctor_response_type || data.status) {
      case 'Both No Show':
        titleTime = I18n.t('appointments.summary.status.scheduledTime').toUpperCase()
        contentTime = FormatDate.switchToLocalTime(data.scheduled_time)
        break
      case 'Past':
        titleTime = I18n.t('appointments.summary.status.duration').toUpperCase()
        contentTime = FormatDate.switchToLocalTime(data.started_at.date) + ' - ' + FormatDate.switchToLocalTime(data.ended_at.date)
        break
      default:
        titleTime = I18n.t('appointments.summary.status.duration').toUpperCase()
        break
    }

    return (
      <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer] }>
        <Label label={ titleTime } style={{ marginBottom: 0, fontSize: FontSizes.BIG }} />
        <Text style={ [globalStyles.normalText] }>
          {contentTime}
        </Text>
        <Text style={ [globalStyles.normalText] }>
          {data.duration !== -1
            ? `${data.duration} ${I18n.t('appointments.summary.mins')}`
            : `0 minute`
          }
        </Text>
      </View>
    )
  }

  renderContainer = () => {
    const {appointmentDetail} = this.state

    if (typeof appointmentDetail !== "undefined" || appointmentDetail !== null) {
      return (
        <ScrollView scrollEnabled={this.state.enableScrollViewScroll}>
          <Card
            userImage={ appointmentDetail.doctor_image }
            heading={ appointmentDetail.doctor_name }
            subHeading={ appointmentDetail.doctor_speciality }
          />

          {this.renderDuration(appointmentDetail)}

          {appointmentDetail.invoice_id && this.renderInvoice()}

          <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer] }>
            <Label label={ I18n.t('appointments.summary.doctorMemo').toUpperCase() } style={{ marginBottom: 0, fontSize: FontSizes.BIG }} />
            <Text style={ [globalStyles.normalText] }>
              {appointmentDetail.doctor_memo === '' ? 'No memo' : appointmentDetail.doctor_memo}
            </Text>
          </View>

          {appointmentDetail.chatlog.length > 0 ? this.renderChatLog() : null}

          <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer] }>
            <Label label={ I18n.t('appointments.make.stepOne.symptoms').toUpperCase() } style={{ marginBottom: 0, fontSize: FontSizes.BIG }} />
            <Text style={ [globalStyles.normalText] }>
              {appointmentDetail.symptoms === '' || appointmentDetail.symptoms === null ? 'No content' : appointmentDetail.symptoms}
            </Text>
          </View>
          <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer] }>
            <Label label={ I18n.t('appointments.make.stepOne.medicalHistory').toUpperCase() } style={{ marginBottom: 0, fontSize: FontSizes.BIG }} />
            <Text style={ [globalStyles.normalText] }>
              {appointmentDetail.medical_history === '' || appointmentDetail.medical_history === null ? 'No content' : appointmentDetail.medical_history}
            </Text>
          </View>
          <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer] }>
            <Label label={ I18n.t('appointments.make.stepOne.drugHistory').toUpperCase() } style={{ marginBottom: 0, fontSize: FontSizes.BIG }} />
            <Text style={ [globalStyles.normalText] }>
              {appointmentDetail.drug_history === '' || appointmentDetail.drug_history === null ? 'No content' : appointmentDetail.drug_history}
            </Text>
          </View>
          <View style={ [globalStyles.blockgreyDetail, localStyles.paddingContainer, {marginBottom: 10}] }>
            <Label label={ I18n.t('appointments.make.stepOne.allergies').toUpperCase() } style={{ marginBottom: 0, fontSize: FontSizes.BIG }} />
            <Text style={ [globalStyles.normalText] }>
              {appointmentDetail.drug_allergies === '' || appointmentDetail.drug_allergies === null ? 'No content' : appointmentDetail.drug_allergies}
            </Text>
          </View>
        </ScrollView>
      )
    } else {
      return (<View />)
    }
  }

  handleViewInvoice = (id) => {
    const {navigate} = this.props.navigation

    navigate('BillingDetailPatient',{invoice_id: id});
  }

  render() {
    return (
      <View style={ localStyles.wrapper } onStartShouldSetResponderCapture={ () => this.setState({ enableScrollViewScroll: true })}>
        { this.state.isDataLoading ? this.renderLoadingIndicator() : this.renderContainer() }
      </View>
    )
  }
}

const mapStateToProps = state => ({
  language: state.settings.language
})

export default connect(mapStateToProps, null)(SummaryAppointmentScreen)
