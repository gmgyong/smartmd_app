import React, { Component } from 'react'
import { View, ActivityIndicator, FlatList, Text } from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'
import Icons from '../../configs/icons'
import { SearchBar } from 'react-native-elements'
import ItemView from '../../components/ItemView'
import LocalStorage from '../../components/localStorage'
import localStyles from './localStyles'
import GMGClient from '../../configs/gmgHttpClient'
import { APIs, StorageKeys } from '../../configs/constants'

class DoctorsScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      searching: false,
      searchEnd: false,
      data: [],
      page: 1,
      per_page: 10,
      error: null,
      refreshing: false,
      filter_text: '',
      totalItem: 0,
      user_id: ''
    }
  }

  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarIcon: Icons.Doctors,
    tabBarPosition: 'top'
    // title: I18n.t('doctors.title')
  })

  makeRequest = () => {
    const { page, per_page, filter_text } = this.state
    const urlAPI =
      APIs.LIST_DOCTOR
      + "?page=" + page
      + "&per_page=" + per_page
      + "&filter_text=" + filter_text

    this.setState({ loading: true })

    console.log(urlAPI)

    GMGClient.getData(
      urlAPI,
      json => {
        this.setState({
          data: page === 1
            ? json.data
            : [...this.state.data, ...json.data],
          error: !json.success,
          loading: false,
          refreshing: false,
          totalItem: json.total,
          searchEnd: true
        })
      },
      error => {
        alert(error)
      }
    )
  }

  handleRefresh = () => {
    console.log('-------REFRESHING-------')
    return  this.setState(
      {
        page: 1,
        refreshing: true
      },
      ()=> {
        this.makeRequest()
      }
    )
  }

  handleLoadMore = () => {
    console.log('-------LOAD MORE-------')
    return this.setState(
      {
        loading: true,
        page: this.state.page + 1
      },
      ()=> {
        this.makeRequest()
      }
    )
  }

  handleChangeSearch = filter_text => {
    if(filter_text === '') {
      console.log('-------REFRESHING-------')
      this.setState(
        {
          loading: true,
          page: 1,
          searching: false,
          searchEnd: false,
          filter_text: filter_text
        },
        () => {
          this.makeRequest()
        }
      )
    } else {
      this.setState({filter_text})
    }
  }


  handleSubmitSearch = () => {
    console.log('-------SEARCHING-------')
    return this.setState(
      {
        loading: true,
        page: 1,
        searching: true,
        searchEnd: false,
        filter_text: this.state.filter_text.trim()
      },
      ()=> {
        this.makeRequest()
      }
    )
  }

  renderFooter = () => {
    if(this.state.loading) {
      return (
        <View
          style={{
            paddingVertical: 15,
          }}
        >
          <ActivityIndicator animating size="large"/>
        </View>
      )
    }
    else {
      return null
    }
  }

  renderSeparator = () => (
    <View style={{height: 10, backgroundColor: "#FFF"}}/>
  )

  async componentDidMount() {
    const user_id = await LocalStorage.getString(StorageKeys.USERID)
    console.log('>>>>>>>>>>>>>>>>>>>>>>> Doctors')
    this.setState({user_id: user_id})
    this.makeRequest()
  }

  render() {
    const { navigate } = this.props.navigation
    return (
      <View style={ localStyles.container }>
        <SearchBar
          containerRef="containerSearch"
          textInputRef="inputSearch"
          placeholder={ I18n.t("doctors.placeholderSearch") }
          placeholderTextColor="#a4a4a4"
          containerStyle={ localStyles.containerSearch }
          inputStyle={ localStyles.inputSearch }
          icon={{ color: '#a4a4a4'}}
          clearIcon={{color:'#a4a4a4'}}
          showLoadingIcon={ this.state.loading }
          onChangeText={this.handleChangeSearch}
          onSubmitEditing={ this.handleSubmitSearch }
          returnKeyType="search"
          value={ this.state.filter_text}
        />
        { (this.state.searching && this.state.searchEnd) ? <Text style={localStyles.textResult}>{this.state.totalItem} result with keyword: "{this.state.filter_text}"</Text> : null }
        <FlatList
          // ListFooterComponent={ this.renderFooter }
          ItemSeparatorComponent={ this.renderSeparator }
          refreshing={ this.state.refreshing }
          onRefresh={ this.handleRefresh }
          onEndReached={ this.handleLoadMore }
          onEndReachedThreshold={ 0.3 }
          keyExtractor={ (item, index) => index }
          data={ this.state.data }
          renderItem={({item}) => (
            <ItemView
              key={ item.doctor_id }
              user_id={ this.state.user_id }
              doctor_profile={ item.doctor_profile }
              navigate={ navigate }
              titleBtnInfo={ I18n.t("doctors.btnInfo") }
              titleBtnAppointment={ I18n.t("doctors.btnMakeAppointment") }
            />
          )}
        />
        {/*{ this.state.loading ? <View style={{paddingVertical: 15}}><ActivityIndicator animating size="large"/></View> : null}*/}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
}

export default connect(mapStateToProps, null)(DoctorsScreen)
