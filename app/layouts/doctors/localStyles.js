import { StyleSheet } from 'react-native'
import { Colors, FontSizes } from '../../configs/constants'

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_MAIN_SCREEN
  },
  containerSearch: {
    backgroundColor: Colors.BACKGROUND_STACKS,
    marginTop: 10,
    marginBottom: 10,
    borderTopWidth: 0,
    borderBottomWidth: 0
  },
  inputSearch: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: FontSizes.NORMAL,
    color: Colors.TEXT_PRIMARY,
    backgroundColor: Colors.BACKGROUND_MAIN_SCREEN
  },
  textResult: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: FontSizes.LARGE,
    color: Colors.TEXT_PRIMARY,
  }
})

export default localStyles
