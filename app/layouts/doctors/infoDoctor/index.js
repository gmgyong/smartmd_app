import React from 'react'
import {View, Text, ScrollView, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import I18n from 'react-native-i18n'

import Card from '../../../components/Card'
import globalStyles from '../../../styles'
import { Label } from '../../../components/form/Field'
import {Button} from 'react-native-elements'

class InfoDoctor extends React.Component {
  constructor(props) {
    super(props)
  }

  static navigationOptions = ({navigation}) => ({
    title: I18n.t('doctorInfo.title')
  })

  onPressMakeAppointment = () => {
    const { navigate } = this.props.navigation
    const {info} = this.props.navigation.state.params
    navigate('MakeAppointment',{info: info});
  }

  renderPrice = (prices =this.props.navigation.state.params.info.price_breakdown) => {
    return prices.map( (item,index) => (
      <Text key={index} style={ [globalStyles.normalText] }>{item.description}: {item.price}</Text>
    ))
  }

  render() {
    const {info} = this.props.navigation.state.params
    console.log(info)

    return (
      <ScrollView style={ globalStyles.wrapper }>
        <Card
          // backgroundImage={ require('../../../images/bgDoctor.jpg') }
          userImage={ info.photo_url }
          heading={ info.name }
          subHeading={ info.specialty }
        />
        <View style={ globalStyles.blockgreyDetail }>
          <Label label={ I18n.t('doctorInfo.labelPrice') } style={{ marginBottom: 0, fontWeight: 'bold' }} />
          
          { this.renderPrice() }
        </View>
        <View style={ globalStyles.blockgreyDetail }>
          <Label label={ I18n.t('doctorInfo.labelSubSpecialties') } style={{ marginBottom: 0, fontWeight: 'bold' }} />
          <Text style={ [globalStyles.normalText] }>
            { info.subspecialties }
          </Text>
        </View>
        <View style={ globalStyles.blockgreyDetail }>
          <Label label={ I18n.t('doctorInfo.labelBackground') } style={{ marginBottom: 0, fontWeight: 'bold' }} />
          <Text style={ [globalStyles.normalText] }>
            { info.background }
          </Text>
        </View>
        <View style={ globalStyles.blockgreyDetail }>
          <Label label={ I18n.t('doctorInfo.labelQualfications') } style={{ marginBottom: 0, fontWeight: 'bold' }} />
          <Text style={ [globalStyles.normalText] }>
            { info.qualifications }
          </Text>
        </View>
        <Button
          title={ I18n.t('doctorInfo.btnMakeAppointment') }
          onPress={ this.onPressMakeAppointment }
          buttonStyle={ localStyles.buttonBlue }
          containerViewStyle={ localStyles.button }
          icon={{name: 'comments-o', type: 'font-awesome'}}
        />
      </ScrollView>
    )
  }
}

const localStyles = StyleSheet.create({

  button: {
    marginLeft: 0, marginRight: 0, marginTop: 10
  },
  buttonBlue: {
    backgroundColor: '#35c1cf',
    height: 50,
    borderRadius: 0,
  },
})

const mapStateToProps = (state) => ({
  language: state.settings.language

})

export default connect(mapStateToProps, null)(InfoDoctor)
