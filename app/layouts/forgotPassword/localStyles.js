import {StyleSheet, Dimensions} from 'react-native'
import { SPACE } from '../../configs/constants'

// let SPACE = (Dimensions.get('window').width - MaxWidth)/2;
//
// SPACE = (SPACE < 30) ? 30 : SPACE
// console.log(SPACE)

const localStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#FFF"
  },
  container: {
    marginLeft: SPACE, marginRight: SPACE, marginTop: -150,
  },
  figure: {
    alignItems: 'center',
    marginBottom: 30,
  },
  logo: {
    width: 120,
    height: 45,
    resizeMode: 'contain',
  },
  button: {
    marginLeft: 0, marginRight: 0,
  },
  buttonPurple: {
    backgroundColor: '#584b8d',
    height: 35,
    borderRadius: 4,
  },
  buttonFlat: {
    backgroundColor: 'transparent',
    marginTop: 10,
    marginBottom: 10,
    height: 35,
  },
  paragraphNormal: {
    fontSize: 14,
    color: '#555',
    lineHeight: 20,
    marginBottom: 15,
    textAlign: 'center'
  },
  title: {
    fontSize: 22,
    color: '#555',
    lineHeight: 28,
    marginBottom: 15,
    textAlign: 'center'
  }
});

/** export module */
module.exports = localStyles;
