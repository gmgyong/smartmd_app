import React from 'react'
import {View, Text, Image, ActivityIndicator} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {Button} from 'react-native-elements'

import {navigate} from 'react-navigation'
import InlineLabelField from '../../components/form/InlineLabelField'

import { ErrorMes } from '../../components/form/Field'
import globalStyles from '../../styles'
import localStyles from './localStyles'
import images from '../../configs/images'
import { APIs, StorageKeys, Colors } from '../../configs/constants'
import LocalStorage from '../../components/localStorage'
import GMGClient from '../../configs/gmgHttpClient'

class ForgotPasswordScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      swithPasswordErrorMess: false,
      isDataLoading: false,
      step: '',
      errorAPIStep1: {
        value: '',
        error: false
      },
      errorAPIStep2: {
        value: '',
        error: false
      },
      errorAPIStep3: {
        value: '',
        error: false
      },
      email: {
        value: '',
        error: undefined
      },
      code: {
        value: '',
        error: undefined
      },
      password: {
        value: '',
        error: undefined
      },
      reTypePass: {
        value: '',
        error: undefined
      }
    };
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle: I18n.t("forgotPassword.title")
  });

  onChangeMail = value => {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (value === '' || value === undefined || !regex.test(value)) {
      this.setState({email: {error: true}});
      return false;
    } else {
      this.setState({email: {value: value, error: false}});
      return true;
    }
  };

  onChangeCode = value => {
    if (value === '' || value === undefined) {
      this.setState({code: {error: true}});
      return false;
    } else {
      this.setState({code: {value: value, error: false}});
      return true;
    }
  };

  onChangePassword = value => {
    if (value === '' || value === undefined) {
      this.setState({password: {error: true}});
      return false;
    }else if (value.length < 8) {
      this.setState({password: {error: true}, swithPasswordErrorMess: true});
    } else {
      this.setState({password: {value: value, error: false}});
      return true;
    }
  };

  onChangeReTypePass = value => {
    let password = this.state.password.value;
    if (value === '' || value === undefined || value !== password) {
      this.setState({reTypePass: {error: true}});
      return false;
    } else {
      this.setState({reTypePass: {value: value, error: false}});
      return true;
    }
  };

  handleNextStep1 = () => {
    let valid = this.onChangeMail(this.state.email.value);

    this.setState({errorAPIStep1: {error: false}})
    if (valid) {
      //sent email to server if true:
      this.callAPIStep1();
    }
  };

  handleNextStep2 = () => {
    let valid = this.onChangeCode(this.state.code.value);

    this.setState({errorAPIStep1: {error: false}})
    if (valid) {
      //sent email to server if true:
      this.callAPIStep2();
    }
  };

  handleNextStep3 = () => {
    let validPassword = this.onChangePassword(this.state.password.value);
    let validReTypePass = this.onChangeReTypePass(this.state.reTypePass.value);

    this.setState({errorAPIStep1: {error: false}})
    if (validPassword && validReTypePass) {
      this.callAPIStep3()
    }

  };

  callAPIStep1 = () => {

    this.setLoadingProgress(true);

    const bodyData= {
      email: this.state.email.value,
    }

    GMGClient.postData(
      APIs.CREATE_PASSWORD_RESET_TOKEN,
      bodyData,
      json => {
        this.handleAPIStep1(json);
        this.setLoadingProgress(false);
      },
      error => {
        console.log('///////ERROR://///////');
        console.log(error);
        this.setLoadingProgress(false);
      }


    )
  }

  handleAPIStep1 = data => {
    switch (data.status_code) {
      case 200: {
        //save email login to LocalStorage
        LocalStorage.setString('username', this.state.email.value);

        this.setState({step: 'STEP2'});
        break;
      }
      case 201: {
        this.setState({ errorAPIStep1: { value: 'Email not found. Please check again', error: true }});
        break;

      }
      default: {
        this.setState({ errorAPIStep1: { value: data.status_code + '\n' + data.errors.join('\n'), error: true }});
        break;
      }
    }
  }

  callAPIStep2 = () => {

    this.setLoadingProgress(true);

    const urlAPI=
      APIs.CREATE_PASSWORD_RESET_TOKEN
      + "/" + this.state.code.value + "?"

    console.log(urlAPI)

    GMGClient.getData(
      urlAPI,
      json => {
        this.handleAPIStep2(json)
        this.setLoadingProgress(false)
      },
      error => {
        console.log(error)
        this.setLoadingProgress(false)
      }
    )
  }

  handleAPIStep2 = data => {
    switch (data.status_code) {
      case 200: {

        this.setState({step: 'STEP3'});
        break;
      }
      case 201: {
        this.setState({ errorAPIStep2: { value: 'Code not found. Please check again', error: true }});
        break;

      }
      default: {
        this.setState({ errorAPIStep2: { value: data.status_code + '\n' + data.errors.join('\n'), error: true }});
        break;
      }
    }
  }

  callAPIStep3 = () => {

    this.setLoadingProgress(true);

    const bodyData= {
      new_password: this.state.password.value,
    }

    const urlAPI = APIs.RESET_PASSWORD + '/' + this.state.code.value

    GMGClient.putData(
      urlAPI,
      bodyData,
      json => {
        this.handleAPIStep3(json);
        this.setLoadingProgress(false);
      },
      error => {
        console.log('///////ERROR://///////');
        console.log(error);
        this.setLoadingProgress(false);
      }


    )
  }

  handleAPIStep3 = data => {
    switch (data.status_code) {
      case 200: {
        const {navigate} = this.props.navigation;

        //save email login to LocalStorage
        LocalStorage.setString('password', this.state.password.value);
        navigate('SignIn')
        break;
      }
      case 402: {
        this.setState({ errorAPIStep3: { value: data.errors.join('\n'), error: true }});
        break;
      }
      default: {
        this.setState({ errorAPIStep3: { value: data.status_code + '\n' + data.errors.join('\n'), error: true }});
        break;
      }
    }
  }
  renderLoadingIndicator = (isShowing) => {
    if (isShowing) {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={globalStyles.indicatorPageLoading}
          color={Colors.TEXT_SECONDARY}
          size='small'/>
      )
    } else {
      return null;
    }
  }

  setLoadingProgress = (isDataLoading) => {
    this.setState({isDataLoading});
  }

  render() {
    const Step1Screen = () => {
      return (
        <View>
          <Text style={ localStyles.paragraphNormal }>{ I18n.t("forgotPassword.textStep1")}</Text>
          <InlineLabelField
            label={ I18n.t("forgotPassword.email.label") }
            placeholder={ I18n.t("forgotPassword.email.placeholder") }
            onChangeText={ value => this.onChangeMail(value) }
            errorMes={ I18n.t("forgotPassword.email.errorMes") }
            error={ this.state.email.error }
            returnKeyType="next"
            onSubmitEditing={ this.handleNextStep1 }
          />

          { (this.state.errorAPIStep1.error) ? <ErrorMes errorMes={ this.state.errorAPIStep1.value } arrow={ false} /> : null}

          <Button
            title={ I18n.t("forgotPassword.btnNext") }
            onPress={ this.handleNextStep1 }
            buttonStyle={ localStyles.buttonPurple }
            containerViewStyle={ localStyles.button }
          />
        </View>
      )
    };

    const Step2Screen = () => {
      return (
        <View>
          <Text style={ localStyles.title }>{ I18n.t("forgotPassword.titleStep2") + ' ' + this.state.email.value }</Text>
          <Text style={ localStyles.paragraphNormal }>{ I18n.t("forgotPassword.textStep2")}</Text>
          <InlineLabelField
            label={ I18n.t("forgotPassword.code.label") }
            placeholder={ I18n.t("forgotPassword.code.placeholder") }
            onChangeText={ value => this.onChangeCode(value) }
            errorMes={ I18n.t("forgotPassword.code.errorMes") }
            error={ this.state.code.error }
            returnKeyType="next"
            onSubmitEditing={ this.handleNextStep2 }
          />

          { (this.state.errorAPIStep2.error) ? <ErrorMes errorMes={ this.state.errorAPIStep2.value } arrow={ false} /> : null}

          <Button
            title={ I18n.t("forgotPassword.btnNext") }
            onPress={ this.handleNextStep2 }
            buttonStyle={ localStyles.buttonPurple }
            containerViewStyle={ localStyles.button }
          />
        </View>
      )
    };

    const Step3Screen = () => {
      return (
        <View>
          <Text style={ localStyles.title }>{ I18n.t("forgotPassword.textStep3")}</Text>
          <InlineLabelField
            label={ I18n.t("forgotPassword.password.label") }
            placeholder={ I18n.t("forgotPassword.password.placeholder") }
            onChangeText={ value => this.onChangePassword(value) }
            errorMes={ this.state.swithPasswordErrorMess? I18n.t("signUp.password.swithPasswordErrorMess") : I18n.t("forgotPassword.password.errorMes") }
            error={ this.state.password.error }
            secureTextEntry={ true }
            returnKeyType="next"
            onSubmitEditing={ () => this.inputReTypePass.focus() }
          />
          <View/>
          <InlineLabelField
            label={ I18n.t("forgotPassword.reTypePass.label") }
            placeholder={ I18n.t("forgotPassword.reTypePass.placeholder") }
            onChangeText={ value => this.onChangeReTypePass(value) }
            errorMes={ I18n.t("forgotPassword.reTypePass.errorMes") }
            error={ this.state.reTypePass.error }
            secureTextEntry={ true }
            inputRef={ node => this.inputReTypePass = node }
            returnKeyType="send"
            onSubmitEditing={ this.handleNextStep3 }
          />

          { (this.state.errorAPIStep3.error) ? <ErrorMes errorMes={ this.state.errorAPIStep3.value } arrow={ false} /> : null}

          <Button
            title={ I18n.t("forgotPassword.btnChangePassword") }
            onPress={ this.handleNextStep3 }
            buttonStyle={ localStyles.buttonPurple }
            containerViewStyle={ localStyles.button }
          />
        </View>
      )
    };

    const switchStep = (state) => {
      switch (state.step) {
        case 'STEP1':
          return Step1Screen()
        case 'STEP2':
          return Step2Screen()
        case 'STEP3':
          return Step3Screen()
        default:
          return Step1Screen()
      }
    };

    return (
      <View style={ localStyles.wrapper }>
        <View style={ localStyles.container }>
          <View style={ localStyles.figure }>
            <Image source={ images.logo } style={ localStyles.logo }/>
          </View>
          { switchStep(this.state) }
        </View>
        {this.renderLoadingIndicator(this.state.isDataLoading)}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    language: state.settings.language
  }
};

export default connect(mapStateToProps, null)(ForgotPasswordScreen)
