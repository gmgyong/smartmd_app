import React from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Keyboard,
  BackHandler,
  ActivityIndicator,
  Alert
} from 'react-native'
import { AccessToken, LoginManager, GraphRequestManager, GraphRequest } from 'react-native-fbsdk'
import { GoogleSignin } from 'react-native-google-signin'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {Button} from 'react-native-elements'
import {navigate} from 'react-navigation'
// import DeviceInfo from 'react-native-device-info'
import LocalStorage from '../../components/localStorage'
import {StorageKeys} from '../../configs/constants'

import InlineLabelField from '../../components/form/InlineLabelField'
import {ErrorMes} from '../../components/form/Field'
import GMGClient from '../../configs/gmgHttpClient'
import {AccountGoogleSignin, APIs, Colors} from '../../configs/constants'
import images from '../../configs/images'
import localStyles from './localStyles'
import globalStyles from '../../styles'
import moment from 'moment-timezone'

class SignInScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      errorAPI: {
        value: '',
        error: false
      },
      userName: {
        value: '',
        error: false,
      },
      password: {
        value: '',
        error: false,
      },
      tokenDevice: '',
      osDevice: ''
    }
  }

  validation = () => {
    let userName = this.state.userName.value;
    let password = this.state.password.value;
    let check = true;

    if (userName === '' || userName === undefined || userName === null) {
      this.setState((prevState) => ({userName: {value: prevState.userName.value, error: true}}));
      check = false;
    } else {
      this.setState((prevState) => ({userName: {value: prevState.userName.value, error: false}}));
    }

    if (password === '' || password === undefined) {
      this.setState((prevState) => ({password: {value: prevState.password.value, error: true}}));
      check = false;
    } else {
      this.setState((prevState) => ({password: {value: prevState.password.value, error: false}}));
    }

    return check;
  };

  /*handleBackPress = () => {
    console.log('click back button');
    Alert.alert(
      'Alert',
      'Are you want exit app?',
      [
        {text: 'No', onPress: () => console.log('click No')},
        {text: 'Yes', onPress: () => BackHandler.exitApp()},
      ]
    );
    return true;
  };*/

  handleSignUp = () => {
    const {navigate} = this.props.navigation;
    navigate('SignUp');
  }

  setLoadingProgress = (isDataLoading) => {
    this.setState({isDataLoading});
  }

  renderLoadingIndicator = (isShowing) => {
    if (isShowing) {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={globalStyles.indicatorPageLoading}
          color={Colors.TEXT_SECONDARY}
          size='small'/>
      )
    } else {
      return null;
    }
  }

  handleSubmit = () => {
    this.setState({errorAPI: {error: false}});
    this.setLoadingProgress(true);

    const {navigate} = this.props.navigation;
    let checkValidate = this.validation()

    let bodyData = {
      "username": this.state.userName.value,
      "password": this.state.password.value,
      // deviceUid: DeviceInfo.getUniqueID(),
      // timezone: moment.tz.guess(),
    }

    Keyboard.dismiss()

    if (checkValidate) {
      GMGClient.postData(
        APIs.SIGN_IN,
        bodyData,
        (json) => {
          this.setLoadingProgress(false);
          this.handleAPI(json)
        },
        (error) => {
          this.setLoadingProgress(false);
          console.log(error);
        }
      )
    } else {
      this.setLoadingProgress(false)
    }
  };

  handleAPI = data => {
    const {navigate} = this.props.navigation;
    switch (data.status_code) {
      case 200: {
        console.log('>>>> success' + 200);
        console.log("===================================")

        switch (data.data.type) {
          case 'Doctor': {
            alert('Please use app "SmartMD Doctor"')
            break
          }
          case 'Patient': {
            LocalStorage.setString(StorageKeys.USERNAME, this.state.userName.value);
            LocalStorage.setString(StorageKeys.PASSWORD, this.state.password.value);
            LocalStorage.setString(StorageKeys.USERID, data.data.id.toString());
            LocalStorage.setString(StorageKeys.USERFULLNAME, data.data.name)
            LocalStorage.setString(StorageKeys.USEREMAIL, data.data.email)
            LocalStorage.setString(StorageKeys.USERURL, data.data.photo_url)
            LocalStorage.setString(StorageKeys.TYPE_LOGIN, 'NORMAL')

            // Update device informations
            let bodyUpdate = {
              device_token: this.state.tokenDevice,
              platform: this.state.osDevice,
              is_login: 1
            }
            let urlAPI = APIs.UPDATE_DEVICE_TOKEN + '/' + data.data.id.toString()

            GMGClient.postData(
              urlAPI,
              bodyUpdate,
              res => {
                console.log(res, 'Update Device Success')
              },
              error => {
                console.log(error, 'Update Device Error')
              }
            )
            
            navigate('Authorized');
            break
          }
          default: {
            alert('Please check type account')
            break
          }
        }
        break;
      }
      case 402: {
        console.log('>>>> success' + 402)
        this.setState({errorAPI: {value: "Not a valid email or password. Please try again", error: true}});
        break;
      }
      case 401: {
        console.log('>>>> success' + 401)
        this.setState({errorAPI: {value: "Not a valid email or password. Please try again", error: true}});
        break;
      }
      default: {
        console.log('>>>> success - different errors')
        this.setState({errorAPI: {value: data.status_code + '\n' + data.errors.join(' '), error: true}});
        break;
      }
    }
  }

  handleForgotPassword = () => {
    const {navigate} = this.props.navigation;
    navigate('ForgotPassword');
  };


  handleLoginFB = () => {
    const _this = this

    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(function(result) {
      if (result.isCancelled) {
        console.log("user press cancel login facebook")
      } else {
        console.log(result)
        _this.setLoadingProgress(true)

        AccessToken.getCurrentAccessToken().then(
          data => {
            const body = {
              accessToken: data.accessToken,
              parameters: {
                fields: {
                  string: 'id, email, name, picture'
                }
              }
            }

            const infoRequest = new GraphRequest(
              '/me',
              body,
              (error, result) => {
                if(error) {
                  console.log(error)
                  alert("get info error: " + error.toString())
                } else {
                  console.log(result)

                  //get local device timezone
                  const { navigate } = _this.props.navigation
                  let timezone = moment.tz.guess() + ' ' + moment.tz(moment.tz.guess()).format('Z')
                  const body = {
                    user_id: result.id,
                    email: result.email,
                    name: result.name,
                    photo_url: result.picture.data.url,
                    timezone: timezone
                  }


                  const bodyUpdateDevice = {
                    device_token: _this.state.tokenDevice,
                    platform: _this.state.osDevice,
                    is_login: 1
                  }

                  GMGClient.postData(
                    APIs.LOGIN_FACEBOOK,
                    body,
                    json => {
                      _this.setLoadingProgress(false)
                      if(json.success) {

                        //save type user login
                        LocalStorage.setString(StorageKeys.TYPE_LOGIN, 'FACEBOOK')
                        LocalStorage.setString(StorageKeys.USERID, json.data.id.toString());
                        LocalStorage.setString(StorageKeys.USERFULLNAME, json.data.name)
                        LocalStorage.setString(StorageKeys.USEREMAIL, json.data.email)
                        LocalStorage.setString(StorageKeys.USERURL, json.data.photo_url)


                        // Update device informations
                        let urlAPI = APIs.UPDATE_DEVICE_TOKEN + '/' + json.data.id.toString()

                        GMGClient.postData(
                          urlAPI,
                          bodyUpdateDevice,
                          res => {
                            console.log(res, 'Update Device Success')
                          },
                          error => {
                            console.log(error, 'Update Device Error')
                          }
                        )

                        navigate("Authorized")
                      } else if(result.email === undefined) {
                        navigate("UpdateProfileSocial", {body: body, bodyUpdateDevice: bodyUpdateDevice})
                      } else {
                        this.setState({errorAPI: {value: json.errors.join('\n'), error: true}})
                      }
                    },
                    error => {
                      _this.setLoadingProgress(false)
                    }
                  )
                }
              }
            )

            new GraphRequestManager().addRequest(infoRequest).start()
          }
        ).catch( error => {
          _this.setLoadingProgress(false)
          console.log(error)
        })
      }
    }, function(error) {
      _this.setLoadingProgress(false)
      console.log("some error occurred!!");
    })
  }

  handleLoginGoogle = () => {
    const _this = this

    GoogleSignin.signIn()
      .then((user) => {
        console.log("LOGGED IN WITH GOOGLE")

        _this.setLoadingProgress(true)

        const { navigate } = _this.props.navigation
        let timezone = moment.tz.guess() + ' ' + moment.tz(moment.tz.guess()).format('Z')
        const body = {
          user_id: user.id,
          email: user.email,
          name: user.name,
          photo_url: user.photo,
          timezone: timezone
        }

        const bodyUpdateDevice = {
          device_token: _this.state.tokenDevice,
          platform: _this.state.osDevice,
          is_login: 1
        }

        GMGClient.postData(
          APIs.LOGIN_GOOGLE,
          body,
          json => {
            _this.setLoadingProgress(false)
            if(json.success) {

              LocalStorage.setString(StorageKeys.TYPE_LOGIN, 'GOOGLE')
              LocalStorage.setString(StorageKeys.USERID, json.data.id.toString());
              LocalStorage.setString(StorageKeys.USERFULLNAME, json.data.name)
              LocalStorage.setString(StorageKeys.USEREMAIL, json.data.email)
              LocalStorage.setString(StorageKeys.USERURL, json.data.photo_url)


              // Update device informations
              let urlAPI = APIs.UPDATE_DEVICE_TOKEN + '/' + json.data.id.toString()

              GMGClient.postData(
                urlAPI,
                bodyUpdateDevice,
                res => {
                  console.log(res, 'Update Device Success')
                },
                error => {
                  console.log(error, 'Update Device Error')
                }
              )

              navigate("Authorized")
            } else {
              this.setState({errorAPI: {value: json.errors.join('\n'), error: true}})
            }
          },
          error => {
            _this.setLoadingProgress(false)
          }
        )
      })
      .catch((err) => {
        console.log('WRONG SIGNIN', err);
      })
      .done();
  }

  setupGoogleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices({ autoResolve: true });
      await GoogleSignin.configure({
        iosClientId: AccountGoogleSignin.iosClientId,
        webClientId: AccountGoogleSignin.webClientId,
        offlineAccess: false,
      });

      const user = await GoogleSignin.currentUserAsync();
      console.log("currentUserAsync");
      console.log(user);
      if(!user) {
        this.handleLoginGoogle()
      } else {
        GoogleSignin.revokeAccess().then(() => GoogleSignin.signOut()).then(() => {
          this.setState({user: null});
        })
          .done();
      }
    }
    catch(err) {
      console.log("Play services error", err.code, err.message);
    }
  }

  componentDidMount() {
    //disable back button
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    this._loadInitialState()
    // this.setupGoogleSignIn()
  }

  _loadInitialState = async () => {
    var value = await LocalStorage.getString(StorageKeys.USERNAME);
    let tokenDevice = await LocalStorage.getString(StorageKeys.DEVICE_TOKEN)
    let osDevice = await LocalStorage.getString(StorageKeys.DEVICE_OS)
    
    if (value !== null || value !== undefined) {
      this.setState({
        userName: {
          value: value,
        },
        tokenDevice,
        osDevice
      });
    }
  }

  render() {
    const {language} = this.props;

    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', backgroundColor: '#fff'}}>
        
        <View style={ localStyles.maxContainer }>
          <View style={ localStyles.container }>
            <View style={ localStyles.figure }>
              <Image source={ images.logo } style={ localStyles.logo }/>
            </View>
            <InlineLabelField
              value={ this.state.userName.value }
              label={ I18n.t("signIn.email.label") }
              placeholder={ I18n.t("signIn.email.placeholder") }
              onChangeText={ (value) => this.setState({userName: {value}}) }
              errorMes={ I18n.t("signIn.email.errorMes") }
              error={ this.state.userName.error }
              returnKeyType="next"
              onSubmitEditing={() => this.inputPassword.focus() }
            />
            <InlineLabelField
              label={ I18n.t("signIn.password.label") }
              placeholder={ I18n.t("signIn.password.placeholder") }
              onChangeText={ (value) => this.setState({password: {value}}) }
              errorMes={ I18n.t("signIn.password.errorMes") }
              error={ this.state.password.error }
              secureTextEntry={ true }
              inputRef={ node => this.inputPassword = node }
              returnKeyType="send"
              onSubmitEditing={ this.handleSubmit }
            />

            { (this.state.errorAPI.error) ? <ErrorMes errorMes={ this.state.errorAPI.value } arrow={ false}/> : null}

            <Button
              title={ I18n.t("signIn.btnSignIn") }
              onPress={ this.handleSubmit }
              buttonStyle={ localStyles.buttonSignIn }
              containerViewStyle={ localStyles.button }
            />
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                style={ localStyles.buttonFlat }
                onPress={ this.handleForgotPassword }
                activeOpacity={0.7}
              >
                <Text style={ localStyles.textBlue }>{ I18n.t("signIn.linkForgotPassword") }</Text>
              </TouchableOpacity>
              <Text style={{flex: 1, backgroundColor: 'transparent'}}/>
              <TouchableOpacity
                style={ localStyles.buttonFlat }
                onPress={ this.handleSignUp }
                activeOpacity={0.7}
              >
                <Text style={ localStyles.textBlue }>{ I18n.t("signIn.linkSignUp") }</Text>
              </TouchableOpacity>
            </View>
            <Button
              title={ I18n.t("signIn.btnSignInGoogle") }
              icon={{name: 'google', type: 'font-awesome'}}
              onPress={ this.setupGoogleSignIn }
              buttonStyle={ localStyles.buttonGoogle }
              containerViewStyle={[localStyles.button, {marginBottom: 15}]}
            />
            <Button
              title={ I18n.t("signIn.btnSignInFacebook") }
              icon={{name: 'facebook', type: 'font-awesome'}}
              onPress={ this.handleLoginFB }
              buttonStyle={ localStyles.buttonFacebook }
              containerViewStyle={ localStyles.button }
            />
          </View>
        </View>

        {this.renderLoadingIndicator(this.state.isDataLoading)}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language
  }
};

export default connect(mapStateToProps, null)(SignInScreen)
