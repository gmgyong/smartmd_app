import { StyleSheet } from 'react-native';

const localStyles = StyleSheet.create({
    bgContainer: {
        width: null,
        height: null,
        resizeMode: 'stretch',
        position: 'absolute',
        top: 0, left: 0, right: 0, bottom: 0,
    },
    maxContainer: {
        flex: 1,
        justifyContent: 'center',
        maxWidth: 480,
    },
    container: {
        marginLeft: 30, marginRight: 30,
        backgroundColor: '#fff',
    },
    figure: {
        alignItems: 'center',
        marginBottom: 30,
    },
    logo: {
        width: 120,
        height: 45,
        resizeMode: 'contain',
    },
    button: {
        marginLeft: 0, marginRight: 0,
    },
    buttonSignIn: {
        backgroundColor: '#584b8d',
        height: 35,
        borderRadius: 4,
    },
    buttonGoogle: {
        backgroundColor: '#df4a32',
        height: 35,
        borderRadius: 4,
    },
    buttonFacebook: {
        backgroundColor: '#3b5998',
        height: 35,
        borderRadius: 4,
    },
    buttonFlat: {
        backgroundColor: 'transparent',
        marginTop: 10,
        marginBottom: 10,
        height: 35,
    },
    textBlue: {
        fontSize: 14,
        color: '#35c1cf',
        lineHeight: 25,
    }
});

/** export module */
module.exports = localStyles;
