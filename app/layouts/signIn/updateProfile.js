import React from 'react'
import {View, Text, Image, ActivityIndicator, StyleSheet} from 'react-native'
import {connect} from 'react-redux'
import I18n from 'react-native-i18n'
import {Button} from 'react-native-elements'

import {navigate} from 'react-navigation'
import InlineLabelField from '../../components/form/InlineLabelField'

import { ErrorMes } from '../../components/form/Field'
import globalStyles from '../../styles'
import images from '../../configs/images'
import { APIs, StorageKeys, Colors, SPACE } from '../../configs/constants'
import LocalStorage from '../../components/localStorage'
import GMGClient from '../../configs/gmgHttpClient'

class UpdateProfileScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isFetching: false,
      errorAPI: {
        error: false,
        value:''
      },
      email: {
        error: false,
        value: ''
      }
    }
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle: I18n.t("updateProfileSignIn.title")
  });

  onChangeMail = value => {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (value === '' || value === undefined || !regex.test(value)) {
      this.setState({email: {error: true}});
      return false;
    } else {
      this.setState({email: {value: value, error: false}});
      return true;
    }
  }

  callAPI = () => {
    let bodyCheckEmail = {
      email: this.state.email.value
    }

    let bodyUpdate = {
      ...this.props.navigation.state.params.body,
      email: this.state.email.value
    }

    GMGClient.postData(
      APIs.CHECK_EMAIL_EXIST,
      bodyCheckEmail,
      json => {
        if(json.success) {
          GMGClient.postData(
            APIs.LOGIN_FACEBOOK,
            bodyUpdate,
            json => {
              if(json.success) {

                LocalStorage.setString(StorageKeys.USERID, json.data.id.toString());
                LocalStorage.setString(StorageKeys.USERFULLNAME, json.data.name)
                LocalStorage.setString(StorageKeys.USEREMAIL, json.data.email)
                LocalStorage.setString(StorageKeys.USERURL, json.data.photo_url)


                // Update device informations
                let urlAPI = APIs.UPDATE_DEVICE_TOKEN + '/' + json.data.id.toString()

                GMGClient.postData(
                  urlAPI,
                  this.props.navigation.state.params.bodyUpdateDevice,
                  res => {
                    console.log(res, 'Update Device Success')
                  },
                  error => {
                    console.log(error, 'Update Device Error')
                  }
                )

                this.props.navigation.navigate("Authorized")
              } else {
                this.setState({errorAPI: {value: json.status_code + '\n' + json.errors.join(' '), error: true}});
              }
            },
            error => {}
          )
        } else {
          this.setState({errorAPI: {value: json.errors.join(' '), error: true}});
        }
      },
      error => {}
    )

  }

  handleSubmit = () => {

    let valid = this.onChangeMail(this.state.email.value);

    this.setState({errorAPI: {error: false}})
    if (valid) {
      //sent email to server if true:
      this.callAPI();
    }
  }

  renderLoadingIndicator = (isShowing) => {
    if (isShowing) {
      return (
        <ActivityIndicator
          animating={this.state.isDataLoading}
          style={globalStyles.indicatorPageLoading}
          color={Colors.TEXT_SECONDARY}
          size='small'/>
      )
    } else {
      return null;
    }
  }

  render() {
    return (
      <View style={ localStyles.wrapper }>
        <View style={ localStyles.container }>
          <View style={ localStyles.figure }>
            <Image source={ images.logo } style={ localStyles.logo }/>
          </View>
          <View>
            <Text style={ localStyles.paragraphNormal }>{ I18n.t("updateProfileSignIn.text")}</Text>
            <InlineLabelField
              label={ I18n.t("updateProfileSignIn.email.label") }
              placeholder={ I18n.t("updateProfileSignIn.email.placeholder") }
              onChangeText={ value => this.onChangeMail(value) }
              errorMes={ I18n.t("updateProfileSignIn.email.errorMes") }
              error={ this.state.email.error }
              returnKeyType="send"
              onSubmitEditing={ this.handleSubmit }
            />

            { (this.state.errorAPI.error) ? <ErrorMes errorMes={ this.state.errorAPI.value } arrow={ false} /> : null}

            <Button
              title={ I18n.t("updateProfileSignIn.btnSend") }
              onPress={ this.handleSubmit }
              buttonStyle={ localStyles.buttonPurple }
              containerViewStyle={ localStyles.button }
            />
          </View>
        </View>
        {this.renderLoadingIndicator(this.state.isFetching)}
      </View>
    )
  }
}

const localStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#FFF"
  },
  container: {
    marginLeft: SPACE, marginRight: SPACE, marginTop: -150,
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#FFF"
  },
  figure: {
    alignItems: 'center',
    marginBottom: 30,
  },
  logo: {
    width: 120,
    height: 45,
    resizeMode: 'contain',
  },
  button: {
    marginLeft: 0, marginRight: 0,
  },
  buttonPurple: {
    backgroundColor: '#584b8d',
    height: 35,
    borderRadius: 4,
  },
  buttonFlat: {
    backgroundColor: 'transparent',
    marginTop: 10,
    marginBottom: 10,
    height: 35,
  },
  paragraphNormal: {
    fontSize: 14,
    color: '#555',
    lineHeight: 20,
    marginBottom: 15,
    textAlign: 'center'
  },
  title: {
    fontSize: 22,
    color: '#555',
    lineHeight: 28,
    marginBottom: 15,
    textAlign: 'center'
  }
});

const mapStateToProps = state => {
  return {
    language: state.settings.language
  }
};



export default connect(mapStateToProps,null)(UpdateProfileScreen)