import React, { Component } from 'react'
import { Platform, Alert, AsyncStorage } from 'react-native'
import { Provider, connect } from 'react-redux'

import moment from 'moment-timezone'
import FormatDate from './configs/formatDate'

import './lib/mutilLanguages/I18n/I18n' // keep before RootContainer
import Root from './routes'
import PushNotification from 'react-native-push-notification'

import LocalStorage from './components/localStorage'
import {StorageKeys} from './configs/constants'
import Environments from './configs/environmentsAPIs'
import createStore from './lib/mutilLanguages/Redux'
import GmgNetInfo from './configs/netInfo'

// create our store
const store = createStore()

export default class GlobalMedicalGroup extends Component {
  constructor(props) {
    super(props)

    this.state = {
      deviceInfo: {},
      // listNotification: []
    }

    let options = {
      onRegister: (token) => {
        this.setDeviceInfo(token)
        console.log('>>>>> New device token for push notification: ' + JSON.stringify(token))
      },
      onNotification: (notification) => this.handleNotification(notification),
      popInitialNotification: true,
      requestPermissions: true,
    }

    Platform.OS === 'ios' ? options.permissions = {
      alert: true,
      badge: true,
      sound: true
    } : options.senderID = Environments.PUSH_NOTIFICATION_SENDER_ID

    PushNotification.configure(options);
  }

  componentDidMount() {
    GmgNetInfo.checkNetInfo()
  }

  setDeviceInfo = (info) => {
    LocalStorage.setString(StorageKeys.DEVICE_TOKEN, info.token);
    LocalStorage.setString(StorageKeys.DEVICE_OS, info.os);
  }

  handleFormatDate = (date) => {
    let arrDate = [
      moment(date).year(),
      moment(date).month(),
      moment(date).date(),
      moment(date).hour(),
      moment(date).minute(),
      moment(date).second()
    ]

    let format = new Date(moment(arrDate))

    return format
  }

  handleNotification = (notification) => {
    console.log(notification)
    // let {listNotification} = this.state

    if (Platform.OS === 'android' || (Platform.OS === 'ios' && notification.data.remote === true)) {
      const notificationInfo = Platform.OS === 'ios' ? notification.message : notification.notification
      console.log(notification, 'HSGDGSDKFDS')
      if (notification.userInteraction === false) {
        let currentTime = moment().format('DD MMM YYYY HH:mm')
        let nextTime = FormatDate.switchToLocalTime(notificationInfo.scheduled_time)
        let previousTimePush = moment(nextTime).subtract(30, 'minutes').format('DD MMM YYYY HH:mm')

        // AsyncStorage.setItem(StorageKeys.NOTIFICATION, JSON.stringify(notificationInfo))
        LocalStorage.setJson(StorageKeys.NOTIFICATION, notificationInfo)

        if(currentTime < previousTimePush) {
         switch(notificationInfo.action) {
           case 'DOCTOR_CONFIRM':
           PushNotification.localNotificationSchedule({
           id: notificationInfo.appointment_id,
           title: 'GMG - Remind teleconsult',
           message: nextTime + ' - ' + notificationInfo.doctor_name,
           date: this.handleFormatDate(previousTimePush),
           userInfo: {id: notificationInfo.appointment_id},
           })
           break
           case 'DOCTOR_RESCHEDULE':
           PushNotification.cancelLocalNotifications({
           id: notificationInfo.appointment_id,
           userInfo: {id: notificationInfo.appointment_id},
           })

           PushNotification.localNotificationSchedule({
           id: notificationInfo.appointment_id,
           title: 'GMG - Remind teleconsult',
           message: nextTime + ' - ' + notificationInfo.doctor_name,
           date: this.handleFormatDate(previousTimePush),
           userInfo: {id: notificationInfo.appointment_id},
           })
           break
           case 'DOCTOR_CANCEL':
           PushNotification.cancelLocalNotifications({
           id: notificationInfo.appointment_id,
           userInfo: {id: notificationInfo.appointment_id},
           })
           break
           default:
           break
         }

         }
     console.log(Date.now(), notificationInfo, 'test notification', previousTimePush, this.handleFormatDate(previousTimePush))
        // Force to show notification in the notification center.
        PushNotification.localNotification({
          title: notificationInfo.title,
          alertTitle: notificationInfo.title,
          message: notificationInfo.body,
        })

      } else if (notification.userInteraction === true) {
        // Navigate the route.
      }
    }

    // console.log(notification, 'listNotification')
  }
  
  render() {
    const { dispatch, nav } = this.props;

    return (
      <Provider store={store}>
        <Root />
      </Provider>
    )
  }
}
