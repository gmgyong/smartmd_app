import React, { Component } from 'react'
import {
  View,
  ListView,
  Text,
  Image,
  TouchableHighlight,
  Dimensions,
} from 'react-native'
import moment from 'moment'
import { Icon } from 'react-native-elements'
import I18n from 'react-native-i18n'

import { Icons } from '../../configs/icons'
import { Colors } from '../../configs/constants'
import localStyles from './localStyles'

export default class ListAppointments extends Component {
  constructor(props) {
    super(props)
    
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

    this.state = {
      dataSource: ds.cloneWithRows([]),
    }
  }

  loadDetail = (data) => {
    const {navigate} = this.props.navigation
    let status = data.status || data.doctor_response_type
    console.log("DATA APPOINTMENT:")
    console.log(data)

    switch (status) {
      case 'Ready':
        navigate('AppointmentDetailPatient',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      case 'Upcoming':
        navigate('AppointmentDetailPatient',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      case 'Ongoing':
        navigate('AppointmentDetailPatient',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      case 'Awaiting Response':
        navigate('AppointmentDetailPatient',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      case 'Waiting':
        navigate('AppointmentDetailPatient',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      case 'Past':
        navigate('SummaryAppointment',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      case 'Patient No Show':
        navigate('SummaryAppointment',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      case 'Both No Show':
        navigate('SummaryAppointment',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      case 'Cancelled':
        navigate('AppointmentDetailPatient',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      case 'Error':
        navigate('AppointmentDetailPatient',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
      default:
        navigate('AppointmentDetailPatient',{user_id: data.user_id, appointment_id: data.appointment_id, appointment_request_id: data.appointment_request_id});
        break
    }
    
  }

  typeStatus = (status) => {
    switch (status) {
      case 'Ready':
        return localStyles.readyStatus
        break
      case 'Upcoming':
        return localStyles.upComingStatus
        break
      case 'Ongoing':
        return localStyles.onGoingStatus
        break
      case 'Awaiting Response':
        return localStyles.awaitingStatus
        break
      case 'Waiting':
        return localStyles.awaitingStatus
        break
      case 'Past':
        return localStyles.pastStatus
        break
      case 'Patient No Show':
        return localStyles.noShowStatus
        break
      case 'Both No Show':
        return localStyles.noShowStatus
        break
      case 'Cancelled':
        return localStyles.cancelStatus
        break
      case 'Error':
        return localStyles.cancelStatus
        break
      default:
        return localStyles.defaultStatus
        break
    }
  }

  renderItem(data, sectionID, rowID, highlightRow) {
    // const checkSelected = moment(this.props.selectedDate).format('MMMM DD, YYYY') == `${data.date}`;
    let status = data.status || data.doctor_response_type
    let checkStatus = this.typeStatus(status)

    return (
      <TouchableHighlight
        underlayColor='#dcccb0'
        style={localStyles.blockDoctor}
        onPress={ ()=> this.loadDetail(data) }>
        <View style={localStyles.blockDetail}>
          {/*<Image*/}
            {/*source={{uri: data.doctor_image}}*/}
            {/*style={localStyles.blockPhoto}*/}
          {/*/>*/}

          <View style={ localStyles.figure }>
            { data.doctor_image !== null && data.doctor_image !== "" ? <Image source={{uri: data.doctor_image}} style={ [localStyles.figure, localStyles.blockPhoto] }/> : null }
          </View>
          <View style={[localStyles.blockInfo, {flex: 2}]}>
            <Text style={localStyles.textInfo}>
              {`${data.doctor_name}`}
            </Text>
            <Text style={localStyles.textInfo}>
              { I18n.t("home.status") }: <Text style={checkStatus}>{status}</Text>
            </Text>
            <Text style={localStyles.textDateTime}>
              {data.scheduled_time}
            </Text>
          </View>
          <View style={localStyles.blockIcon}>
            <Icon size={14} name="chevron-right" type='font-awesome' color={Colors.TEXT_SECONDARY} />
          </View>
        </View>
      </TouchableHighlight>
    )
  }

  render() {
    const { listAppointments, listRef } = this.props

    return (
      <ListView
        ref={listRef}
        dataSource={this.state.dataSource.cloneWithRows(listAppointments)}
        renderRow={(data, sectionID, rowID, highlightRow) => this.renderItem(data, sectionID, rowID, highlightRow) } />
    )
  }
}
