import React, { Component } from 'react'
import {
  View,
  ListView,
  Text,
  Image,
  TouchableHighlight,
  Dimensions,
} from 'react-native'
import moment from 'moment'
import { Icon } from 'react-native-elements'
import I18n from 'react-native-i18n'
import FormatDate from '../../configs/formatDate'
import { Icons } from '../../configs/icons'
import { Colors } from '../../configs/constants'
import localStyles from './localStyles'
import globalStyles from '../../styles'

export default class ListFastConnect extends Component {
  constructor(props) {
    super(props)

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

    this.state = {
      dataSource: ds.cloneWithRows([]),
    }
  }

  detailFastConnect = (data) =>{
      const {navigate} = this.props.navigation
      navigate('FastConnect',{idFastConnectDetails: data.id});
  }

  renderItem(data, sectionID, rowID, highlightRow) {
    return (
      <TouchableHighlight
        underlayColor='#dcccb0'
        style={localStyles.blockDoctor}
          onPress={ ()=> this.detailFastConnect(data) }>
        <View style={localStyles.blockDetail}>
          {/*<Image*/}
            {/*source={{uri: data.doctor_photo_url}}*/}
            {/*style={localStyles.blockPhoto} />*/}

          <View style={ localStyles.figure }>
            { data.doctor_photo_url !== null && data.doctor_photo_url !== "" ? <Image source={{uri: data.doctor_photo_url}} style={ [localStyles.figure, localStyles.blockPhoto] }/> : null }
          </View>

          <View style={[localStyles.blockInfo, {flex: 2}]}>
            <Text style={localStyles.textInfo}>
              {`${data.doctor_name}`}
            </Text>
            <Text style={localStyles.textInfo}>
            { I18n.t("home.status") }:
              <Text style={localStyles.awaitingStatus}> { I18n.t("home.awaitingStatus") }</Text>
            </Text>
            <Text style={localStyles.textDateTime}>
              {FormatDate.switchToLocalTime(data.appointment_time)}
            </Text>
          </View>
          <View style={localStyles.blockIcon}>
            <Icon size={14} name="chevron-right" type='font-awesome' color={Colors.TEXT_SECONDARY} />
          </View>
        </View>
      </TouchableHighlight>
    )
  }

  render() {
    const { listFastConnect, listRef } = this.props

    return (
      <ListView
        ref={listRef}
        dataSource={this.state.dataSource.cloneWithRows(listFastConnect)}
        renderRow={(data, sectionID, rowID, highlightRow) => this.renderItem(data, sectionID, rowID, highlightRow) } />
    )
  }
}
