import React, { Component } from 'react'
import {
  View,
  ListView,
  Text,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  Dimensions,
} from 'react-native'
import moment from 'moment'
import { Icon } from 'react-native-elements'
import I18n from 'react-native-i18n'
import Communications from 'react-native-communications'

import { Icons } from '../../configs/icons'
import { Colors } from '../../configs/constants'
import FormatDate from '../../configs/formatDate'
import localStyles from './localStyles'
import globalStyles from '../../styles'

export default class ListChatLog extends Component {
  constructor(props) {
    super(props)
    
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

    this.state = {
      dataSource: ds.cloneWithRows([]),
    }
  }

  handleGetUploadFile = text => {
    // get all index of char {"} in message
    let listChar1 = []
    let listChar2 = []
    let lengthMes = text.length

    for (let i = 0; i < lengthMes; i++) {
      text[i] === '"' && listChar1.push(i)
      text[i] === '>' && listChar2.push(i)
    }

    let url = text.slice((listChar1[2] +1), listChar1[3])
    let name = text.slice((listChar2[0] +1), (lengthMes - 4))
    // console.log(url, name)
    return (
      {
        url,
        name
      }
    )
  }

  renderItem(data, sectionID, rowID, highlightRow) {
    const { doctorId } = this.props
    let checkURLOnWeb = false
    let getFile

    if(data.text.indexOf('<a target="_blank" ') !== -1) {
      getFile = this.handleGetUploadFile(data.text)

      checkURLOnWeb = true
    }

    return (
      <View style={[globalStyles.chatLogBlock, data.user_id === doctorId ? globalStyles.chatLogDoctor : globalStyles.chatLogPatient,]}>
        { checkURLOnWeb ? <TouchableOpacity onPress={()=> Communications.web(getFile.url)}>
          <Text style={[globalStyles.normalText, data.user_id === doctorId ? null : {textAlign: 'right'}, {paddingTop: 0}]}>
            Upload file: <Text style={{color: '#35c1cf'}}>{ getFile.name }</Text>
          </Text>
        </TouchableOpacity> : <Text style={[globalStyles.normalText, data.user_id === doctorId ? null : {textAlign: 'right'}, {paddingTop: 0}]}>{`${data.text}`}</Text>}
        <Text style={[globalStyles.chatLogTimeText, data.user_id === doctorId ? null : {textAlign: 'right'}]}>
          {FormatDate.switchToLocalTime(data.created_at)}
        </Text>
      </View>
    )
  }

  render() {
    const { listChatLog, listRef } = this.props

    return (
      <View style={globalStyles.chatLogContainer}>
        <View style={globalStyles.chatLogWrapper}>
          <ListView
            ref={listRef}
            dataSource={this.state.dataSource.cloneWithRows(listChatLog)}
            renderRow={(data, sectionID, rowID, highlightRow) => this.renderItem(data, sectionID, rowID, highlightRow) } />
        </View>
      </View>
    )
  }
}
