import React, { Component } from 'react'
import {
  View,
  ListView,
  Text,
  Image,
  TouchableHighlight,
  Dimensions,
} from 'react-native'
import moment from 'moment'
import { Icon } from 'react-native-elements'
import I18n from 'react-native-i18n'

import { Icons } from '../../configs/icons'
import { Colors } from '../../configs/constants'
import localStyles from './localStyles'
import globalStyles from '../../styles'

export default class ListBillings extends Component {
  constructor(props) {
    super(props)
    
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

    this.state = {
      dataSource: ds.cloneWithRows([]),
    }
  }

  billingDetailPatient = (data) => {
    const {navigate} = this.props.navigation
    console.log(data)
    navigate('BillingDetailPatient',{invoice_id: data.invoice_id});
  }

  informationSummaryAppointment = (data) => {
    const {navigate} = this.props.navigation
    console.log(data)
    navigate('SummaryAppointment',{appointment_id: data.appointment_id});
  }
  
  renderItem(data, sectionID, rowID, highlightRow) {
    return (
      <View style={localStyles.blockInvoice}>
          <View style={[localStyles.blockInvoiceInfo, {flex: 1}]}>
            <Text style={localStyles.textInfo}>
              <Text style={localStyles.pastStatus}>{I18n.t('billing.invoiceNo')}</Text> {`${data.invoice_no}`}
            </Text>
            <Text style={localStyles.textInfo}>
              <Text style={localStyles.pastStatus}>{I18n.t('billing.doctor')}</Text> {`${data.doctor_name}`}
            </Text>
            <Text style={localStyles.textInfo}>
              <Text style={localStyles.pastStatus}>{I18n.t('billing.amount')}</Text> {`${data.amount}`} SGD
            </Text>
          </View>
          <TouchableHighlight
            underlayColor='#dcccb0'
            style={[localStyles.blockInvoiceButton, localStyles.blockInvoiceDetailButtonBackground]}
            onPress={ ()=> this.billingDetailPatient(data) }>
            <Text style={localStyles.blockInvoiceButtonText}>{I18n.t('buttons.btnInvoice').toUpperCase()}</Text>
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor='#dcccb0'
            style={[localStyles.blockInvoiceButton, localStyles.blockInvoiceInfoButtonBackground]}
            onPress={ ()=> this.informationSummaryAppointment(data) }>
            <Text style={localStyles.blockInvoiceButtonText}>{I18n.t('buttons.btnInfo').toUpperCase()}</Text>
          </TouchableHighlight>
      </View>
    )
  }

  render() {
    const { listInvoices, listRef } = this.props

    return (
      <ListView
        ref={listRef}
        dataSource={this.state.dataSource.cloneWithRows(listInvoices)}
        renderRow={(data, sectionID, rowID, highlightRow) => this.renderItem(data, sectionID, rowID, highlightRow) } />
    )
  }
}
