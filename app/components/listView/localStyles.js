import { StyleSheet, Dimensions } from 'react-native'
import { Colors, FontSizes } from '../../configs/constants'

const window = Dimensions.get('window')

const localStyles = StyleSheet.create({
  blockInvoice: {
    flex: 1,
    flexDirection:'row',
    marginTop: 10,
  },
  blockInvoiceInfo: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 10,
    paddingLeft: 10,
    backgroundColor: Colors.BACKGROUND_FIELD,
  },
  blockInvoiceButton: {
    width: 80,
    justifyContent: 'center',
  },
  blockInvoiceDetailButtonBackground: {
    backgroundColor: Colors.STATUS_PAST,
  },
  blockInvoiceInfoButtonBackground: {
    backgroundColor: Colors.BACKGROUND_BUTTON_GREY,
  },
  blockInvoiceButtonText: {
    color: Colors.TEXT_TABS_ICON,
    textAlign: 'center',
  },
  blockDoctor: {
    backgroundColor: Colors.BACKGROUND_FIELD,
    marginTop: 10,
  },
  blockDetail: {
    flex: 1,
    flexDirection:'row',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  blockPhoto: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderColor: Colors.TEXT_TABS_ICON,
    borderWidth: 1,
    marginRight: 10,
    overflow: 'hidden',
  },
  figure: {
    width: 50,
    height: 50,
    borderRadius: 25,
    // backgroundColor: '#ccc',
    overflow: 'hidden',
    marginRight: 10,
  },
  blockInfo: {
    paddingTop: 2,
  },
  blockIcon: {
    paddingTop: 2,
    paddingRight: 10,
    justifyContent: 'center'
  },
  textInfo: {
    flexDirection: 'row',
    fontSize: FontSizes.NORMAL,
  },
  textDateTime: {
    flexDirection: 'row',
    fontSize: FontSizes.NORMAL,
    color: Colors.TEXT_SECONDARY,
  },
  readyStatus: {
    color: Colors.STATUS_READY,
  },
  upComingStatus: {
    color: Colors.STATUS_UP_COMING,
  },
  onGoingStatus: {
    color: Colors.STATUS_ON_GOING,
  },
  noScheduledStatus: {
    color: Colors.STATUS_NO_SCHEDULED,
  },
  noShowStatus: {
    color: Colors.STATUS_NO_SHOW,
  },
  pastStatus: {
    color: Colors.STATUS_PAST,
  },
  cancelStatus: {
    color: Colors.STATUS_CANCEL,
  },
  awaitingStatus: {
    color: Colors.STATUS_AWAITING,
  },
  defaultStatus: {
    color: Colors.TEXT_PRIMARY
  }
})

/** export module */
module.exports = localStyles
