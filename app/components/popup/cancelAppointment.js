import React, { Component, } from 'react'
import { Text, View, Image, Modal, TouchableHighlight, TouchableWithoutFeedback, TextInput } from 'react-native'
import I18n from 'react-native-i18n'
import {Button} from 'react-native-elements'
import TopLabelField from '../../components/form/TopLabelField'
import localStyles from './localStyles'
import globalStyles from '../../styles'

export default class PopupCancelAppointment extends Component {
  constructor(props) {
      super(props)

      this.state = {
        reason: '',
        error: undefined,
      }
  }

  _onClose() {
    this.setModalVisible(!this.props.modalVisible);
    this.setState({ error: false})
  }

  _onRequestClose() {

  }

  setModalVisible(visible) {
    this.props.setModalVisible(visible);
  }

  onPressCheckValid = async () => {
    let reason = await this.state.reason.trim()
    if(reason === ''){
      this.setState({ error : true})
    }else {
      this.onPressConfirm()
    }
  }

  onPressConfirm() {
    this.setModalVisible(!this.props.modalVisible);
    this.props.callAPICancelAppointment(this.state.reason);
  }

  render() {
    return (
      <View>
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.props.modalVisible}
          onRequestClose={() => {this._onRequestClose();}} >
          <TouchableWithoutFeedback
              onPress={() => this._onClose()}>
            <View style={localStyles.popupWrapper}>
              <View style={localStyles.popupContainer}>
                <View style={ localStyles.popupContent}>
                  <TopLabelField
                    label={ I18n.t("appointmentDetailPatient.cancelTitle") }
                    error={ this.state.error }
                    errorMes={ I18n.t("appointmentDetailPatient.reason") }
                    onChangeText={ reason => this.setState({reason}) }
                  />
                </View>

                <View style={[localStyles.popupClose]}>
                  <Button
                    title={ I18n.t("buttons.btnConfirm").toUpperCase() }
                    onPress={ () => this.onPressCheckValid() }
                    buttonStyle={ [globalStyles.buttonBlue, globalStyles.button] }
                    containerViewStyle={ [globalStyles.buttonView, { marginTop: 0, flex: 1 }] }
                  />
                  <Button
                    title={ I18n.t("buttons.btnCancel").toUpperCase() }
                    onPress={ () => this._onClose() }
                    buttonStyle={ [globalStyles.buttonGrey, globalStyles.button] }
                    containerViewStyle={ [globalStyles.buttonView, { marginTop: 0, flex: 1, marginLeft: 10, }] }
                  />
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  }
}
