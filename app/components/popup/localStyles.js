import { StyleSheet, Dimensions, } from 'react-native';
import { FontNames, Colors, FontSizes, SPACE } from '../../configs/constants'

var styles = StyleSheet.create({
  /* popup */
  popupWrapper: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(82,82,82,0.7)'
  },
  popupContainer: {
    backgroundColor: '#f9f9f9',
    borderRadius: 10,
    paddingTop: 20,
    paddingBottom: 15,
    marginHorizontal: SPACE,
  },
  popupContent: {
    paddingHorizontal: 30,
  },
  popupClose: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#cfcfcf',
    paddingTop: 15,
    paddingHorizontal: 30,
  },
})

module.exports = styles;
