/**
 * Created by user on 6/16/17.
 */
import React from 'react';
import { View, Image, StyleSheet, Animated } from 'react-native';
import images from '../../configs/images';

export default class Loading extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flexAnimation: new Animated.Value(2),
        }
    }

    componentDidMount() {
        Animated.loop(
            Animated.sequence([
                Animated.timing(
                    this.state.flexAnimation, { toValue: 1, duration: 1000 }
                ),
                Animated.timing(
                    this.state.flexAnimation, { toValue: 2, duration: 1000 }
                )
            ])
        )
            .start();
    }

    render() {
        return (
            <View style={ styles.container }>
                {/*<Image source={ images.bgSignIn } style={ styles.bg } />*/}
                {/*<View style={{ flex: 1 }} />*/}
                <View style = {{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flex: 1 }} />
                    <Image source={ images.logo } style={ styles.logo } />
                    {/*<Animated.Image*/}
                        {/*source={ images.logo }*/}
                        {/*style={{*/}
                            {/*flex: this.state.flexAnimation,*/}
                            {/*width: null,*/}
                            {/*resizeMode: 'contain',*/}
                        {/*}}*/}
                    {/*/>*/}
                    <View style={{ flex: 1 }} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    bg: {
        width: null,
        height: null,
        resizeMode: 'stretch',
        position: 'absolute',
        top: 0, left: 0, right: 0, bottom: 0,
    },
    logo: {
        flex: 2,
        width: null,
        resizeMode: 'contain',
    }
});