import React, { PropTypes, Component } from 'react'
import {View, Image, Text, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Icon } from 'react-native-elements'

import { Colors, } from '../../configs/constants'
import globalStyles from '../../styles'

export default class Card extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      isDataLoading: false
    }
  }

  renderBtnStart = () => {
    const { onPressBtnStart } = this.props

    return(
      <TouchableOpacity style={[styles.figure, styles.figureImage, styles.btnStart]} onPress={ onPressBtnStart }>
        <Icon
          name='play'
          type='font-awesome'
          color='#FFF'
          size={36}
        />
        <Text style={ styles.textWhite }>START</Text>
      </TouchableOpacity>
    )
  }

  renderBtnCancel = () => {
    const { onPressBtnCancel } = this.props

    return(
      <TouchableOpacity
        style={[styles.figure, styles.figureImage, styles.btnCancel]}
        onPress={ onPressBtnCancel }
      >
        <Icon
          name='times'
          type='font-awesome'
          color='#FFF'
          size={36}
        />
        <Text style={ styles.textWhite }>CANCEL</Text>
      </TouchableOpacity>
    )
  }

  renderLoadingIndicator = (isDataLoading) => {
    if (isDataLoading) {
      console.log(isDataLoading, 'checking')
      return (
        <ActivityIndicator
          animating={isDataLoading}
          style={[globalStyles.indicatorPageLoading, {backgroundColor: 'transparent'}]}
          color={Colors.TEXT_SECONDARY}
          size='small'
        />
      )
    } else {
      return null
    }
  }

  renderImage = () => {
    const { userImage } = this.props

    return (
      <View style={{position: 'relative'}}>
        <Image
          source={{uri: userImage}}
          style={ [styles.figureImage, styles.userImage] }
          onLoadStart={(e) => {
            if(userImage !== null || userImage !== "") {
              this.setState({isDataLoading: true})
            }
          }}
          onLoadEnd={(e) => { 
            if(userImage !== null || userImage !== "") {
              this.setState({isDataLoading: false})
            }
          } } />
        
        { this.state.isDataLoading ? this.renderLoadingIndicator(this.state.isDataLoading) : null }
      </View>
    )
  }

    

  render () {
    const {backgroundImage, userImage, heading, subHeading, btnStart, btnCancel} = this.props

    return (
      <View style={ styles.container }>
        <Image source={ backgroundImage } style={ styles.backgroundImage }/>
        <View style={styles.boxImage }>
          <View style={ styles.figure }>
            { userImage !== null || userImage !== "" ? this.renderImage() : null }
          </View>
          {btnStart ? this.renderBtnStart() : null}
          {btnCancel ? this.renderBtnCancel() : null}
        </View>
        <Text style={ styles.h3 }>{ heading && heading.toUpperCase() }</Text>
        <View style={ styles.line }/>
        <Text style={ styles.h5 }>{ subHeading }</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#d7d7d7',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
    paddingBottom: 20,
    height: 220,
    position: 'relative',
    overflow: 'hidden'
  },
  backgroundImage: {
    position: 'absolute',
    flex: 1,
    resizeMode: 'cover',
  },
  boxImage: {
    flexDirection: 'row'
  },
  figure: {
    width: 110,
    height: 110,
    backgroundColor: '#ccc',
    borderRadius: 55,
    overflow: 'hidden',
    marginHorizontal: 10,
  },
  figureImage: {
    width: 110,
    height: 110,
    borderRadius: 55,
    borderWidth: 1,
    borderColor: '#ffffff',
  },
  userImage: {
    resizeMode: 'cover',
  },
  line: {
    width: 25,
    height: 1,
    backgroundColor: '#555',
    marginBottom: 5
  },
  h3: {
    fontSize: 14,
    fontWeight: 'bold',
    lineHeight: 18,
    marginTop: 15,
    marginBottom: 5,
    backgroundColor: 'transparent'
  },
  h5: {
    fontSize: 11,
    lineHeight: 15,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: 'transparent'
  },
  textWhite: {
    color: '#FFF',
    fontSize: 10,
    marginBottom: -5,
    marginTop: 5
  },
  btnStart: {
    backgroundColor: "#00aff0",
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnCancel: {
    backgroundColor: "#555555",
    alignItems: 'center',
    justifyContent: 'center'
  }
})
