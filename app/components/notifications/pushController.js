import React, { Component } from 'react';
import { Platform } from 'react-native';
import PushNotification from 'react-native-push-notification';
import Environments from '../../configs/environmentsAPIs';

export default class PushController extends Component {
  componentDidMount() {
    let options = {
      onRegister: (token) => {
        // this.setDeviceToken(token)
        console.log('>>>>> New device token for push notification: ' + JSON.stringify(token))
      },
      onNotification: (notification) => {
        // this.handleNotification(notification)
        console.log('>>>>> New notification with data: ' + JSON.stringify(notification))
      },
      popInitialNotification: true,
      requestPermissions: true,
    }

    Platform.OS === 'ios' ? options.permissions = {
      alert: true,
      badge: true,
      sound: true
    } : options.senderID = Environments.PUSH_NOTIFICATION_SENDER_ID

    PushNotification.configure(options);
  }

  render() {
    return null;
  }
}
