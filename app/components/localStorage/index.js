import React from 'react';

import { AsyncStorage } from 'react-native';
import Logger from '../../configs/logger';

class LocalStorage {
  constructor() {}

  async setString(key, value) {
    if (key && key !== '' && value) {
      Logger.log('***** Save value: ' + value + ' for key: ' + key);
      try {
        await AsyncStorage.setItem(key, value);
      } catch (error) {
        Logger.log('***** AsyncStorage error: ' + error.message);
      }
    }
  }

  async getString(key) {
    if (key && key !== '') {
      try {
        const value = await AsyncStorage.getItem(key);

        // Logger.log('***** Key: ' + key + ' has value: ' + value);

        return value;
      } catch (error) {
        Logger.log('***** AsyncStorage error: ' + error.message);
      }
    }

    return '';
  }

  async setJson(key, value) {
    if (key && key !== '' && value) {
      try {
        await AsyncStorage.setItem(key, JSON.stringify(value));
      } catch (error) {
        Logger.log('***** AsyncStorage error: ' + error.message);
      }
    }
  }

  async getJson(key) {
    if (key && key !== '') {
      try {
        return await AsyncStorage.getItem(key,(err, result) => {
          return result
        })
      } catch (error) {
        Logger.log('***** AsyncStorage error: ' + error.message);
      }
    }

    return null;
  }

  removeKey(key) {
    AsyncStorage.removeItem(key);
    return null;
  }
}

module.exports = new LocalStorage();
