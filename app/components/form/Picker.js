import React from 'react'
import { View, StyleSheet } from 'react-native'
import ModalPicker from 'react-native-modal-picker'
import {TextField, Label, ErrorMes } from './Field'

export default class Picker extends React.Component {
    render() {
        const { data, placeholder, onChange, error, value, label, errorMes } = this.props;
        return(
            <View>
                { (label) ? <Label error={ error } label={ label } /> : null }
                <ModalPicker
                    data={data}
                    initValue={ placeholder }
                    onChange={ onChange }
                    cancelText="Cancel"
                    style={ styles.formControl }
                >
                    <TextField
                        placeholder={ placeholder }
                        editable={ false }
                        error={ error }
                        value={ value }
                        arrow={ true }
                    />
                </ModalPicker>
                { (label && error) ? <ErrorMes errorMes={ errorMes } /> : null }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    formControl: {
        marginBottom: 15,
    }
});


