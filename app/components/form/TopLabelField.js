import React from 'react'
import { View, StyleSheet } from 'react-native'

import { TextField, Label, ErrorMes } from './Field'

const TopLabelField = (props) => {
    const {
        label,
        placeholder,
        onChangeText,
        onFocus,
        error,
        errorMes,
        arrow,
        secureTextEntry,
        editable,
        value,
        returnKeyType,
        onSubmitEditing,
        inputRef,
        multiline,
        numberOfLines,
        blurOnSubmit,
        calendarIcon
    } = props

    return(
        <View>
            <View style={ styles.formGroup }>
                <Label error={ error } label={ label } />
                <TextField
                    error={ error }
                    placeholder={ placeholder }
                    onChangeText={ onChangeText }
                    onFocus={ onFocus }
                    secureTextEntry={ secureTextEntry }
                    value={ value }
                    editable={ editable }
                    returnKeyType={ returnKeyType }
                    onSubmitEditing={ onSubmitEditing }
                    inputRef={ inputRef }
                    multiline={ multiline }
                    numberOfLines={ numberOfLines }
                    calendarIcon={ calendarIcon }
                    blurOnSubmit={ blurOnSubmit }
                />
            </View>
            { (error) ? <ErrorMes arrow={ arrow } errorMes={ errorMes } /> : null }
        </View>
    )
}

const styles = StyleSheet.create({
    formGroup: {
        // flex: 1,
        // width: 200,
        marginBottom: 15,
        overflow: 'hidden',
    },
    formControl: {
        flex: 1,
        height: 35,
        paddingLeft: 15,
        paddingRight: 15,
        color: '#555',
        fontSize: 12,
        backgroundColor: '#f5f5f5',
        borderColor: '#959595',
        borderWidth: 1,
        borderStyle: 'solid',
        borderRadius: 4,
        overflow: 'hidden',
    },
    formControlError: {
        borderColor: '#f23d2c',
    }
});


export default TopLabelField
