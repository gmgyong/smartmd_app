import React from 'react'
import { View, Text, TextInput, StyleSheet, Platform } from 'react-native'

import { ErrorMes } from './Field'

const InlineLabelField = (props) => {
    const {
        label,
        placeholder,
        onChangeText,
        onFocus,
        error,
        errorMes,
        arrow,
        secureTextEntry,
        inputRef,
        returnKeyType,
        onSubmitEditing,
        value
    } = props;

    return(
        <View>
            <View style={ (error) ? [styles.formGroup, styles.formGroupError] : styles.formGroup }>
                <Text style={ (error) ? [styles.label, styles.labelError] : styles.label }>{ label }</Text>
                <TextInput
                    value={ value }
                    style={ styles.formControl }
                    placeholder={ placeholder }
                    underlineColorAndroid="transparent"
                    onChangeText={ onChangeText }
                    onFocus={ onFocus }
                    placeholderTextColor="#a4a4a4"
                    secureTextEntry={ secureTextEntry }
                    ref={ inputRef }
                    returnKeyType={ returnKeyType }
                    onSubmitEditing={ onSubmitEditing }
                />
            </View>
            { (error) ? <ErrorMes arrow={ arrow } errorMes={ errorMes } /> : null }
        </View>
    );
}


const styles = StyleSheet.create({
    formGroup: {
        // flex: 1,
        // width: 200,
        height: 35,
        marginBottom: 15,
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderColor: '#35c1cf',
        borderWidth: 1,
        borderStyle: 'solid',
        borderRadius: 4,
        overflow: 'hidden',
    },
    formGroupError: {
        borderColor: '#f23d2c',
    },
    label: {
        paddingLeft: 10, paddingRight: 10, paddingTop: Platform.OS === 'ios' ? 10 : 7,
        minWidth: 95,
        color: '#555',
        fontSize: 12,
        // lineHeight: 33,
    },
    labelError: {
        color: '#f23d2c',
    },
    formControl: {
        flex: 1,
        height: 36,
        paddingLeft: 10, paddingRight: 10, paddingTop: Platform.OS === 'ios' ? 0 : 7,
        color: '#555',
        fontSize: 12,
        textAlign: 'center',
    },
});

export default InlineLabelField
