import React from 'react'
import {View, Image, Text, TextInput, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export const TextField = (props) => {
  const {
    placeholder,
    onChangeText,
    onFocus,
    error,
    secureTextEntry,
    value,
    editable,
    arrow,
    returnKeyType,
    onSubmitEditing,
    inputRef,
    multiline,
    numberOfLines,
    blurOnSubmit,
    calendarIcon
  } = props

  return (
    <View>
      <TextInput
        style={ (error) ? [styles.formControl, styles.formControlError, !multiline ? styles.formControlOneLine : null] : [styles.formControl, !multiline ? styles.formControlOneLine : null] }
        placeholder={ placeholder }
        underlineColorAndroid="transparent"
        onChangeText={ onChangeText }
        onFocus={ onFocus }
        placeholderTextColor="#a4a4a4"
        secureTextEntry={ secureTextEntry }
        value={ value }
        editable={ editable }
        returnKeyType={ returnKeyType }
        onSubmitEditing={ onSubmitEditing }
        ref={ inputRef }
        multiline={ multiline }
        blurOnSubmit={ blurOnSubmit }
        numberOfLines={ numberOfLines }
      />
      {(arrow) ? <Icon name="angle-down" size={15} color="#7d7d7d" style={styles.formArrow}/> : null }
      {(calendarIcon) ? <Icon name="calendar" size={15} color="#7d7d7d" style={styles.formArrow}/> : null }
    </View>

  );
};

export const Label = (props) => {
  const {error, label, style} = props;
  return (
    <Text style={ (error) ? [styles.label, styles.labelError, style] : [styles.label, style] }>{ label }</Text>
  );
};

export const ErrorMes = (props) => {
  const {arrow, errorMes, mesType, styleContainer} = props;

  const handleMesType = () => {
    switch (mesType) {
      case 'SUCCESS':
        return styles.successMes
      case 'WARNING':
        return styles.warningMes
      default:
        return styles.errorMes
    }
  }

  return (
    <View style={ [styles.errorGroup, styleContainer] }>
      { arrow === false ? null : <Image source={ images.arrowMes } style={ styles.errorArrow }/> }
      <Text style={ [styles.error, handleMesType(), props.style] }>{ errorMes }</Text>
    </View>
  );
};

const images = {
  arrowMes: require('./images/arrowError.png'),
};

const styles = StyleSheet.create({
  formGroup: {
    // flex: 1,
    // width: 200,
    marginBottom: 15,
    overflow: 'hidden',
  },
  label: {
    marginBottom: 10,
    color: '#35c1cf',
    fontSize: 12,
  },
  labelError: {
    color: '#f23d2c',
  },
  formControlOneLine: {
    // flex: 1,
    height: 35,
  },
  formControl: {
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 7,
    color: '#555',
    fontSize: 12,
    lineHeight: 15,
    backgroundColor: '#f5f5f5',
    borderColor: '#959595',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 4,
    overflow: 'hidden',
  },
  formControlError: {
    borderColor: '#f23d2c',
  },
  formArrow: {
    position: 'absolute',
    right: 10, top: 10,
    backgroundColor: 'transparent',
  },
  errorGroup: {
    position: 'relative',
    marginBottom: 15,
  },
  error: {
    fontSize: 11,
    lineHeight: 14,
    textAlign: 'center',
    paddingTop: 7,
    paddingBottom: 7,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 4,
    overflow: 'hidden'
  },
  errorMes: {
    color: '#FFF',
    backgroundColor: '#f23d2c',
  },
  warningMes: {
    color: '#FFF',
    backgroundColor: '#f0ad4e',
  },
  successMes: {
    color: '#FFF',
    backgroundColor: '#5cb85c',
  },
  errorArrow: {
    position: 'absolute',
    top: -7,
    right: 10,
  }
});
