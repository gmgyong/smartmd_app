import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import { Button } from 'react-native-elements'

const ItemView = (props) => {
  const { navigate, titleBtnInfo, titleBtnAppointment, doctor_profile, user_id } = props

  const info = {
    user_id: user_id,
    doctor_id: doctor_profile.doctor_id,
    name: doctor_profile.name,
    specialty: doctor_profile.specialty.name,
    photo_url: doctor_profile.user.photo_url,
    price_breakdown: doctor_profile.price_breakdown,
    price_comments: doctor_profile.price_comments,
    subspecialties: doctor_profile.subspecialties,
    background: doctor_profile.background,
    qualifications: doctor_profile.qualifications
  }

  const onPressInfo = () => {
    navigate('InfoDoctor',{ info: info });
  }

  const onPressMakeAppointment = () => {
    navigate('MakeAppointment',{ info: info });
  }

  return(
    <View style={ localStyles.container }>
      <View style={ localStyles.figure }>
        <Image
          style={ localStyles.image }
          source={{ uri: info.photo_url }}
        />
      </View>
      <View style={ localStyles.content }>
        <Text style={ localStyles.doctorName }> { info.name }</Text>
        <Text style={ localStyles.doctorType }>{ info.specialty }</Text>
        <Button
          title={ titleBtnInfo }
          onPress={ onPressInfo }
          buttonStyle={ localStyles.buttonGray }
          containerViewStyle={ localStyles.button }
        />
        <Button
          title={ titleBtnAppointment }
          onPress={ onPressMakeAppointment }
          buttonStyle={ localStyles.buttonBlue }
          containerViewStyle={ localStyles.button }
        />
      </View>
    </View>
  )
}

const localStyles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    // marginTop: 10,
    // marginBottom: 10,
    paddingTop: 15,
    paddingBottom: 15,
    paddingHorizontal: 10,
    flexDirection: 'row',

  },
  figure: {
    width: 120,
    height: 120,
    backgroundColor: '#fff',
    borderColor: '#27909a',
    borderWidth: 1,
    overflow: 'hidden'
  },
  image: {
    flex: 1,
    resizeMode: 'contain'
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 20,
  },
  doctorName: {
    fontSize: 14,
    lineHeight: 18,
    fontWeight: 'bold',
    color: '#555',
    marginBottom: 2
  },
  doctorType: {
    fontSize: 11,
    lineHeight: 13,
    color: '#555',
    marginBottom: 8,
    marginLeft: 3
  },
  button: {
    marginLeft: 0, marginRight: 0,
  },
  buttonGray: {
    backgroundColor: '#bcbcbc',
    height: 35,
    borderRadius: 4,
    marginBottom: 10
  },
  buttonBlue: {
    backgroundColor: '#35c1cf',
    height: 35,
    borderRadius: 4,
  },
})

export default ItemView
