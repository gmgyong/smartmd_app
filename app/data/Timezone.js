
// export const dataTimezone = [
//   {
//     "key": index++,
//     "value": "UTC+7",
//     "label": "Ha Noi, Viet Nam (UTC+7:00)"
//   },
//   {
//     "key": index++,
//     "value": "UTC+8",
//     "label": "Singapore (UTC+8:00)"
//   }
// ]
export const dataTimezone =
[
    {
        "key": 274,
        "value": "Asia/Singapore",
        "label": "Asia/Singapore - GMT+8"
    },
    {
        "key": 237,
        "value": "Asia/Ho_Chi_Minh",
        "label": "Asia/Ho Chi Minh - GMT+7"
    },
    {
        "key": 0,
        "value": "Africa/Abidjan",
        "label": "Africa/Abidjan - GMT0"
    },
    {
        "key": 1,
        "value": "Africa/Accra",
        "label": "Africa/Accra - GMT0"
    },
    {
        "key": 2,
        "value": "Africa/Addis_Ababa",
        "label": "Africa/Addis Ababa - GMT+3"
    },
    {
        "key": 3,
        "value": "Africa/Algiers",
        "label": "Africa/Algiers - GMT+1"
    },
    {
        "key": 4,
        "value": "Africa/Asmara",
        "label": "Africa/Asmara - GMT+3"
    },
    {
        "key": 5,
        "value": "Africa/Bamako",
        "label": "Africa/Bamako - GMT0"
    },
    {
        "key": 6,
        "value": "Africa/Bangui",
        "label": "Africa/Bangui - GMT+1"
    },
    {
        "key": 7,
        "value": "Africa/Banjul",
        "label": "Africa/Banjul - GMT0"
    },
    {
        "key": 8,
        "value": "Africa/Bissau",
        "label": "Africa/Bissau - GMT0"
    },
    {
        "key": 9,
        "value": "Africa/Blantyre",
        "label": "Africa/Blantyre - GMT+2"
    },
    {
        "key": 10,
        "value": "Africa/Brazzaville",
        "label": "Africa/Brazzaville - GMT+1"
    },
    {
        "key": 11,
        "value": "Africa/Bujumbura",
        "label": "Africa/Bujumbura - GMT+2"
    },
    {
        "key": 12,
        "value": "Africa/Cairo",
        "label": "Africa/Cairo - GMT+2"
    },
    {
        "key": 13,
        "value": "Africa/Casablanca",
        "label": "Africa/Casablanca - GMT+1"
    },
    {
        "key": 14,
        "value": "Africa/Ceuta",
        "label": "Africa/Ceuta - GMT+2"
    },
    {
        "key": 15,
        "value": "Africa/Conakry",
        "label": "Africa/Conakry - GMT0"
    },
    {
        "key": 16,
        "value": "Africa/Dakar",
        "label": "Africa/Dakar - GMT0"
    },
    {
        "key": 17,
        "value": "Africa/Dar_es_Salaam",
        "label": "Africa/Dar es Salaam - GMT+3"
    },
    {
        "key": 18,
        "value": "Africa/Djibouti",
        "label": "Africa/Djibouti - GMT+3"
    },
    {
        "key": 19,
        "value": "Africa/Douala",
        "label": "Africa/Douala - GMT+1"
    },
    {
        "key": 20,
        "value": "Africa/El_Aaiun",
        "label": "Africa/El Aaiun - GMT+1"
    },
    {
        "key": 21,
        "value": "Africa/Freetown",
        "label": "Africa/Freetown - GMT0"
    },
    {
        "key": 22,
        "value": "Africa/Gaborone",
        "label": "Africa/Gaborone - GMT+2"
    },
    {
        "key": 23,
        "value": "Africa/Harare",
        "label": "Africa/Harare - GMT+2"
    },
    {
        "key": 24,
        "value": "Africa/Johannesburg",
        "label": "Africa/Johannesburg - GMT+2"
    },
    {
        "key": 25,
        "value": "Africa/Juba",
        "label": "Africa/Juba - GMT+3"
    },
    {
        "key": 26,
        "value": "Africa/Kampala",
        "label": "Africa/Kampala - GMT+3"
    },
    {
        "key": 27,
        "value": "Africa/Khartoum",
        "label": "Africa/Khartoum - GMT+3"
    },
    {
        "key": 28,
        "value": "Africa/Kigali",
        "label": "Africa/Kigali - GMT+2"
    },
    {
        "key": 29,
        "value": "Africa/Kinshasa",
        "label": "Africa/Kinshasa - GMT+1"
    },
    {
        "key": 30,
        "value": "Africa/Lagos",
        "label": "Africa/Lagos - GMT+1"
    },
    {
        "key": 31,
        "value": "Africa/Libreville",
        "label": "Africa/Libreville - GMT+1"
    },
    {
        "key": 32,
        "value": "Africa/Lome",
        "label": "Africa/Lome - GMT0"
    },
    {
        "key": 33,
        "value": "Africa/Luanda",
        "label": "Africa/Luanda - GMT+1"
    },
    {
        "key": 34,
        "value": "Africa/Lubumbashi",
        "label": "Africa/Lubumbashi - GMT+2"
    },
    {
        "key": 35,
        "value": "Africa/Lusaka",
        "label": "Africa/Lusaka - GMT+2"
    },
    {
        "key": 36,
        "value": "Africa/Malabo",
        "label": "Africa/Malabo - GMT+1"
    },
    {
        "key": 37,
        "value": "Africa/Maputo",
        "label": "Africa/Maputo - GMT+2"
    },
    {
        "key": 38,
        "value": "Africa/Maseru",
        "label": "Africa/Maseru - GMT+2"
    },
    {
        "key": 39,
        "value": "Africa/Mbabane",
        "label": "Africa/Mbabane - GMT+2"
    },
    {
        "key": 40,
        "value": "Africa/Mogadishu",
        "label": "Africa/Mogadishu - GMT+3"
    },
    {
        "key": 41,
        "value": "Africa/Monrovia",
        "label": "Africa/Monrovia - GMT0"
    },
    {
        "key": 42,
        "value": "Africa/Nairobi",
        "label": "Africa/Nairobi - GMT+3"
    },
    {
        "key": 43,
        "value": "Africa/Ndjamena",
        "label": "Africa/Ndjamena - GMT+1"
    },
    {
        "key": 44,
        "value": "Africa/Niamey",
        "label": "Africa/Niamey - GMT+1"
    },
    {
        "key": 45,
        "value": "Africa/Nouakchott",
        "label": "Africa/Nouakchott - GMT0"
    },
    {
        "key": 46,
        "value": "Africa/Ouagadougou",
        "label": "Africa/Ouagadougou - GMT0"
    },
    {
        "key": 47,
        "value": "Africa/Porto-Novo",
        "label": "Africa/Porto-Novo - GMT+1"
    },
    {
        "key": 48,
        "value": "Africa/Sao_Tome",
        "label": "Africa/Sao Tome - GMT0"
    },
    {
        "key": 49,
        "value": "Africa/Tripoli",
        "label": "Africa/Tripoli - GMT+2"
    },
    {
        "key": 50,
        "value": "Africa/Tunis",
        "label": "Africa/Tunis - GMT+1"
    },
    {
        "key": 51,
        "value": "Africa/Windhoek",
        "label": "Africa/Windhoek - GMT+1"
    },
    {
        "key": 52,
        "value": "America/Adak",
        "label": "America/Adak - GMT-9"
    },
    {
        "key": 53,
        "value": "America/Anchorage",
        "label": "America/Anchorage - GMT-8"
    },
    {
        "key": 54,
        "value": "America/Anguilla",
        "label": "America/Anguilla - GMT-4"
    },
    {
        "key": 55,
        "value": "America/Antigua",
        "label": "America/Antigua - GMT-4"
    },
    {
        "key": 56,
        "value": "America/Araguaina",
        "label": "America/Araguaina - GMT-3"
    },
    {
        "key": 57,
        "value": "America/Argentina/Buenos_Aires",
        "label": "America/Argentina/Buenos Aires - GMT-3"
    },
    {
        "key": 58,
        "value": "America/Argentina/Catamarca",
        "label": "America/Argentina/Catamarca - GMT-3"
    },
    {
        "key": 59,
        "value": "America/Argentina/Cordoba",
        "label": "America/Argentina/Cordoba - GMT-3"
    },
    {
        "key": 60,
        "value": "America/Argentina/Jujuy",
        "label": "America/Argentina/Jujuy - GMT-3"
    },
    {
        "key": 61,
        "value": "America/Argentina/La_Rioja",
        "label": "America/Argentina/La Rioja - GMT-3"
    },
    {
        "key": 62,
        "value": "America/Argentina/Mendoza",
        "label": "America/Argentina/Mendoza - GMT-3"
    },
    {
        "key": 63,
        "value": "America/Argentina/Rio_Gallegos",
        "label": "America/Argentina/Rio Gallegos - GMT-3"
    },
    {
        "key": 64,
        "value": "America/Argentina/Salta",
        "label": "America/Argentina/Salta - GMT-3"
    },
    {
        "key": 65,
        "value": "America/Argentina/San_Juan",
        "label": "America/Argentina/San Juan - GMT-3"
    },
    {
        "key": 66,
        "value": "America/Argentina/San_Luis",
        "label": "America/Argentina/San Luis - GMT-3"
    },
    {
        "key": 67,
        "value": "America/Argentina/Tucuman",
        "label": "America/Argentina/Tucuman - GMT-3"
    },
    {
        "key": 68,
        "value": "America/Argentina/Ushuaia",
        "label": "America/Argentina/Ushuaia - GMT-3"
    },
    {
        "key": 69,
        "value": "America/Aruba",
        "label": "America/Aruba - GMT-4"
    },
    {
        "key": 70,
        "value": "America/Asuncion",
        "label": "America/Asuncion - GMT-4"
    },
    {
        "key": 71,
        "value": "America/Atikokan",
        "label": "America/Atikokan - GMT-5"
    },
    {
        "key": 72,
        "value": "America/Bahia",
        "label": "America/Bahia - GMT-3"
    },
    {
        "key": 73,
        "value": "America/Bahia_Banderas",
        "label": "America/Bahia Banderas - GMT-5"
    },
    {
        "key": 74,
        "value": "America/Barbados",
        "label": "America/Barbados - GMT-4"
    },
    {
        "key": 75,
        "value": "America/Belem",
        "label": "America/Belem - GMT-3"
    },
    {
        "key": 76,
        "value": "America/Belize",
        "label": "America/Belize - GMT-6"
    },
    {
        "key": 77,
        "value": "America/Blanc-Sablon",
        "label": "America/Blanc-Sablon - GMT-4"
    },
    {
        "key": 78,
        "value": "America/Boa_Vista",
        "label": "America/Boa Vista - GMT-4"
    },
    {
        "key": 79,
        "value": "America/Bogota",
        "label": "America/Bogota - GMT-5"
    },
    {
        "key": 80,
        "value": "America/Boise",
        "label": "America/Boise - GMT-6"
    },
    {
        "key": 81,
        "value": "America/Cambridge_Bay",
        "label": "America/Cambridge Bay - GMT-6"
    },
    {
        "key": 82,
        "value": "America/Campo_Grande",
        "label": "America/Campo Grande - GMT-4"
    },
    {
        "key": 83,
        "value": "America/Cancun",
        "label": "America/Cancun - GMT-5"
    },
    {
        "key": 84,
        "value": "America/Caracas",
        "label": "America/Caracas - GMT-4"
    },
    {
        "key": 85,
        "value": "America/Cayenne",
        "label": "America/Cayenne - GMT-3"
    },
    {
        "key": 86,
        "value": "America/Cayman",
        "label": "America/Cayman - GMT-5"
    },
    {
        "key": 87,
        "value": "America/Chicago",
        "label": "America/Chicago - GMT-5"
    },
    {
        "key": 88,
        "value": "America/Chihuahua",
        "label": "America/Chihuahua - GMT-6"
    },
    {
        "key": 89,
        "value": "America/Costa_Rica",
        "label": "America/Costa Rica - GMT-6"
    },
    {
        "key": 90,
        "value": "America/Creston",
        "label": "America/Creston - GMT-7"
    },
    {
        "key": 91,
        "value": "America/Cuiaba",
        "label": "America/Cuiaba - GMT-4"
    },
    {
        "key": 92,
        "value": "America/Curacao",
        "label": "America/Curacao - GMT-4"
    },
    {
        "key": 93,
        "value": "America/Danmarkshavn",
        "label": "America/Danmarkshavn - GMT0"
    },
    {
        "key": 94,
        "value": "America/Dawson",
        "label": "America/Dawson - GMT-7"
    },
    {
        "key": 95,
        "value": "America/Dawson_Creek",
        "label": "America/Dawson Creek - GMT-7"
    },
    {
        "key": 96,
        "value": "America/Denver",
        "label": "America/Denver - GMT-6"
    },
    {
        "key": 97,
        "value": "America/Detroit",
        "label": "America/Detroit - GMT-4"
    },
    {
        "key": 98,
        "value": "America/Dominica",
        "label": "America/Dominica - GMT-4"
    },
    {
        "key": 99,
        "value": "America/Edmonton",
        "label": "America/Edmonton - GMT-6"
    },
    {
        "key": 100,
        "value": "America/Eirunepe",
        "label": "America/Eirunepe - GMT-5"
    },
    {
        "key": 101,
        "value": "America/El_Salvador",
        "label": "America/El Salvador - GMT-6"
    },
    {
        "key": 102,
        "value": "America/Fort_Nelson",
        "label": "America/Fort Nelson - GMT-7"
    },
    {
        "key": 103,
        "value": "America/Fortaleza",
        "label": "America/Fortaleza - GMT-3"
    },
    {
        "key": 104,
        "value": "America/Glace_Bay",
        "label": "America/Glace Bay - GMT-3"
    },
    {
        "key": 105,
        "value": "America/Godthab",
        "label": "America/Godthab - GMT-2"
    },
    {
        "key": 106,
        "value": "America/Goose_Bay",
        "label": "America/Goose Bay - GMT-3"
    },
    {
        "key": 107,
        "value": "America/Grand_Turk",
        "label": "America/Grand Turk - GMT-4"
    },
    {
        "key": 108,
        "value": "America/Grenada",
        "label": "America/Grenada - GMT-4"
    },
    {
        "key": 109,
        "value": "America/Guadeloupe",
        "label": "America/Guadeloupe - GMT-4"
    },
    {
        "key": 110,
        "value": "America/Guatemala",
        "label": "America/Guatemala - GMT-6"
    },
    {
        "key": 111,
        "value": "America/Guayaquil",
        "label": "America/Guayaquil - GMT-5"
    },
    {
        "key": 112,
        "value": "America/Guyana",
        "label": "America/Guyana - GMT-4"
    },
    {
        "key": 113,
        "value": "America/Halifax",
        "label": "America/Halifax - GMT-3"
    },
    {
        "key": 114,
        "value": "America/Havana",
        "label": "America/Havana - GMT-4"
    },
    {
        "key": 115,
        "value": "America/Hermosillo",
        "label": "America/Hermosillo - GMT-7"
    },
    {
        "key": 116,
        "value": "America/Indiana/Indianapolis",
        "label": "America/Indiana/Indianapolis - GMT-4"
    },
    {
        "key": 117,
        "value": "America/Indiana/Knox",
        "label": "America/Indiana/Knox - GMT-5"
    },
    {
        "key": 118,
        "value": "America/Indiana/Marengo",
        "label": "America/Indiana/Marengo - GMT-4"
    },
    {
        "key": 119,
        "value": "America/Indiana/Petersburg",
        "label": "America/Indiana/Petersburg - GMT-4"
    },
    {
        "key": 120,
        "value": "America/Indiana/Tell_City",
        "label": "America/Indiana/Tell City - GMT-5"
    },
    {
        "key": 121,
        "value": "America/Indiana/Vevay",
        "label": "America/Indiana/Vevay - GMT-4"
    },
    {
        "key": 122,
        "value": "America/Indiana/Vincennes",
        "label": "America/Indiana/Vincennes - GMT-4"
    },
    {
        "key": 123,
        "value": "America/Indiana/Winamac",
        "label": "America/Indiana/Winamac - GMT-4"
    },
    {
        "key": 124,
        "value": "America/Inuvik",
        "label": "America/Inuvik - GMT-6"
    },
    {
        "key": 125,
        "value": "America/Iqaluit",
        "label": "America/Iqaluit - GMT-4"
    },
    {
        "key": 126,
        "value": "America/Jamaica",
        "label": "America/Jamaica - GMT-5"
    },
    {
        "key": 127,
        "value": "America/Juneau",
        "label": "America/Juneau - GMT-8"
    },
    {
        "key": 128,
        "value": "America/Kentucky/Louisville",
        "label": "America/Kentucky/Louisville - GMT-4"
    },
    {
        "key": 129,
        "value": "America/Kentucky/Monticello",
        "label": "America/Kentucky/Monticello - GMT-4"
    },
    {
        "key": 130,
        "value": "America/Kralendijk",
        "label": "America/Kralendijk - GMT-4"
    },
    {
        "key": 131,
        "value": "America/La_Paz",
        "label": "America/La Paz - GMT-4"
    },
    {
        "key": 132,
        "value": "America/Lima",
        "label": "America/Lima - GMT-5"
    },
    {
        "key": 133,
        "value": "America/Los_Angeles",
        "label": "America/Los Angeles - GMT-7"
    },
    {
        "key": 134,
        "value": "America/Lower_Princes",
        "label": "America/Lower Princes - GMT-4"
    },
    {
        "key": 135,
        "value": "America/Maceio",
        "label": "America/Maceio - GMT-3"
    },
    {
        "key": 136,
        "value": "America/Managua",
        "label": "America/Managua - GMT-6"
    },
    {
        "key": 137,
        "value": "America/Manaus",
        "label": "America/Manaus - GMT-4"
    },
    {
        "key": 138,
        "value": "America/Marigot",
        "label": "America/Marigot - GMT-4"
    },
    {
        "key": 139,
        "value": "America/Martinique",
        "label": "America/Martinique - GMT-4"
    },
    {
        "key": 140,
        "value": "America/Matamoros",
        "label": "America/Matamoros - GMT-5"
    },
    {
        "key": 141,
        "value": "America/Mazatlan",
        "label": "America/Mazatlan - GMT-6"
    },
    {
        "key": 142,
        "value": "America/Menominee",
        "label": "America/Menominee - GMT-5"
    },
    {
        "key": 143,
        "value": "America/Merida",
        "label": "America/Merida - GMT-5"
    },
    {
        "key": 144,
        "value": "America/Metlakatla",
        "label": "America/Metlakatla - GMT-8"
    },
    {
        "key": 145,
        "value": "America/Mexico_City",
        "label": "America/Mexico City - GMT-5"
    },
    {
        "key": 146,
        "value": "America/Miquelon",
        "label": "America/Miquelon - GMT-2"
    },
    {
        "key": 147,
        "value": "America/Moncton",
        "label": "America/Moncton - GMT-3"
    },
    {
        "key": 148,
        "value": "America/Monterrey",
        "label": "America/Monterrey - GMT-5"
    },
    {
        "key": 149,
        "value": "America/Montevideo",
        "label": "America/Montevideo - GMT-3"
    },
    {
        "key": 150,
        "value": "America/Montserrat",
        "label": "America/Montserrat - GMT-4"
    },
    {
        "key": 151,
        "value": "America/Nassau",
        "label": "America/Nassau - GMT-4"
    },
    {
        "key": 152,
        "value": "America/New_York",
        "label": "America/New York - GMT-4"
    },
    {
        "key": 153,
        "value": "America/Nipigon",
        "label": "America/Nipigon - GMT-4"
    },
    {
        "key": 154,
        "value": "America/Nome",
        "label": "America/Nome - GMT-8"
    },
    {
        "key": 155,
        "value": "America/Noronha",
        "label": "America/Noronha - GMT-2"
    },
    {
        "key": 156,
        "value": "America/North_Dakota/Beulah",
        "label": "America/North Dakota/Beulah - GMT-5"
    },
    {
        "key": 157,
        "value": "America/North_Dakota/Center",
        "label": "America/North Dakota/Center - GMT-5"
    },
    {
        "key": 158,
        "value": "America/North_Dakota/New_Salem",
        "label": "America/North Dakota/New Salem - GMT-5"
    },
    {
        "key": 159,
        "value": "America/Ojinaga",
        "label": "America/Ojinaga - GMT-6"
    },
    {
        "key": 160,
        "value": "America/Panama",
        "label": "America/Panama - GMT-5"
    },
    {
        "key": 161,
        "value": "America/Pangnirtung",
        "label": "America/Pangnirtung - GMT-4"
    },
    {
        "key": 162,
        "value": "America/Paramaribo",
        "label": "America/Paramaribo - GMT-3"
    },
    {
        "key": 163,
        "value": "America/Phoenix",
        "label": "America/Phoenix - GMT-7"
    },
    {
        "key": 164,
        "value": "America/Port-au-Prince",
        "label": "America/Port-au-Prince - GMT-5"
    },
    {
        "key": 165,
        "value": "America/Port_of_Spain",
        "label": "America/Port of Spain - GMT-4"
    },
    {
        "key": 166,
        "value": "America/Porto_Velho",
        "label": "America/Porto Velho - GMT-4"
    },
    {
        "key": 167,
        "value": "America/Puerto_Rico",
        "label": "America/Puerto Rico - GMT-4"
    },
    {
        "key": 168,
        "value": "America/Rainy_River",
        "label": "America/Rainy River - GMT-5"
    },
    {
        "key": 169,
        "value": "America/Rankin_Inlet",
        "label": "America/Rankin Inlet - GMT-5"
    },
    {
        "key": 170,
        "value": "America/Recife",
        "label": "America/Recife - GMT-3"
    },
    {
        "key": 171,
        "value": "America/Regina",
        "label": "America/Regina - GMT-6"
    },
    {
        "key": 172,
        "value": "America/Resolute",
        "label": "America/Resolute - GMT-5"
    },
    {
        "key": 173,
        "value": "America/Rio_Branco",
        "label": "America/Rio Branco - GMT-5"
    },
    {
        "key": 174,
        "value": "America/Santarem",
        "label": "America/Santarem - GMT-3"
    },
    {
        "key": 175,
        "value": "America/Santiago",
        "label": "America/Santiago - GMT-4"
    },
    {
        "key": 176,
        "value": "America/Santo_Domingo",
        "label": "America/Santo Domingo - GMT-4"
    },
    {
        "key": 177,
        "value": "America/Sao_Paulo",
        "label": "America/Sao Paulo - GMT-3"
    },
    {
        "key": 178,
        "value": "America/Scoresbysund",
        "label": "America/Scoresbysund - GMT0"
    },
    {
        "key": 179,
        "value": "America/Sitka",
        "label": "America/Sitka - GMT-8"
    },
    {
        "key": 180,
        "value": "America/St_Barthelemy",
        "label": "America/St Barthelemy - GMT-4"
    },
    {
        "key": 181,
        "value": "America/St_Johns",
        "label": "America/St Johns - GMT-2.5"
    },
    {
        "key": 182,
        "value": "America/St_Kitts",
        "label": "America/St Kitts - GMT-4"
    },
    {
        "key": 183,
        "value": "America/St_Lucia",
        "label": "America/St Lucia - GMT-4"
    },
    {
        "key": 184,
        "value": "America/St_Thomas",
        "label": "America/St Thomas - GMT-4"
    },
    {
        "key": 185,
        "value": "America/St_Vincent",
        "label": "America/St Vincent - GMT-4"
    },
    {
        "key": 186,
        "value": "America/Swift_Current",
        "label": "America/Swift Current - GMT-6"
    },
    {
        "key": 187,
        "value": "America/Tegucigalpa",
        "label": "America/Tegucigalpa - GMT-6"
    },
    {
        "key": 188,
        "value": "America/Thule",
        "label": "America/Thule - GMT-3"
    },
    {
        "key": 189,
        "value": "America/Thunder_Bay",
        "label": "America/Thunder Bay - GMT-4"
    },
    {
        "key": 190,
        "value": "America/Tijuana",
        "label": "America/Tijuana - GMT-7"
    },
    {
        "key": 191,
        "value": "America/Toronto",
        "label": "America/Toronto - GMT-4"
    },
    {
        "key": 192,
        "value": "America/Tortola",
        "label": "America/Tortola - GMT-4"
    },
    {
        "key": 193,
        "value": "America/Vancouver",
        "label": "America/Vancouver - GMT-7"
    },
    {
        "key": 194,
        "value": "America/Whitehorse",
        "label": "America/Whitehorse - GMT-7"
    },
    {
        "key": 195,
        "value": "America/Winnipeg",
        "label": "America/Winnipeg - GMT-5"
    },
    {
        "key": 196,
        "value": "America/Yakutat",
        "label": "America/Yakutat - GMT-8"
    },
    {
        "key": 197,
        "value": "America/Yellowknife",
        "label": "America/Yellowknife - GMT-6"
    },
    {
        "key": 198,
        "value": "Antarctica/Casey",
        "label": "Antarctica/Casey - GMT+11"
    },
    {
        "key": 199,
        "value": "Antarctica/Davis",
        "label": "Antarctica/Davis - GMT+7"
    },
    {
        "key": 200,
        "value": "Antarctica/DumontDUrville",
        "label": "Antarctica/DumontDUrville - GMT+10"
    },
    {
        "key": 201,
        "value": "Antarctica/Macquarie",
        "label": "Antarctica/Macquarie - GMT+11"
    },
    {
        "key": 202,
        "value": "Antarctica/Mawson",
        "label": "Antarctica/Mawson - GMT+5"
    },
    {
        "key": 203,
        "value": "Antarctica/McMurdo",
        "label": "Antarctica/McMurdo - GMT+12"
    },
    {
        "key": 204,
        "value": "Antarctica/Palmer",
        "label": "Antarctica/Palmer - GMT-4"
    },
    {
        "key": 205,
        "value": "Antarctica/Rothera",
        "label": "Antarctica/Rothera - GMT-3"
    },
    {
        "key": 206,
        "value": "Antarctica/Syowa",
        "label": "Antarctica/Syowa - GMT+3"
    },
    {
        "key": 207,
        "value": "Antarctica/Troll",
        "label": "Antarctica/Troll - GMT+2"
    },
    {
        "key": 208,
        "value": "Antarctica/Vostok",
        "label": "Antarctica/Vostok - GMT+6"
    },
    {
        "key": 209,
        "value": "Arctic/Longyearbyen",
        "label": "Arctic/Longyearbyen - GMT+2"
    },
    {
        "key": 210,
        "value": "Asia/Aden",
        "label": "Asia/Aden - GMT+3"
    },
    {
        "key": 211,
        "value": "Asia/Almaty",
        "label": "Asia/Almaty - GMT+6"
    },
    {
        "key": 212,
        "value": "Asia/Amman",
        "label": "Asia/Amman - GMT+3"
    },
    {
        "key": 213,
        "value": "Asia/Anadyr",
        "label": "Asia/Anadyr - GMT+12"
    },
    {
        "key": 214,
        "value": "Asia/Aqtau",
        "label": "Asia/Aqtau - GMT+5"
    },
    {
        "key": 215,
        "value": "Asia/Aqtobe",
        "label": "Asia/Aqtobe - GMT+5"
    },
    {
        "key": 216,
        "value": "Asia/Ashgabat",
        "label": "Asia/Ashgabat - GMT+5"
    },
    {
        "key": 217,
        "value": "Asia/Atyrau",
        "label": "Asia/Atyrau - GMT+5"
    },
    {
        "key": 218,
        "value": "Asia/Baghdad",
        "label": "Asia/Baghdad - GMT+3"
    },
    {
        "key": 219,
        "value": "Asia/Bahrain",
        "label": "Asia/Bahrain - GMT+3"
    },
    {
        "key": 220,
        "value": "Asia/Baku",
        "label": "Asia/Baku - GMT+4"
    },
    {
        "key": 221,
        "value": "Asia/Bangkok",
        "label": "Asia/Bangkok - GMT+7"
    },
    {
        "key": 222,
        "value": "Asia/Barnaul",
        "label": "Asia/Barnaul - GMT+7"
    },
    {
        "key": 223,
        "value": "Asia/Beirut",
        "label": "Asia/Beirut - GMT+3"
    },
    {
        "key": 224,
        "value": "Asia/Bishkek",
        "label": "Asia/Bishkek - GMT+6"
    },
    {
        "key": 225,
        "value": "Asia/Brunei",
        "label": "Asia/Brunei - GMT+8"
    },
    {
        "key": 226,
        "value": "Asia/Chita",
        "label": "Asia/Chita - GMT+9"
    },
    {
        "key": 227,
        "value": "Asia/Choibalsan",
        "label": "Asia/Choibalsan - GMT+9"
    },
    {
        "key": 228,
        "value": "Asia/Colombo",
        "label": "Asia/Colombo - GMT+5.5"
    },
    {
        "key": 229,
        "value": "Asia/Damascus",
        "label": "Asia/Damascus - GMT+3"
    },
    {
        "key": 230,
        "value": "Asia/Dhaka",
        "label": "Asia/Dhaka - GMT+6"
    },
    {
        "key": 231,
        "value": "Asia/Dili",
        "label": "Asia/Dili - GMT+9"
    },
    {
        "key": 232,
        "value": "Asia/Dubai",
        "label": "Asia/Dubai - GMT+4"
    },
    {
        "key": 233,
        "value": "Asia/Dushanbe",
        "label": "Asia/Dushanbe - GMT+5"
    },
    {
        "key": 234,
        "value": "Asia/Famagusta",
        "label": "Asia/Famagusta - GMT+3"
    },
    {
        "key": 235,
        "value": "Asia/Gaza",
        "label": "Asia/Gaza - GMT+3"
    },
    {
        "key": 236,
        "value": "Asia/Hebron",
        "label": "Asia/Hebron - GMT+3"
    },
    {
        "key": 238,
        "value": "Asia/Hong_Kong",
        "label": "Asia/Hong Kong - GMT+8"
    },
    {
        "key": 239,
        "value": "Asia/Hovd",
        "label": "Asia/Hovd - GMT+8"
    },
    {
        "key": 240,
        "value": "Asia/Irkutsk",
        "label": "Asia/Irkutsk - GMT+8"
    },
    {
        "key": 241,
        "value": "Asia/Jakarta",
        "label": "Asia/Jakarta - GMT+7"
    },
    {
        "key": 242,
        "value": "Asia/Jayapura",
        "label": "Asia/Jayapura - GMT+9"
    },
    {
        "key": 243,
        "value": "Asia/Jerusalem",
        "label": "Asia/Jerusalem - GMT+3"
    },
    {
        "key": 244,
        "value": "Asia/Kabul",
        "label": "Asia/Kabul - GMT+4.5"
    },
    {
        "key": 245,
        "value": "Asia/Kamchatka",
        "label": "Asia/Kamchatka - GMT+12"
    },
    {
        "key": 246,
        "value": "Asia/Karachi",
        "label": "Asia/Karachi - GMT+5"
    },
    {
        "key": 247,
        "value": "Asia/Kathmandu",
        "label": "Asia/Kathmandu - GMT+5.75"
    },
    {
        "key": 248,
        "value": "Asia/Khandyga",
        "label": "Asia/Khandyga - GMT+9"
    },
    {
        "key": 249,
        "value": "Asia/Kolkata",
        "label": "Asia/Kolkata - GMT+5.5"
    },
    {
        "key": 250,
        "value": "Asia/Krasnoyarsk",
        "label": "Asia/Krasnoyarsk - GMT+7"
    },
    {
        "key": 251,
        "value": "Asia/Kuala_Lumpur",
        "label": "Asia/Kuala Lumpur - GMT+8"
    },
    {
        "key": 252,
        "value": "Asia/Kuching",
        "label": "Asia/Kuching - GMT+8"
    },
    {
        "key": 253,
        "value": "Asia/Kuwait",
        "label": "Asia/Kuwait - GMT+3"
    },
    {
        "key": 254,
        "value": "Asia/Macau",
        "label": "Asia/Macau - GMT+8"
    },
    {
        "key": 255,
        "value": "Asia/Magadan",
        "label": "Asia/Magadan - GMT+11"
    },
    {
        "key": 256,
        "value": "Asia/Makassar",
        "label": "Asia/Makassar - GMT+8"
    },
    {
        "key": 257,
        "value": "Asia/Manila",
        "label": "Asia/Manila - GMT+8"
    },
    {
        "key": 258,
        "value": "Asia/Muscat",
        "label": "Asia/Muscat - GMT+4"
    },
    {
        "key": 259,
        "value": "Asia/Nicosia",
        "label": "Asia/Nicosia - GMT+3"
    },
    {
        "key": 260,
        "value": "Asia/Novokuznetsk",
        "label": "Asia/Novokuznetsk - GMT+7"
    },
    {
        "key": 261,
        "value": "Asia/Novosibirsk",
        "label": "Asia/Novosibirsk - GMT+7"
    },
    {
        "key": 262,
        "value": "Asia/Omsk",
        "label": "Asia/Omsk - GMT+6"
    },
    {
        "key": 263,
        "value": "Asia/Oral",
        "label": "Asia/Oral - GMT+5"
    },
    {
        "key": 264,
        "value": "Asia/Phnom_Penh",
        "label": "Asia/Phnom Penh - GMT+7"
    },
    {
        "key": 265,
        "value": "Asia/Pontianak",
        "label": "Asia/Pontianak - GMT+7"
    },
    {
        "key": 266,
        "value": "Asia/Pyongyang",
        "label": "Asia/Pyongyang - GMT+8.5"
    },
    {
        "key": 267,
        "value": "Asia/Qatar",
        "label": "Asia/Qatar - GMT+3"
    },
    {
        "key": 268,
        "value": "Asia/Qyzylorda",
        "label": "Asia/Qyzylorda - GMT+6"
    },
    {
        "key": 269,
        "value": "Asia/Riyadh",
        "label": "Asia/Riyadh - GMT+3"
    },
    {
        "key": 270,
        "value": "Asia/Sakhalin",
        "label": "Asia/Sakhalin - GMT+11"
    },
    {
        "key": 271,
        "value": "Asia/Samarkand",
        "label": "Asia/Samarkand - GMT+5"
    },
    {
        "key": 272,
        "value": "Asia/Seoul",
        "label": "Asia/Seoul - GMT+9"
    },
    {
        "key": 273,
        "value": "Asia/Shanghai",
        "label": "Asia/Shanghai - GMT+8"
    },
    {
        "key": 275,
        "value": "Asia/Srednekolymsk",
        "label": "Asia/Srednekolymsk - GMT+11"
    },
    {
        "key": 276,
        "value": "Asia/Taipei",
        "label": "Asia/Taipei - GMT+8"
    },
    {
        "key": 277,
        "value": "Asia/Tashkent",
        "label": "Asia/Tashkent - GMT+5"
    },
    {
        "key": 278,
        "value": "Asia/Tbilisi",
        "label": "Asia/Tbilisi - GMT+4"
    },
    {
        "key": 279,
        "value": "Asia/Tehran",
        "label": "Asia/Tehran - GMT+4.5"
    },
    {
        "key": 280,
        "value": "Asia/Thimphu",
        "label": "Asia/Thimphu - GMT+6"
    },
    {
        "key": 281,
        "value": "Asia/Tokyo",
        "label": "Asia/Tokyo - GMT+9"
    },
    {
        "key": 282,
        "value": "Asia/Tomsk",
        "label": "Asia/Tomsk - GMT+7"
    },
    {
        "key": 283,
        "value": "Asia/Ulaanbaatar",
        "label": "Asia/Ulaanbaatar - GMT+9"
    },
    {
        "key": 284,
        "value": "Asia/Urumqi",
        "label": "Asia/Urumqi - GMT+6"
    },
    {
        "key": 285,
        "value": "Asia/Ust-Nera",
        "label": "Asia/Ust-Nera - GMT+10"
    },
    {
        "key": 286,
        "value": "Asia/Vientiane",
        "label": "Asia/Vientiane - GMT+7"
    },
    {
        "key": 287,
        "value": "Asia/Vladivostok",
        "label": "Asia/Vladivostok - GMT+10"
    },
    {
        "key": 288,
        "value": "Asia/Yakutsk",
        "label": "Asia/Yakutsk - GMT+9"
    },
    {
        "key": 289,
        "value": "Asia/Yangon",
        "label": "Asia/Yangon - GMT+6.5"
    },
    {
        "key": 290,
        "value": "Asia/Yekaterinburg",
        "label": "Asia/Yekaterinburg - GMT+5"
    },
    {
        "key": 291,
        "value": "Asia/Yerevan",
        "label": "Asia/Yerevan - GMT+4"
    },
    {
        "key": 292,
        "value": "Atlantic/Azores",
        "label": "Atlantic/Azores - GMT0"
    },
    {
        "key": 293,
        "value": "Atlantic/Bermuda",
        "label": "Atlantic/Bermuda - GMT-3"
    },
    {
        "key": 294,
        "value": "Atlantic/Canary",
        "label": "Atlantic/Canary - GMT+1"
    },
    {
        "key": 295,
        "value": "Atlantic/Cape_Verde",
        "label": "Atlantic/Cape Verde - GMT-1"
    },
    {
        "key": 296,
        "value": "Atlantic/Faroe",
        "label": "Atlantic/Faroe - GMT+1"
    },
    {
        "key": 297,
        "value": "Atlantic/Madeira",
        "label": "Atlantic/Madeira - GMT+1"
    },
    {
        "key": 298,
        "value": "Atlantic/Reykjavik",
        "label": "Atlantic/Reykjavik - GMT0"
    },
    {
        "key": 299,
        "value": "Atlantic/South_Georgia",
        "label": "Atlantic/South Georgia - GMT-2"
    },
    {
        "key": 300,
        "value": "Atlantic/St_Helena",
        "label": "Atlantic/St Helena - GMT0"
    },
    {
        "key": 301,
        "value": "Atlantic/Stanley",
        "label": "Atlantic/Stanley - GMT-3"
    },
    {
        "key": 302,
        "value": "Australia/Adelaide",
        "label": "Australia/Adelaide - GMT+9.5"
    },
    {
        "key": 303,
        "value": "Australia/Brisbane",
        "label": "Australia/Brisbane - GMT+10"
    },
    {
        "key": 304,
        "value": "Australia/Broken_Hill",
        "label": "Australia/Broken Hill - GMT+9.5"
    },
    {
        "key": 305,
        "value": "Australia/Currie",
        "label": "Australia/Currie - GMT+10"
    },
    {
        "key": 306,
        "value": "Australia/Darwin",
        "label": "Australia/Darwin - GMT+9.5"
    },
    {
        "key": 307,
        "value": "Australia/Eucla",
        "label": "Australia/Eucla - GMT+8.75"
    },
    {
        "key": 308,
        "value": "Australia/Hobart",
        "label": "Australia/Hobart - GMT+10"
    },
    {
        "key": 309,
        "value": "Australia/Lindeman",
        "label": "Australia/Lindeman - GMT+10"
    },
    {
        "key": 310,
        "value": "Australia/Lord_Howe",
        "label": "Australia/Lord Howe - GMT+10.5"
    },
    {
        "key": 311,
        "value": "Australia/Melbourne",
        "label": "Australia/Melbourne - GMT+10"
    },
    {
        "key": 312,
        "value": "Australia/Perth",
        "label": "Australia/Perth - GMT+8"
    },
    {
        "key": 313,
        "value": "Australia/Sydney",
        "label": "Australia/Sydney - GMT+10"
    },
    {
        "key": 314,
        "value": "Europe/Amsterdam",
        "label": "Europe/Amsterdam - GMT+2"
    },
    {
        "key": 315,
        "value": "Europe/Andorra",
        "label": "Europe/Andorra - GMT+2"
    },
    {
        "key": 316,
        "value": "Europe/Astrakhan",
        "label": "Europe/Astrakhan - GMT+4"
    },
    {
        "key": 317,
        "value": "Europe/Athens",
        "label": "Europe/Athens - GMT+3"
    },
    {
        "key": 318,
        "value": "Europe/Belgrade",
        "label": "Europe/Belgrade - GMT+2"
    },
    {
        "key": 319,
        "value": "Europe/Berlin",
        "label": "Europe/Berlin - GMT+2"
    },
    {
        "key": 320,
        "value": "Europe/Bratislava",
        "label": "Europe/Bratislava - GMT+2"
    },
    {
        "key": 321,
        "value": "Europe/Brussels",
        "label": "Europe/Brussels - GMT+2"
    },
    {
        "key": 322,
        "value": "Europe/Bucharest",
        "label": "Europe/Bucharest - GMT+3"
    },
    {
        "key": 323,
        "value": "Europe/Budapest",
        "label": "Europe/Budapest - GMT+2"
    },
    {
        "key": 324,
        "value": "Europe/Busingen",
        "label": "Europe/Busingen - GMT+2"
    },
    {
        "key": 325,
        "value": "Europe/Chisinau",
        "label": "Europe/Chisinau - GMT+3"
    },
    {
        "key": 326,
        "value": "Europe/Copenhagen",
        "label": "Europe/Copenhagen - GMT+2"
    },
    {
        "key": 327,
        "value": "Europe/Dublin",
        "label": "Europe/Dublin - GMT+1"
    },
    {
        "key": 328,
        "value": "Europe/Gibraltar",
        "label": "Europe/Gibraltar - GMT+2"
    },
    {
        "key": 329,
        "value": "Europe/Guernsey",
        "label": "Europe/Guernsey - GMT+1"
    },
    {
        "key": 330,
        "value": "Europe/Helsinki",
        "label": "Europe/Helsinki - GMT+3"
    },
    {
        "key": 331,
        "value": "Europe/Isle_of_Man",
        "label": "Europe/Isle of Man - GMT+1"
    },
    {
        "key": 332,
        "value": "Europe/Istanbul",
        "label": "Europe/Istanbul - GMT+3"
    },
    {
        "key": 333,
        "value": "Europe/Jersey",
        "label": "Europe/Jersey - GMT+1"
    },
    {
        "key": 334,
        "value": "Europe/Kaliningrad",
        "label": "Europe/Kaliningrad - GMT+2"
    },
    {
        "key": 335,
        "value": "Europe/Kiev",
        "label": "Europe/Kiev - GMT+3"
    },
    {
        "key": 336,
        "value": "Europe/Kirov",
        "label": "Europe/Kirov - GMT+3"
    },
    {
        "key": 337,
        "value": "Europe/Lisbon",
        "label": "Europe/Lisbon - GMT+1"
    },
    {
        "key": 338,
        "value": "Europe/Ljubljana",
        "label": "Europe/Ljubljana - GMT+2"
    },
    {
        "key": 339,
        "value": "Europe/London",
        "label": "Europe/London - GMT+1"
    },
    {
        "key": 340,
        "value": "Europe/Luxembourg",
        "label": "Europe/Luxembourg - GMT+2"
    },
    {
        "key": 341,
        "value": "Europe/Madrid",
        "label": "Europe/Madrid - GMT+2"
    },
    {
        "key": 342,
        "value": "Europe/Malta",
        "label": "Europe/Malta - GMT+2"
    },
    {
        "key": 343,
        "value": "Europe/Mariehamn",
        "label": "Europe/Mariehamn - GMT+3"
    },
    {
        "key": 344,
        "value": "Europe/Minsk",
        "label": "Europe/Minsk - GMT+3"
    },
    {
        "key": 345,
        "value": "Europe/Monaco",
        "label": "Europe/Monaco - GMT+2"
    },
    {
        "key": 346,
        "value": "Europe/Moscow",
        "label": "Europe/Moscow - GMT+3"
    },
    {
        "key": 347,
        "value": "Europe/Oslo",
        "label": "Europe/Oslo - GMT+2"
    },
    {
        "key": 348,
        "value": "Europe/Paris",
        "label": "Europe/Paris - GMT+2"
    },
    {
        "key": 349,
        "value": "Europe/Podgorica",
        "label": "Europe/Podgorica - GMT+2"
    },
    {
        "key": 350,
        "value": "Europe/Prague",
        "label": "Europe/Prague - GMT+2"
    },
    {
        "key": 351,
        "value": "Europe/Riga",
        "label": "Europe/Riga - GMT+3"
    },
    {
        "key": 352,
        "value": "Europe/Rome",
        "label": "Europe/Rome - GMT+2"
    },
    {
        "key": 353,
        "value": "Europe/Samara",
        "label": "Europe/Samara - GMT+4"
    },
    {
        "key": 354,
        "value": "Europe/San_Marino",
        "label": "Europe/San Marino - GMT+2"
    },
    {
        "key": 355,
        "value": "Europe/Sarajevo",
        "label": "Europe/Sarajevo - GMT+2"
    },
    {
        "key": 356,
        "value": "Europe/Saratov",
        "label": "Europe/Saratov - GMT+4"
    },
    {
        "key": 357,
        "value": "Europe/Simferopol",
        "label": "Europe/Simferopol - GMT+3"
    },
    {
        "key": 358,
        "value": "Europe/Skopje",
        "label": "Europe/Skopje - GMT+2"
    },
    {
        "key": 359,
        "value": "Europe/Sofia",
        "label": "Europe/Sofia - GMT+3"
    },
    {
        "key": 360,
        "value": "Europe/Stockholm",
        "label": "Europe/Stockholm - GMT+2"
    },
    {
        "key": 361,
        "value": "Europe/Tallinn",
        "label": "Europe/Tallinn - GMT+3"
    },
    {
        "key": 362,
        "value": "Europe/Tirane",
        "label": "Europe/Tirane - GMT+2"
    },
    {
        "key": 363,
        "value": "Europe/Ulyanovsk",
        "label": "Europe/Ulyanovsk - GMT+4"
    },
    {
        "key": 364,
        "value": "Europe/Uzhgorod",
        "label": "Europe/Uzhgorod - GMT+3"
    },
    {
        "key": 365,
        "value": "Europe/Vaduz",
        "label": "Europe/Vaduz - GMT+2"
    },
    {
        "key": 366,
        "value": "Europe/Vatican",
        "label": "Europe/Vatican - GMT+2"
    },
    {
        "key": 367,
        "value": "Europe/Vienna",
        "label": "Europe/Vienna - GMT+2"
    },
    {
        "key": 368,
        "value": "Europe/Vilnius",
        "label": "Europe/Vilnius - GMT+3"
    },
    {
        "key": 369,
        "value": "Europe/Volgograd",
        "label": "Europe/Volgograd - GMT+3"
    },
    {
        "key": 370,
        "value": "Europe/Warsaw",
        "label": "Europe/Warsaw - GMT+2"
    },
    {
        "key": 371,
        "value": "Europe/Zagreb",
        "label": "Europe/Zagreb - GMT+2"
    },
    {
        "key": 372,
        "value": "Europe/Zaporozhye",
        "label": "Europe/Zaporozhye - GMT+3"
    },
    {
        "key": 373,
        "value": "Europe/Zurich",
        "label": "Europe/Zurich - GMT+2"
    },
    {
        "key": 374,
        "value": "Indian/Antananarivo",
        "label": "Indian/Antananarivo - GMT+3"
    },
    {
        "key": 375,
        "value": "Indian/Chagos",
        "label": "Indian/Chagos - GMT+6"
    },
    {
        "key": 376,
        "value": "Indian/Christmas",
        "label": "Indian/Christmas - GMT+7"
    },
    {
        "key": 377,
        "value": "Indian/Cocos",
        "label": "Indian/Cocos - GMT+6.5"
    },
    {
        "key": 378,
        "value": "Indian/Comoro",
        "label": "Indian/Comoro - GMT+3"
    },
    {
        "key": 379,
        "value": "Indian/Kerguelen",
        "label": "Indian/Kerguelen - GMT+5"
    },
    {
        "key": 380,
        "value": "Indian/Mahe",
        "label": "Indian/Mahe - GMT+4"
    },
    {
        "key": 381,
        "value": "Indian/Maldives",
        "label": "Indian/Maldives - GMT+5"
    },
    {
        "key": 382,
        "value": "Indian/Mauritius",
        "label": "Indian/Mauritius - GMT+4"
    },
    {
        "key": 383,
        "value": "Indian/Mayotte",
        "label": "Indian/Mayotte - GMT+3"
    },
    {
        "key": 384,
        "value": "Indian/Reunion",
        "label": "Indian/Reunion - GMT+4"
    },
    {
        "key": 385,
        "value": "Pacific/Apia",
        "label": "Pacific/Apia - GMT+13"
    },
    {
        "key": 386,
        "value": "Pacific/Auckland",
        "label": "Pacific/Auckland - GMT+12"
    },
    {
        "key": 387,
        "value": "Pacific/Bougainville",
        "label": "Pacific/Bougainville - GMT+11"
    },
    {
        "key": 388,
        "value": "Pacific/Chatham",
        "label": "Pacific/Chatham - GMT+12.75"
    },
    {
        "key": 389,
        "value": "Pacific/Chuuk",
        "label": "Pacific/Chuuk - GMT+10"
    },
    {
        "key": 390,
        "value": "Pacific/Easter",
        "label": "Pacific/Easter - GMT-6"
    },
    {
        "key": 391,
        "value": "Pacific/Efate",
        "label": "Pacific/Efate - GMT+11"
    },
    {
        "key": 392,
        "value": "Pacific/Enderbury",
        "label": "Pacific/Enderbury - GMT+13"
    },
    {
        "key": 393,
        "value": "Pacific/Fakaofo",
        "label": "Pacific/Fakaofo - GMT+13"
    },
    {
        "key": 394,
        "value": "Pacific/Fiji",
        "label": "Pacific/Fiji - GMT+12"
    },
    {
        "key": 395,
        "value": "Pacific/Funafuti",
        "label": "Pacific/Funafuti - GMT+12"
    },
    {
        "key": 396,
        "value": "Pacific/Galapagos",
        "label": "Pacific/Galapagos - GMT-6"
    },
    {
        "key": 397,
        "value": "Pacific/Gambier",
        "label": "Pacific/Gambier - GMT-9"
    },
    {
        "key": 398,
        "value": "Pacific/Guadalcanal",
        "label": "Pacific/Guadalcanal - GMT+11"
    },
    {
        "key": 399,
        "value": "Pacific/Guam",
        "label": "Pacific/Guam - GMT+10"
    },
    {
        "key": 400,
        "value": "Pacific/Honolulu",
        "label": "Pacific/Honolulu - GMT-10"
    },
    {
        "key": 401,
        "value": "Pacific/Johnston",
        "label": "Pacific/Johnston - GMT-10"
    },
    {
        "key": 402,
        "value": "Pacific/Kiritimati",
        "label": "Pacific/Kiritimati - GMT+14"
    },
    {
        "key": 403,
        "value": "Pacific/Kosrae",
        "label": "Pacific/Kosrae - GMT+11"
    },
    {
        "key": 404,
        "value": "Pacific/Kwajalein",
        "label": "Pacific/Kwajalein - GMT+12"
    },
    {
        "key": 405,
        "value": "Pacific/Majuro",
        "label": "Pacific/Majuro - GMT+12"
    },
    {
        "key": 406,
        "value": "Pacific/Marquesas",
        "label": "Pacific/Marquesas - GMT-9.5"
    },
    {
        "key": 407,
        "value": "Pacific/Midway",
        "label": "Pacific/Midway - GMT-11"
    },
    {
        "key": 408,
        "value": "Pacific/Nauru",
        "label": "Pacific/Nauru - GMT+12"
    },
    {
        "key": 409,
        "value": "Pacific/Niue",
        "label": "Pacific/Niue - GMT-11"
    },
    {
        "key": 410,
        "value": "Pacific/Norfolk",
        "label": "Pacific/Norfolk - GMT+11"
    },
    {
        "key": 411,
        "value": "Pacific/Noumea",
        "label": "Pacific/Noumea - GMT+11"
    },
    {
        "key": 412,
        "value": "Pacific/Pago_Pago",
        "label": "Pacific/Pago Pago - GMT-11"
    },
    {
        "key": 413,
        "value": "Pacific/Palau",
        "label": "Pacific/Palau - GMT+9"
    },
    {
        "key": 414,
        "value": "Pacific/Pitcairn",
        "label": "Pacific/Pitcairn - GMT-8"
    },
    {
        "key": 415,
        "value": "Pacific/Pohnpei",
        "label": "Pacific/Pohnpei - GMT+11"
    },
    {
        "key": 416,
        "value": "Pacific/Port_Moresby",
        "label": "Pacific/Port Moresby - GMT+10"
    },
    {
        "key": 417,
        "value": "Pacific/Rarotonga",
        "label": "Pacific/Rarotonga - GMT-10"
    },
    {
        "key": 418,
        "value": "Pacific/Saipan",
        "label": "Pacific/Saipan - GMT+10"
    },
    {
        "key": 419,
        "value": "Pacific/Tahiti",
        "label": "Pacific/Tahiti - GMT-10"
    },
    {
        "key": 420,
        "value": "Pacific/Tarawa",
        "label": "Pacific/Tarawa - GMT+12"
    },
    {
        "key": 421,
        "value": "Pacific/Tongatapu",
        "label": "Pacific/Tongatapu - GMT+13"
    },
    {
        "key": 422,
        "value": "Pacific/Wake",
        "label": "Pacific/Wake - GMT+12"
    },
    {
        "key": 423,
        "value": "Pacific/Wallis",
        "label": "Pacific/Wallis - GMT+12"
    },
    {
        "key": 424,
        "value": "UTC",
        "label": "UTC - GMT0"
    }
]
