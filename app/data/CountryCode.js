let index = 0;
export const dataCC = [
  {
    key: index++,
    "label": "Singapore (+65)",
    "dial_code": "+65",
    "code": "SG"
  },
  {
    key: index++,
    "label": "Vietnam (+84)",
    "dial_code": "+84",
    "code": "VN"
  },
  {
    key: index++,
    "label": "Afghanistan (+93)",
    "dial_code": "+93",
    "code": "AF"
  },
  {
    key: index++,
    "label": "Albania (+355)",
    "dial_code": "+355",
    "code": "AL"
  },
  {
    key: index++,
    "label": "Algeria (+213)",
    "dial_code": "+213",
    "code": "DZ"
  },
  {
    key: index++,
    "label": "AmericanSamoa (+1 684)",
    "dial_code": "+1 684",
    "code": "AS"
  },
  {
    key: index++,
    "label": "Andorra (+376)",
    "dial_code": "+376",
    "code": "AD"
  },
  {
    key: index++,
    "label": "Angola (+244)",
    "dial_code": "+244",
    "code": "AO"
  },
  {
    key: index++,
    "label": "Anguilla (+1 264)",
    "dial_code": "+1 264",
    "code": "AI"
  },
  {
    key: index++,
    "label": "Antarctica (null)",
    "dial_code": "null",
    "code": "AQ"
  },
  {
    key: index++,
    "label": "Antigua and Barbuda (+1268)",
    "dial_code": "+1268",
    "code": "AG"
  },
  {
    key: index++,
    "label": "Argentina (+54)",
    "dial_code": "+54",
    "code": "AR"
  },
  {
    key: index++,
    "label": "Armenia (+374)",
    "dial_code": "+374",
    "code": "AM"
  },
  {
    key: index++,
    "label": "Aruba (+297)",
    "dial_code": "+297",
    "code": "AW"
  },
  {
    key: index++,
    "label": "Australia (+61)",
    "dial_code": "+61",
    "code": "AU"
  },
  {
    key: index++,
    "label": "Austria (+43)",
    "dial_code": "+43",
    "code": "AT"
  },
  {
    key: index++,
    "label": "Azerbaijan (+994)",
    "dial_code": "+994",
    "code": "AZ"
  },
  {
    key: index++,
    "label": "Bahamas (+1 242)",
    "dial_code": "+1 242",
    "code": "BS"
  },
  {
    key: index++,
    "label": "Bahrain (+973)",
    "dial_code": "+973",
    "code": "BH"
  },
  {
    key: index++,
    "label": "Bangladesh (+880)",
    "dial_code": "+880",
    "code": "BD"
  },
  {
    key: index++,
    "label": "Barbados (+1 246)",
    "dial_code": "+1 246",
    "code": "BB"
  },
  {
    key: index++,
    "label": "Belarus (+375)",
    "dial_code": "+375",
    "code": "BY"
  },
  {
    key: index++,
    "label": "Belgium (+32)",
    "dial_code": "+32",
    "code": "BE"
  },
  {
    key: index++,
    "label": "Belize (+501)",
    "dial_code": "+501",
    "code": "BZ"
  },
  {
    key: index++,
    "label": "Benin (+229)",
    "dial_code": "+229",
    "code": "BJ"
  },
  {
    key: index++,
    "label": "Bermuda (+1 441)",
    "dial_code": "+1 441",
    "code": "BM"
  },
  {
    key: index++,
    "label": "Bhutan (+975)",
    "dial_code": "+975",
    "code": "BT"
  },
  {
    key: index++,
    "label": "Bolivia, Plurinational State of (+591)",
    "dial_code": "+591",
    "code": "BO"
  },
  {
    key: index++,
    "label": "Bosnia and Herzegovina (+387)",
    "dial_code": "+387",
    "code": "BA"
  },
  {
    key: index++,
    "label": "Botswana (+267)",
    "dial_code": "+267",
    "code": "BW"
  },
  {
    key: index++,
    "label": "Brazil (+55)",
    "dial_code": "+55",
    "code": "BR"
  },
  {
    key: index++,
    "label": "British Indian Ocean Territory (+246)",
    "dial_code": "+246",
    "code": "IO"
  },
  {
    key: index++,
    "label": "Brunei Darussalam (+673)",
    "dial_code": "+673",
    "code": "BN"
  },
  {
    key: index++,
    "label": "Bulgaria (+359)",
    "dial_code": "+359",
    "code": "BG"
  },
  {
    key: index++,
    "label": "Burkina Faso (+226)",
    "dial_code": "+226",
    "code": "BF"
  },
  {
    key: index++,
    "label": "Burundi (+257)",
    "dial_code": "+257",
    "code": "BI"
  },
  {
    key: index++,
    "label": "Cambodia (+855)",
    "dial_code": "+855",
    "code": "KH"
  },
  {
    key: index++,
    "label": "Cameroon (+237)",
    "dial_code": "+237",
    "code": "CM"
  },
  {
    key: index++,
    "label": "Canada (+1)",
    "dial_code": "+1",
    "code": "CA"
  },
  {
    key: index++,
    "label": "Cape Verde (+238)",
    "dial_code": "+238",
    "code": "CV"
  },
  {
    key: index++,
    "label": "Cayman Islands (+ 345)",
    "dial_code": "+ 345",
    "code": "KY"
  },
  {
    key: index++,
    "label": "Central African Republic (+236)",
    "dial_code": "+236",
    "code": "CF"
  },
  {
    key: index++,
    "label": "Chad (+235)",
    "dial_code": "+235",
    "code": "TD"
  },
  {
    key: index++,
    "label": "Chile (+56)",
    "dial_code": "+56",
    "code": "CL"
  },
  {
    key: index++,
    "label": "China (+86)",
    "dial_code": "+86",
    "code": "CN"
  },
  {
    key: index++,
    "label": "Christmas Island (+61)",
    "dial_code": "+61",
    "code": "CX"
  },
  {
    key: index++,
    "label": "Cocos (Keeling) Islands (+61)",
    "dial_code": "+61",
    "code": "CC"
  },
  {
    key: index++,
    "label": "Colombia (+57)",
    "dial_code": "+57",
    "code": "CO"
  },
  {
    key: index++,
    "label": "Comoros (+269)",
    "dial_code": "+269",
    "code": "KM"
  },
  {
    key: index++,
    "label": "Congo (+242)",
    "dial_code": "+242",
    "code": "CG"
  },
  {
    key: index++,
    "label": "Congo, The Democratic Republic of the (+243)",
    "dial_code": "+243",
    "code": "CD"
  },
  {
    key: index++,
    "label": "Cook Islands (+682)",
    "dial_code": "+682",
    "code": "CK"
  },
  {
    key: index++,
    "label": "Costa Rica (+506)",
    "dial_code": "+506",
    "code": "CR"
  },
  {
    key: index++,
    "label": "Cote d'Ivoire (+225)",
    "dial_code": "+225",
    "code": "CI"
  },
  {
    key: index++,
    "label": "Croatia (+385)",
    "dial_code": "+385",
    "code": "HR"
  },
  {
    key: index++,
    "label": "Cuba (+53)",
    "dial_code": "+53",
    "code": "CU"
  },
  {
    key: index++,
    "label": "Cyprus (+537)",
    "dial_code": "+537",
    "code": "CY"
  },
  {
    key: index++,
    "label": "Czech Republic (+420)",
    "dial_code": "+420",
    "code": "CZ"
  },
  {
    key: index++,
    "label": "Denmark (+45)",
    "dial_code": "+45",
    "code": "DK"
  },
  {
    key: index++,
    "label": "Djibouti (+253)",
    "dial_code": "+253",
    "code": "DJ"
  },
  {
    key: index++,
    "label": "Dominica (+1 767)",
    "dial_code": "+1 767",
    "code": "DM"
  },
  {
    key: index++,
    "label": "Dominican Republic (+1 849)",
    "dial_code": "+1 849",
    "code": "DO"
  },
  {
    key: index++,
    "label": "Ecuador (+593)",
    "dial_code": "+593",
    "code": "EC"
  },
  {
    key: index++,
    "label": "Egypt (+20)",
    "dial_code": "+20",
    "code": "EG"
  },
  {
    key: index++,
    "label": "El Salvador (+503)",
    "dial_code": "+503",
    "code": "SV"
  },
  {
    key: index++,
    "label": "Equatorial Guinea (+240)",
    "dial_code": "+240",
    "code": "GQ"
  },
  {
    key: index++,
    "label": "Eritrea (+291)",
    "dial_code": "+291",
    "code": "ER"
  },
  {
    key: index++,
    "label": "Estonia (+372)",
    "dial_code": "+372",
    "code": "EE"
  },
  {
    key: index++,
    "label": "Ethiopia (+251)",
    "dial_code": "+251",
    "code": "ET"
  },
  {
    key: index++,
    "label": "Falkland Islands (Malvinas) (+500)",
    "dial_code": "+500",
    "code": "FK"
  },
  {
    key: index++,
    "label": "Faroe Islands (+298)",
    "dial_code": "+298",
    "code": "FO"
  },
  {
    key: index++,
    "label": "Fiji (+679)",
    "dial_code": "+679",
    "code": "FJ"
  },
  {
    key: index++,
    "label": "Finland (+358)",
    "dial_code": "+358",
    "code": "FI"
  },
  {
    key: index++,
    "label": "France (+33)",
    "dial_code": "+33",
    "code": "FR"
  },
  {
    key: index++,
    "label": "French Guiana (+594)",
    "dial_code": "+594",
    "code": "GF"
  },
  {
    key: index++,
    "label": "French Polynesia (+689)",
    "dial_code": "+689",
    "code": "PF"
  },
  {
    key: index++,
    "label": "Gabon (+241)",
    "dial_code": "+241",
    "code": "GA"
  },
  {
    key: index++,
    "label": "Gambia (+220)",
    "dial_code": "+220",
    "code": "GM"
  },
  {
    key: index++,
    "label": "Georgia (+995)",
    "dial_code": "+995",
    "code": "GE"
  },
  {
    key: index++,
    "label": "Germany (+49)",
    "dial_code": "+49",
    "code": "DE"
  },
  {
    key: index++,
    "label": "Ghana (+233)",
    "dial_code": "+233",
    "code": "GH"
  },
  {
    key: index++,
    "label": "Gibraltar (+350)",
    "dial_code": "+350",
    "code": "GI"
  },
  {
    key: index++,
    "label": "Greece (+30)",
    "dial_code": "+30",
    "code": "GR"
  },
  {
    key: index++,
    "label": "Greenland (+299)",
    "dial_code": "+299",
    "code": "GL"
  },
  {
    key: index++,
    "label": "Grenada (+1 473)",
    "dial_code": "+1 473",
    "code": "GD"
  },
  {
    key: index++,
    "label": "Guadeloupe (+590)",
    "dial_code": "+590",
    "code": "GP"
  },
  {
    key: index++,
    "label": "Guam (+1 671)",
    "dial_code": "+1 671",
    "code": "GU"
  },
  {
    key: index++,
    "label": "Guatemala (+502)",
    "dial_code": "+502",
    "code": "GT"
  },
  {
    key: index++,
    "label": "Guernsey (+44)",
    "dial_code": "+44",
    "code": "GG"
  },
  {
    key: index++,
    "label": "Guinea (+224)",
    "dial_code": "+224",
    "code": "GN"
  },
  {
    key: index++,
    "label": "Guinea-Bissau (+245)",
    "dial_code": "+245",
    "code": "GW"
  },
  {
    key: index++,
    "label": "Guyana (+595)",
    "dial_code": "+595",
    "code": "GY"
  },
  {
    key: index++,
    "label": "Haiti (+509)",
    "dial_code": "+509",
    "code": "HT"
  },
  {
    key: index++,
    "label": "Holy See (Vatican City State) (+379)",
    "dial_code": "+379",
    "code": "VA"
  },
  {
    key: index++,
    "label": "Honduras (+504)",
    "dial_code": "+504",
    "code": "HN"
  },
  {
    key: index++,
    "label": "Hong Kong (+852)",
    "dial_code": "+852",
    "code": "HK"
  },
  {
    key: index++,
    "label": "Hungary (+36)",
    "dial_code": "+36",
    "code": "HU"
  },
  {
    key: index++,
    "label": "Iceland (+354)",
    "dial_code": "+354",
    "code": "IS"
  },
  {
    key: index++,
    "label": "India (+91)",
    "dial_code": "+91",
    "code": "IN"
  },
  {
    key: index++,
    "label": "Indonesia (+62)",
    "dial_code": "+62",
    "code": "ID"
  },
  {
    key: index++,
    "label": "Iran, Islamic Republic of (+98)",
    "dial_code": "+98",
    "code": "IR"
  },
  {
    key: index++,
    "label": "Iraq (+964)",
    "dial_code": "+964",
    "code": "IQ"
  },
  {
    key: index++,
    "label": "Ireland (+353)",
    "dial_code": "+353",
    "code": "IE"
  },
  {
    key: index++,
    "label": "Isle of Man (+44)",
    "dial_code": "+44",
    "code": "IM"
  },
  {
    key: index++,
    "label": "Israel (+972)",
    "dial_code": "+972",
    "code": "IL"
  },
  {
    key: index++,
    "label": "Israel (+972)",
    "dial_code": "+972",
    "code": "IL"
  },
  {
    key: index++,
    "label": "Italy (+39)",
    "dial_code": "+39",
    "code": "IT"
  },
  {
    key: index++,
    "label": "Jamaica (+1 876)",
    "dial_code": "+1 876",
    "code": "JM"
  },
  {
    key: index++,
    "label": "Japan (+81)",
    "dial_code": "+81",
    "code": "JP"
  },
  {
    key: index++,
    "label": "Jersey (+44)",
    "dial_code": "+44",
    "code": "JE"
  },
  {
    key: index++,
    "label": "Jordan (+962)",
    "dial_code": "+962",
    "code": "JO"
  },
  {
    key: index++,
    "label": "Kazakhstan (+7 7)",
    "dial_code": "+7 7",
    "code": "KZ"
  },
  {
    key: index++,
    "label": "Kenya (+254)",
    "dial_code": "+254",
    "code": "KE"
  },
  {
    key: index++,
    "label": "Kiribati (+686)",
    "dial_code": "+686",
    "code": "KI"
  },
  {
    key: index++,
    "label": "Korea, Democratic People's Republic of (+850)",
    "dial_code": "+850",
    "code": "KP"
  },
  {
    key: index++,
    "label": "Korea, Republic of (+82)",
    "dial_code": "+82",
    "code": "KR"
  },
  {
    key: index++,
    "label": "Kuwait (+965)",
    "dial_code": "+965",
    "code": "KW"
  },
  {
    key: index++,
    "label": "Kyrgyzstan (+996)",
    "dial_code": "+996",
    "code": "KG"
  },
  {
    key: index++,
    "label": "Lao People's Democratic Republic (+856)",
    "dial_code": "+856",
    "code": "LA"
  },
  {
    key: index++,
    "label": "Latvia (+371)",
    "dial_code": "+371",
    "code": "LV"
  },
  {
    key: index++,
    "label": "Lebanon (+961)",
    "dial_code": "+961",
    "code": "LB"
  },
  {
    key: index++,
    "label": "Lesotho (+266)",
    "dial_code": "+266",
    "code": "LS"
  },
  {
    key: index++,
    "label": "Liberia (+231)",
    "dial_code": "+231",
    "code": "LR"
  },
  {
    key: index++,
    "label": "Libyan Arab Jamahiriya (+218)",
    "dial_code": "+218",
    "code": "LY"
  },
  {
    key: index++,
    "label": "Liechtenstein (+423)",
    "dial_code": "+423",
    "code": "LI"
  },
  {
    key: index++,
    "label": "Lithuania (+370)",
    "dial_code": "+370",
    "code": "LT"
  },
  {
    key: index++,
    "label": "Luxembourg (+352)",
    "dial_code": "+352",
    "code": "LU"
  },
  {
    key: index++,
    "label": "Macao (+853)",
    "dial_code": "+853",
    "code": "MO"
  },
  {
    key: index++,
    "label": "Macedonia, The Former Yugoslav Republic of (+389)",
    "dial_code": "+389",
    "code": "MK"
  },
  {
    key: index++,
    "label": "Madagascar (+261)",
    "dial_code": "+261",
    "code": "MG"
  },
  {
    key: index++,
    "label": "Malawi (+265)",
    "dial_code": "+265",
    "code": "MW"
  },
  {
    key: index++,
    "label": "Malaysia (+60)",
    "dial_code": "+60",
    "code": "MY"
  },
  {
    key: index++,
    "label": "Maldives (+960)",
    "dial_code": "+960",
    "code": "MV"
  },
  {
    key: index++,
    "label": "Mali (+223)",
    "dial_code": "+223",
    "code": "ML"
  },
  {
    key: index++,
    "label": "Malta (+356)",
    "dial_code": "+356",
    "code": "MT"
  },
  {
    key: index++,
    "label": "Marshall Islands (+692)",
    "dial_code": "+692",
    "code": "MH"
  },
  {
    key: index++,
    "label": "Martinique (+596)",
    "dial_code": "+596",
    "code": "MQ"
  },
  {
    key: index++,
    "label": "Mauritania (+222)",
    "dial_code": "+222",
    "code": "MR"
  },
  {
    key: index++,
    "label": "Mauritius (+230)",
    "dial_code": "+230",
    "code": "MU"
  },
  {
    key: index++,
    "label": "Mayotte (+262)",
    "dial_code": "+262",
    "code": "YT"
  },
  {
    key: index++,
    "label": "Mexico (+52)",
    "dial_code": "+52",
    "code": "MX"
  },
  {
    key: index++,
    "label": "Micronesia, Federated States of (+691)",
    "dial_code": "+691",
    "code": "FM"
  },
  {
    key: index++,
    "label": "Moldova, Republic of (+373)",
    "dial_code": "+373",
    "code": "MD"
  },
  {
    key: index++,
    "label": "Monaco (+377)",
    "dial_code": "+377",
    "code": "MC"
  },
  {
    key: index++,
    "label": "Mongolia (+976)",
    "dial_code": "+976",
    "code": "MN"
  },
  {
    key: index++,
    "label": "Montenegro (+382)",
    "dial_code": "+382",
    "code": "ME"
  },
  {
    key: index++,
    "label": "Montserrat (+1664)",
    "dial_code": "+1664",
    "code": "MS"
  },
  {
    key: index++,
    "label": "Morocco (+212)",
    "dial_code": "+212",
    "code": "MA"
  },
  {
    key: index++,
    "label": "Mozambique (+258)",
    "dial_code": "+258",
    "code": "MZ"
  },
  {
    key: index++,
    "label": "Myanmar (+95)",
    "dial_code": "+95",
    "code": "MM"
  },
  {
    key: index++,
    "label": "Namibia (+264)",
    "dial_code": "+264",
    "code": "NA"
  },
  {
    key: index++,
    "label": "Nauru (+674)",
    "dial_code": "+674",
    "code": "NR"
  },
  {
    key: index++,
    "label": "Nepal (+977)",
    "dial_code": "+977",
    "code": "NP"
  },
  {
    key: index++,
    "label": "Netherlands (+31)",
    "dial_code": "+31",
    "code": "NL"
  },
  {
    key: index++,
    "label": "Netherlands Antilles (+599)",
    "dial_code": "+599",
    "code": "AN"
  },
  {
    key: index++,
    "label": "New Caledonia (+687)",
    "dial_code": "+687",
    "code": "NC"
  },
  {
    key: index++,
    "label": "New Zealand (+64)",
    "dial_code": "+64",
    "code": "NZ"
  },
  {
    key: index++,
    "label": "Nicaragua (+505)",
    "dial_code": "+505",
    "code": "NI"
  },
  {
    key: index++,
    "label": "Niger (+227)",
    "dial_code": "+227",
    "code": "NE"
  },
  {
    key: index++,
    "label": "Nigeria (+234)",
    "dial_code": "+234",
    "code": "NG"
  },
  {
    key: index++,
    "label": "Niue (+683)",
    "dial_code": "+683",
    "code": "NU"
  },
  {
    key: index++,
    "label": "Norfolk Island (+672)",
    "dial_code": "+672",
    "code": "NF"
  },
  {
    key: index++,
    "label": "Northern Mariana Islands (+1 670)",
    "dial_code": "+1 670",
    "code": "MP"
  },
  {
    key: index++,
    "label": "Norway (+47)",
    "dial_code": "+47",
    "code": "NO"
  },
  {
    key: index++,
    "label": "Oman (+968)",
    "dial_code": "+968",
    "code": "OM"
  },
  {
    key: index++,
    "label": "Pakistan (+92)",
    "dial_code": "+92",
    "code": "PK"
  },
  {
    key: index++,
    "label": "Palau (+680)",
    "dial_code": "+680",
    "code": "PW"
  },
  {
    key: index++,
    "label": "Palestinian Territory, Occupied (+970)",
    "dial_code": "+970",
    "code": "PS"
  },
  {
    key: index++,
    "label": "Panama (+507)",
    "dial_code": "+507",
    "code": "PA"
  },
  {
    key: index++,
    "label": "Papua New Guinea (+675)",
    "dial_code": "+675",
    "code": "PG"
  },
  {
    key: index++,
    "label": "Paraguay (+595)",
    "dial_code": "+595",
    "code": "PY"
  },
  {
    key: index++,
    "label": "Peru (+51)",
    "dial_code": "+51",
    "code": "PE"
  },
  {
    key: index++,
    "label": "Philippines (+63)",
    "dial_code": "+63",
    "code": "PH"
  },
  {
    key: index++,
    "label": "Pitcairn (+872)",
    "dial_code": "+872",
    "code": "PN"
  },
  {
    key: index++,
    "label": "Poland (+48)",
    "dial_code": "+48",
    "code": "PL"
  },
  {
    key: index++,
    "label": "Portugal (+351)",
    "dial_code": "+351",
    "code": "PT"
  },
  {
    key: index++,
    "label": "Puerto Rico (+1 939)",
    "dial_code": "+1 939",
    "code": "PR"
  },
  {
    key: index++,
    "label": "Qatar (+974)",
    "dial_code": "+974",
    "code": "QA"
  },
  {
    key: index++,
    "label": "Romania (+40)",
    "dial_code": "+40",
    "code": "RO"
  },
  {
    key: index++,
    "label": "Russia (+7)",
    "dial_code": "+7",
    "code": "RU"
  },
  {
    key: index++,
    "label": "Rwanda (+250)",
    "dial_code": "+250",
    "code": "RW"
  },
  {
    key: index++,
    "label": "Réunion (+262)",
    "dial_code": "+262",
    "code": "RE"
  },
  {
    key: index++,
    "label": "Saint Barthélemy (+590)",
    "dial_code": "+590",
    "code": "BL"
  },
  {
    key: index++,
    "label": "Saint Helena, Ascension and Tristan Da Cunha (+290)",
    "dial_code": "+290",
    "code": "SH"
  },
  {
    key: index++,
    "label": "Saint Kitts and Nevis (+1 869)",
    "dial_code": "+1 869",
    "code": "KN"
  },
  {
    key: index++,
    "label": "Saint Lucia (+1 758)",
    "dial_code": "+1 758",
    "code": "LC"
  },
  {
    key: index++,
    "label": "Saint Martin (+590)",
    "dial_code": "+590",
    "code": "MF"
  },
  {
    key: index++,
    "label": "Saint Pierre and Miquelon (+508)",
    "dial_code": "+508",
    "code": "PM"
  },
  {
    key: index++,
    "label": "Saint Vincent and the Grenadines (+1 784)",
    "dial_code": "+1 784",
    "code": "VC"
  },
  {
    key: index++,
    "label": "Samoa (+685)",
    "dial_code": "+685",
    "code": "WS"
  },
  {
    key: index++,
    "label": "San Marino (+378)",
    "dial_code": "+378",
    "code": "SM"
  },
  {
    key: index++,
    "label": "Sao Tome and Principe (+239)",
    "dial_code": "+239",
    "code": "ST"
  },
  {
    key: index++,
    "label": "Saudi Arabia (+966)",
    "dial_code": "+966",
    "code": "SA"
  },
  {
    key: index++,
    "label": "Senegal (+221)",
    "dial_code": "+221",
    "code": "SN"
  },
  {
    key: index++,
    "label": "Serbia (+381)",
    "dial_code": "+381",
    "code": "RS"
  },
  {
    key: index++,
    "label": "Seychelles (+248)",
    "dial_code": "+248",
    "code": "SC"
  },
  {
    key: index++,
    "label": "Sierra Leone (+232)",
    "dial_code": "+232",
    "code": "SL"
  },
  {
    key: index++,
    "label": "Slovakia (+421)",
    "dial_code": "+421",
    "code": "SK"
  },
  {
    key: index++,
    "label": "Slovenia (+386)",
    "dial_code": "+386",
    "code": "SI"
  },
  {
    key: index++,
    "label": "Solomon Islands (+677)",
    "dial_code": "+677",
    "code": "SB"
  },
  {
    key: index++,
    "label": "Somalia (+252)",
    "dial_code": "+252",
    "code": "SO"
  },
  {
    key: index++,
    "label": "South Africa (+27)",
    "dial_code": "+27",
    "code": "ZA"
  },
  {
    key: index++,
    "label": "South Georgia and the South Sandwich Islands (+500)",
    "dial_code": "+500",
    "code": "GS"
  },
  {
    key: index++,
    "label": "Spain (+34)",
    "dial_code": "+34",
    "code": "ES"
  },
  {
    key: index++,
    "label": "Sri Lanka (+94)",
    "dial_code": "+94",
    "code": "LK"
  },
  {
    key: index++,
    "label": "Sudan (+249)",
    "dial_code": "+249",
    "code": "SD"
  },
  {
    key: index++,
    "label": "Surilabel (+597)",
    "dial_code": "+597",
    "code": "SR"
  },
  {
    key: index++,
    "label": "Svalbard and Jan Mayen (+47)",
    "dial_code": "+47",
    "code": "SJ"
  },
  {
    key: index++,
    "label": "Swaziland (+268)",
    "dial_code": "+268",
    "code": "SZ"
  },
  {
    key: index++,
    "label": "Sweden (+46)",
    "dial_code": "+46",
    "code": "SE"
  },
  {
    key: index++,
    "label": "Switzerland (+41)",
    "dial_code": "+41",
    "code": "CH"
  },
  {
    key: index++,
    "label": "Syrian Arab Republic (+963)",
    "dial_code": "+963",
    "code": "SY"
  },
  {
    key: index++,
    "label": "Taiwan, Province of China (+886)",
    "dial_code": "+886",
    "code": "TW"
  },
  {
    key: index++,
    "label": "Tajikistan (+992)",
    "dial_code": "+992",
    "code": "TJ"
  },
  {
    key: index++,
    "label": "Tanzania, United Republic of (+255)",
    "dial_code": "+255",
    "code": "TZ"
  },
  {
    key: index++,
    "label": "Thailand (+66)",
    "dial_code": "+66",
    "code": "TH"
  },
  {
    key: index++,
    "label": "Timor-Leste (+670)",
    "dial_code": "+670",
    "code": "TL"
  },
  {
    key: index++,
    "label": "Togo (+228)",
    "dial_code": "+228",
    "code": "TG"
  },
  {
    key: index++,
    "label": "Tokelau (+690)",
    "dial_code": "+690",
    "code": "TK"
  },
  {
    key: index++,
    "label": "Tonga (+676)",
    "dial_code": "+676",
    "code": "TO"
  },
  {
    key: index++,
    "label": "Trinidad and Tobago (+1 868)",
    "dial_code": "+1 868",
    "code": "TT"
  },
  {
    key: index++,
    "label": "Tunisia (+216)",
    "dial_code": "+216",
    "code": "TN"
  },
  {
    key: index++,
    "label": "Turkey (+90)",
    "dial_code": "+90",
    "code": "TR"
  },
  {
    key: index++,
    "label": "Turkmenistan (+993)",
    "dial_code": "+993",
    "code": "TM"
  },
  {
    key: index++,
    "label": "Turks and Caicos Islands (+1 649)",
    "dial_code": "+1 649",
    "code": "TC"
  },
  {
    key: index++,
    "label": "Tuvalu (+688)",
    "dial_code": "+688",
    "code": "TV"
  },
  {
    key: index++,
    "label": "Uganda (+256)",
    "dial_code": "+256",
    "code": "UG"
  },
  {
    key: index++,
    "label": "Ukraine (+380)",
    "dial_code": "+380",
    "code": "UA"
  },
  {
    key: index++,
    "label": "United Arab Emirates (+971)",
    "dial_code": "+971",
    "code": "AE"
  },
  {
    key: index++,
    "label": "United Kingdom (+44)",
    "dial_code": "+44",
    "code": "GB"
  },
  {
    key: index++,
    "label": "United States (+1)",
    "dial_code": "+1",
    "code": "US"
  },
  {
    key: index++,
    "label": "Uruguay (+598)",
    "dial_code": "+598",
    "code": "UY"
  },
  {
    key: index++,
    "label": "Uzbekistan (+998)",
    "dial_code": "+998",
    "code": "UZ"
  },
  {
    key: index++,
    "label": "Vanuatu (+678)",
    "dial_code": "+678",
    "code": "VU"
  },
  {
    key: index++,
    "label": "Venezuela, Bolivarian Republic of (+58)",
    "dial_code": "+58",
    "code": "VE"
  },
  {
    key: index++,
    "label": "Virgin Islands, British (+1 284)",
    "dial_code": "+1 284",
    "code": "VG"
  },
  {
    key: index++,
    "label": "Virgin Islands, U.S. (+1 340)",
    "dial_code": "+1 340",
    "code": "VI"
  },
  {
    key: index++,
    "label": "Wallis and Futuna (+681)",
    "dial_code": "+681",
    "code": "WF"
  },
  {
    key: index++,
    "label": "Yemen (+967)",
    "dial_code": "+967",
    "code": "YE"
  },
  {
    key: index++,
    "label": "Zambia (+260)",
    "dial_code": "+260",
    "code": "ZM"
  },
  {
    key: index++,
    "label": "Zimbabwe (+263)",
    "dial_code": "+263",
    "code": "ZW"
  }
]
