import React from 'react'
import { Dimensions } from 'react-native'
import {TabNavigator} from 'react-navigation'

import CustomTabBar from './customTabBar'

import Icons from '../configs/icons'
import HomeScreen from '../layouts/home'
import DoctorsScreen from '../layouts/doctors'
import AppointmentsScreen from '../layouts/appointments'
import BillingScreen from '../layouts/billing'
import AccountScreen from '../layouts/account'

const window = Dimensions.get('window')

const navigate = {}

// const tabPress = (currentRoute) => {
//     // console.log(currentRoute, 'current route')
//     navigate(currentRoute.route.routeName)
// }

// const setNavigate = (props) => {
//     navigate = props
//     console.log(navigate, 'set navigate')
// }

const tabsOption = {
    tabBarPosition: 'top',
    swipeEnabled: true,
    // animationEnabled: true,
    // lazy: true,
    tabBarOptions: {
        showIcon: true,
        showLabel: false,
        activeTintColor: '#FFF',
        inactiveTintColor: '#D5D5D5',
        scrollEnabled: true,
        tabStyle: {
          width: window.width/5,
        },
        style: {
          backgroundColor: '#27909a',
          paddingTop: 10,
          height: 60,
        },
        // tabStyle: {
        //     width: 60,
        //     height: 45
        // },
        iconStyle: {
            width: 17,
            height: 17
        },
        indicatorStyle: {
            backgroundColor: '#555',
        },
        // onTabPress: tabPress,
    },
    initialRouteName: 'Home',
    lazy: true,
    tabBarComponent: CustomTabBar,
}

const TabsNavigation = TabNavigator({
    Home: {
        // screen: (props) => <HomeScreen {...props} screenProps={{setNavigate: setNavigate.bind(this)}} />,
        // navigationOptions: {
        //     header: null,
        //     tabBarIcon: Icons.Home,
        //     tabBarPosition: 'top',
        // }
        screen: HomeScreen
    },
    Doctors: {screen: DoctorsScreen},
    Appointments: {screen: AppointmentsScreen},
    Billing: {screen: BillingScreen},
    Account: {screen: AccountScreen}
}, tabsOption)

export default TabsNavigation
