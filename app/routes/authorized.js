import { StackNavigator } from 'react-navigation'
import { Platform } from 'react-native'
import I18n from 'react-native-i18n'
import { Colors } from '../configs/constants'

import TabsNavigation from './tabsNavigation'
// import AccountStack from './accountStack'
import SettingsContainer from '../lib/mutilLanguages/Containers/SettingsContainer'
import MakeAppointmentScreen from '../layouts/appointments/makeAppointment'
import InfoDoctor from '../layouts/doctors/infoDoctor'
import ChangePasswordScreen from '../layouts/account/changePassword'
import ChangeProfileScreen from '../layouts/account/changeProfile'
import PaymentScreen from '../layouts/account/payment'
import AppointmentDetailPatient from '../layouts/appointments/appointmentDetailPatient'
import BillingDetailPatientScreen from '../layouts/billing/billingDetail'
import SummaryAppointmentScreen from '../layouts/appointments/summaryAppointment'
import FastConnectScreen from '../layouts/appointments/fastConnect'
import ThankYouScreen from '../layouts/appointments/fastConnect/thankYou'
import VideoCallScreen from '../layouts/VideoCall'

const StackOptions = {
  // initialRouteName: 'FastConnect',
  navigationOptions: {
    headerBackTitle: null,
    headerStyle: {
      backgroundColor: Colors.BACKGROUND_STACKS,
      paddingBottom:10,
    },
    headerTintColor: '#FFF',
  },
  mode: (Platform.OS === 'ios') ? 'modal' : 'card',
}

const AuthorizedNavigation = StackNavigator({
  TabsNavigation: {
    screen: TabsNavigation
  },
  MakeAppointment: {
    screen: MakeAppointmentScreen,
  },
  FastConnect: {
    screen: FastConnectScreen,
  },
  ThankYou: {
    screen: ThankYouScreen,
  },
  SettingLanguages: {
    screen: SettingsContainer,
  },
  /*ChatTool: {
    screen: ChatToolScreen
  },*/
  InfoDoctor: {
    screen: InfoDoctor
  },
  ChangePassword: {
    screen: ChangePasswordScreen
  },
  AppointmentDetailPatient: {
    screen: AppointmentDetailPatient
  },
  SummaryAppointment: {
    screen: SummaryAppointmentScreen
  },
  BillingDetailPatient: {
    screen: BillingDetailPatientScreen
  },
  ChangeProfile: {
    screen: ChangeProfileScreen
  },
  Payment: {
    screen: PaymentScreen
  },
  VideoCall: {
    screen: VideoCallScreen
  }
}, StackOptions)

export default AuthorizedNavigation;
