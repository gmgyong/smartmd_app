import React, { Component, PropTypes } from 'react'
import { BackHandler } from 'react-native'
import { connect } from 'react-redux'
import StartupActions from '../lib/mutilLanguages/Redux/StartupRedux'
import ReduxPersist from '../lib/mutilLanguages/Config/ReduxPersist'

import I18n from 'react-native-i18n'
import { TabNavigator, StackNavigator, addNavigationHelpers } from 'react-navigation'

import AuthorizedNavigation from './authorized'
import UnauthorizedNavigation from './unauthorized'

const props = []

class NavigationTabs extends Component {
  constructor(props) {
    super(props)

    /*this.state = {
      listNotification: this.props.listNotification
    }*/
    props = this.props.listNotification
  }

  componentDidMount() {
    if (!ReduxPersist.active) {
      this.props.startup()
    }
  }

  render() {
    // const {language} = this.props

    const Root = StackNavigator({
      // initialRouteName: {
      //   screen: AuthorizedNavigation
      // },
      Unauthorized: {
        screen: UnauthorizedNavigation,
        navigationOptions: {
          header: null,
        }
      },
      Authorized: {
        screen: AuthorizedNavigation,
        /*screen: () => {
          return <AuthorizedNavigation listNotification={this.props.listNotification}} />
        },*/
        navigationOptions: {
          header: null,
        }
      }
    });

    return (
      <Root
        onNavigationStateChange={(prevState, currentState, action) => {
          console.log(prevState, ' --- ', currentState, ' --- ', action, 'check onNavigationStateChange')
          if((action.type === 'Navigation/BACK' && currentState.index === 0 && prevState.index === 1) ||
             (action.type === 'Navigation/BACK' && currentState.index === 0 && currentState.routes[0].index === 0) ||
             (action.type === 'Navigation/BACK' && currentState.index > 0   && currentState.routes[currentState.routes.length - 1].index === 0 && currentState.routes[currentState.routes.length - 1].routeName === 'Unauthorized') ||
             (action.type === 'Navigation/BACK' && currentState.index > 0   && currentState.routes[currentState.routes.length - 1].index > 0 && 
                prevState.routes[prevState.routes.length - 1].routes[prevState.routes[prevState.routes.length - 1].routes.length-1].routeName !== 'VideoCall' && 
                prevState.routes[prevState.routes.length - 1].routes[prevState.routes[prevState.routes.length - 1].routes.length-1].routeName !== 'BillingDetailPatient' && 
                prevState.routes[prevState.routes.length - 1].routes[prevState.routes[prevState.routes.length - 1].routes.length-1].routeName !== 'AppointmentDetailPatient' && 
                prevState.routes[prevState.routes.length - 1].routeName === 'Authorized')) {
            // console.log('check condition', currentState.routes[currentState.routes.length - 1].index, prevState.routes[prevState.routes.length - 1].routeName)
            BackHandler.exitApp()
          }
        }}
      />
    )
  }
}

const mapStateToDispatch = dispatch => ({
  startup: () => dispatch(StartupActions.startup())
})


export default connect(null, mapStateToDispatch)(NavigationTabs)

NavigationTabs.propTypes = {
  startup: PropTypes.func.isRequired,
}
