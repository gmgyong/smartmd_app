import { StackNavigator } from 'react-navigation'
import { Platform } from 'react-native'

import SplashScreen from '../layouts/splash'
import SignInScreen from '../layouts/signIn'
import SignUpScreen from '../layouts/signUp'
import ForgotPasswordScreen from '../layouts/forgotPassword'
import UpdateProfileScreen from '../layouts/signIn/updateProfile'

const StackOptions = {
    // initialRouteName: 'ForgotPassword',
    navigationOptions: {
        headerBackTitle: null,
        headerStyle: {
            backgroundColor: '#35c1cf',
            paddingBottom:10,
        },
        headerTintColor: '#FFF',
    },
    mode: (Platform.OS === 'ios') ? 'modal' : 'card',
};

const UnauthorizedNavigation = StackNavigator({
  Splash: {
    screen: SplashScreen,
    navigationOptions: {
      header: null,
    }
  },
  SignIn: {
    screen: SignInScreen,
    navigationOptions: {
      header: null,
    }
  },
  UpdateProfileSocial: {
    screen: UpdateProfileScreen
  },
  SignUp: { screen: SignUpScreen },
  ForgotPassword: { screen: ForgotPasswordScreen },
  // ForgotPasswordVerify: { screen: '' },
  // ForgotPasswordNewPassword: { screen: '' },
}, StackOptions);

export default UnauthorizedNavigation;
