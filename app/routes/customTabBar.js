import React from 'react'
import { NavigationActions, TabBarTop } from 'react-navigation'

const getCurrentTabRoot = (state) => {
  return state.routes[state.index].routes[0].routeName
}

const resetTabNavigation = (dispatch, rootRouteName) => {
  dispatch(NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({
      routeName: rootRouteName,
    })],
  }))
}

export default (props) => {
  return <TabBarTop
    {...props}
    onTabPress={(scene) => {
      console.log(scene, 'onTabPress')
      props.navigation.navigate(scene.route.routeName)
    }}
    jumpToIndex={(index, ...args) => {
      // if trying to move to the same tab
      if (index === props.navigation.state.index) {
        // reset the navigation
        /*resetTabNavigation(
          props.navigation.dispatch,
          getCurrentTabRoot(props.navigation.state),
        )*/
      }

      // console.log(props.navigation.state.routes[index].routeName, 'props.navigation')
     
      // in any case proceed with normal jumpToIndex functionality
      props.jumpToIndex(index, ...args)
      // props.navigation.navigate(props.navigation.state.routes[index].routeName, ...args)
    }}
  />
}
