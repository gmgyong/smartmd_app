import React from 'react';

import Environments from './environmentsAPIs';

class Logger {
  constructor() {
  }

  log(message) {
    if (Environments.LOGGER_ENABLE) {
      console.log(message);
    }
  }
}

module.exports = new Logger();
