import React from 'react';

import { NetInfo, Platform, ToastAndroid } from 'react-native';

import { StorageKeys } from './constants'
import Environments from './environmentsAPIs';
import LocalStorage from '../components/localStorage'
// import Logger from './logger';
import { APIs, AccountAuth } from '../configs/constants'

class GmgHttpClient {
  constructor() {
    // private constants.
    this.HTTP_METHOD_POST = 'POST';
    this.HTTP_METHOD_GET = 'GET';
    this.HTTP_METHOD_PUT = 'PUT';
    this.HTTP_METHOD_DELETE = 'DELETE';
    this.CONTENT_TYPE_JSON = 'application/json';
    this.HTTP_HEADER_ACCEPT = 'Accept';
    this.HTTP_HEADER_CONTENT_TYPE = 'Content-Type';
    this.HTTP_HEADER_X_PLANITV1_EMAIL = 'X-PLANITV1-EMAIL';
    this.HTTP_HEADER_X_PLANITV1_ACCESSTOKEN = 'X-PLANITV1-ACCESSTOKEN';
    this.HTTP_HEADER_X_PLANITV1_IDTOKEN = 'X-PLANITV1-IDTOKEN';
    this.HTTP_HEADER_X_PLANITV1_USERID = 'X-PLANITV1-USERID';
    this.HTTP_HEADER_X_PLANITV1_ACCESS_TOKEN = 'X-PLANITV1-ACCESS-TOKEN';
    this.HTTP_HEADER_X_PLANITV1_INSTITUTIONID = 'X-PLANITV1-INSTITUTIONID';
    this.HTTP_HEADER_X_PLANITV1_PLATFORM = 'X-PLANITV1-PLATFORM';

    // private properties.
    this.userId = '';
    this.email = '';
    this.gmgAccessToken = '';
    this.institutionId = Environments.SYSTEM_INSTITUTION_ID;
  }

  setUserId(userId) {
    this.userId = userId;
  }

  async setAccessToken(token) {
    await LocalStorage.setString(StorageKeys.TOKENDATA, token);
  }

  setGmgAuthentication(userId, token) {
    this.setUserId(userId);
    this.setAccessToken(token);
  }

  postWithGmgHeaders(api, body, success, failure) {
    this.post(true, api, body, success, failure);
  }

  postData(api, body, success, failure) {
    this.post(false, api, body, success, failure);
  }

  post(gmgEnabled, api, body, success, failure) {
    this.request(gmgEnabled, this.HTTP_METHOD_POST, api, body, success, failure);
  }

  putWithGmgHeaders(api, body, success, failure) {
    this.put(true, api, body, success, failure);
  }

  putData(api, body, success, failure) {
    this.put(false, api, body, success, failure);
  }

  put(gmgEnabled, api, body, success, failure) {
    this.request(gmgEnabled, this.HTTP_METHOD_PUT, api, body, success, failure);
  }

  deleteWithGmgHeaders(api, success, failure) {
    this.delete(true, api, success, failure);
  }

  deleteData(api, success, failure) {
    this.delete(false, api, success, failure);
  }

  delete(gmgEnabled, api, success, failure) {
    this.request(gmgEnabled, this.HTTP_METHOD_DELETE, api, null, success, failure);
  }

  getWithGmgHeaders(api, success, failure) {
    this.get(true, api, success, failure);
  }

  getData(api, success, failure) {
    this.get(false, api, success, failure);
  }

  get(gmgEnabled, api, success, failure) {
    this.request(gmgEnabled, this.HTTP_METHOD_GET, api, null, success, failure);
  }

  async request(gmgEnabled, method, api, body, success, failure) {

    //check wifi is available
    NetInfo.fetch().then((reach) => {
      // console.log('Initial: ' + reach);

      if(reach === 'NONE' || reach === 'none') {
        if(Platform.OS === 'ios') {
          alert("Please connect network to continue")
        } else {
          ToastAndroid.show("Please connect network to continue", ToastAndroid.SHORT, ToastAndroid.BOTTOM);
        }
        return
      }
    });

    const token = await LocalStorage.getString(StorageKeys.TOKENDATA)
    let url

    if(method === this.HTTP_METHOD_GET || method === this.HTTP_METHOD_DELETE) {
      url = Environments.API_URL + api + '&api_token=' + token
    } else {
      url = Environments.API_URL + api + '?api_token=' + token
    }

    const bodyString = body !== null ? JSON.stringify(body) : null;

    let headers;
    if (!gmgEnabled) {
      headers = {
        'Accept': this.CONTENT_TYPE_JSON,
        'Content-Type': this.CONTENT_TYPE_JSON,
      };
    } else {
      headers = {
        'Accept': this.CONTENT_TYPE_JSON,
        'Content-Type': this.CONTENT_TYPE_JSON,
        // 'X-PLANITV1-USERID': this.userId,
        // 'X-PLANITV1-ACCESS-TOKEN': this.GmgAccessToken,
        // 'X-PLANITV1-INSTITUTIONID': this.institutionId,
        // 'X-PLANITV1-PLATFORM': Environments.SYSTEM_PLATFORM
      };
    }

    const encodedUrl = encodeURI(url);
    console.log('>>>>> Request ' + method + ': ' + encodedUrl + '\nHeaders: ' + JSON.stringify(headers) + '\nBody: ' + bodyString);

    fetch(encodedUrl,
      {
        method: method,
        headers: headers,
        body: bodyString
      })
      .then((response) => {
        // console.log('<<<<< Response Sucess: ' + JSON.stringify(response));
        return response.json();
      })
      .then((responseJson) => {
        console.log('<<<<< Response Sucess JSON: ')
        console.log(responseJson)
        switch (responseJson.status_code) {
          case 400: {
            console.log('-----REFRESH TOKEN-----')
            return this.getAccessTocken(true,gmgEnabled, method, api, body, success, failure)
          }
          default:
            return success(responseJson);
        }

      })
      .catch((error) => {
        console.log('<<<<< Response Error: ')
        console.log(error)
        return failure(error);
      });
  }

  getAccessTocken(callbackFunc,gmgEnabled, method, api, body, success, failure) {
    this.postData(
      APIs.URL_AUTH,
      AccountAuth,
      json => {
        if(json.success) {
          let tokenData = json.data.api_token;

          this.setAccessToken(tokenData);
          console.log('-------SET NEW ACCESS TOKEN: \n' + tokenData);
        } else {
          alert("Please check network");
        }
        callbackFunc ? this.request(gmgEnabled, method, api, body, success, failure) : null;
      },
      error => {
        console.log(error)
      }
    )
  }
}

const GmgClient = new GmgHttpClient();

module.exports = GmgClient;
