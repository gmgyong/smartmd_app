import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  updateListAppointmentConfirmed: ['listAppointmentConfirmed']
})

export const UpdateAPIsTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  listAppointmentConfirmed: [{error: false, goodies: null}]
})

/* ------------- Reducers ------------- */

// request the data from an api
export const updateListAppointmentConfirmed = (state, {listAppointmentConfirmed}) => state.merge({
  listAppointmentConfirmed
})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.UPDATE_APIS]: updateListAppointmentConfirmed,
})
