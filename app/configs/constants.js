import { Dimensions} from 'react-native'

const Patterns = {
  CHARACTERS: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
  DATE_TIME: 'YYYY-MM-DD HH:mm:ss',
}

const APIs = {
  URL_AUTH: 'api/authentication',
  SIGN_IN: 'api/6/login',
  SIGN_UP: 'api/6/user',
  CREATE_PASSWORD_RESET_TOKEN: 'api/6/password-reset-token',
  RESET_PASSWORD: 'api/6/reset-password',
  CHANGE_PASSWORD: 'api/6/change-password',
  LIST_DOCTOR: 'api/6/doctor',
  LIST_APPOINTMENT_READY: 'api/6/appointment',
  LIST_APPOINTMENT_REQUEST: 'api/6/appointment-request',
  APPOINTMENT_INFO: 'api/6/appointment-info',
  CREATE_APPOINTMENT_REQUEST: 'api/6/appointment-request-step1',
  UPDATE_APPOINTMENT_REQUEST_STEP1: 'api/6/appointment-request-step1',
  UPDATE_APPOINTMENT_REQUEST_STEP2: 'api/6/appointment-request-step2',
  UPDATE_APPOINTMENT_REQUEST_STEP4: 'api/6/appointment-request-step4',
  UPLOAD_FILE_APPOINTMENT_REQUEST_STEP1: 'api/6/appointment/upload-file',
  DELETE_FILE_APPOINTMENT_REQUEST_STEP1: 'api/6/appointment/delete-file',
  CREDIT_CARD: 'api/6/credit-card',
  USER_INFO: 'api/6/user',
  LIST_INVOICES: 'api/6/invoice',
  TOKBOX: 'api/6/teleconsult',
  UPDATE_DEVICE_TOKEN: 'api/6/update-device-token',
  ACCEPT_FAST_CONNECT: 'api/6/fast-connect/accept',
  LIST_FAST_CONNECT: 'api/6/fast-connect/list',
  FAST_CONNECT: 'api/6/fast-connect',
  CANCEL_FAST_CONNECT: 'api/6/fast-connect/delete',
  CANCEL_APPOINTMENT_REQUEST: 'api/6/appointment-request/cancel',
  LOGIN_FACEBOOK: 'api/6/social/facebook/login',
  LOGIN_GOOGLE: 'api/6/social/google/login',
  CHECK_EMAIL_EXIST: 'api/6/check-email-exist',
}

const AccountAuth = {
  // Dev environment
  // username: 'api@s3corp.com.vn',
  // password: '25d55ad283aa400af464c76d713c07ad'
  
  // Demo/Test environment
  // username: 'api@s3corp.com.vn',
  // password: '25f9e794323b453885f5181f1b624d0b'

  // Prod environment
  username: 'api@gmghealth.com',
  password: '550e1bafe077ff0b0b67f4e32f29d751'
}

const AccountGoogleSignin = {
  //Prod environment

  iosClientId: '1019876724544-l1npmtltaittqkpjuc7k27uttj80jgll.apps.googleusercontent.com',
  webClientId: '1019876724544-h0fbspsupoa9fh7p6baaeu6ko8cuc6i7.apps.googleusercontent.com',
}

const TextStatic = {
  DATE_TIME_PLACEHOLDER: 'DD-MM-YYYY HH:MM'
}

const Pages = {
  SIGNIN: 'Signin',
  SIGNUP: 'Signup',
  FORGOT_PASSWORD: 'ForgotPassword',
  RESET_PASSWORD: 'Reset Password',
  CONFIRM_NEW_PASSWORD: 'ConfirmNewPassword',
  VERIFY_EMAIL: 'VerifyEmail',
  LOGOUT: 'Logout',
  HOME: 'Home',
  SETTINGS: 'Settings',
  NOTIFICATIONS: 'Notifications',
  NOTIFICATION_DETAIL: 'Notification Detail',
};

const Colors = {
  STATUS_READY: '#06b527',
  STATUS_UP_COMING: '#0054a6',
  STATUS_AWAITING: '#e0a10b',
  STATUS_ON_GOING: '#8dc63f',
  STATUS_NO_SCHEDULED: '#e0a10b',
  STATUS_NO_SHOW: '#ed1c24',
  STATUS_PAST: '#35c1cf',
  STATUS_CANCEL: '#ed1c24',

  BACKGROUND_TABS_INDICATOR: '#555',
  BACKGROUND_TABS: '#28909b',
  BACKGROUND_STACKS: '#35c1cf',
  BACKGROUND_VALIDATE_ERROR: '#f23d2c',
  BACKGROUND_FIELD: '#f5f5f5',
  BACKGROUND_MAIN_SCREEN: '#fff',
  BACKGROUND_BUTTON_GREY: '#bcbcbc',
  BACKGROUND_BUTTON_GREY_OVER: '#d5d5d5',
  BACKGROUND_BUTTON_PURPLE: '#584b8d',
  BACKGROUND_BUTTON_PURPLE_OVER: '#8a78d4',
  BACKGROUND_BUTTON_BLUE: '#35c1cf',
  BACKGROUND_BUTTON_BLUE_OVER: '#3fe4f4',
  BACKGROUND_BUTTON_BLUE_DISABLE: '#4a7478',
  BACKGROUND_CHATLOG_DOCTOR: '#f0f0f0',
  BACKGROUND_CHATLOG_PATIENT: '#daeffd',


  TEXT_PRIMARY: '#555',
  TEXT_SECONDARY: '#35c1cf',
  TEXT_PLACEHOLDER: '#a4a4a4',
  TEXT_TABS_ICON: '#fff',
  TEXT_TITLE: '#6bccd7',
  TEXT_TITLE_ERROR: '#f23d2c',
  TEXT_CHATLOG_DATETIME: '#999999',

  BORDER: '#959595',
};

const FontSizes = {
  SUPER_HUGE: 20,
  HUGE: 18,
  LARGE: 16,
  BIG: 14,
  NORMAL: 12,
  SMALL: 10,
  TINY: 8,
};

const FontNames = {
  PRIMARY: 'AvenirNext'
};

const StorageKeys = {
  USERNAME: 'username',
  PASSWORD: 'password',
  TOKENDATA: 'tokenData',
  USERID: 'userID',
  USERFULLNAME: 'userFullName',
  USEREMAIL: 'userEmail',
  USERURL: 'userURL',
  DEVICE_TOKEN: 'tokenDevice',
  DEVICE_OS: 'tokenOS',
  NOTIFICATION: 'notification',
  TYPE_LOGIN: 'typeLogin',
}

const APIStatuses = {
  NOT_AUTHENTICATED: '401',
};

const DayTypes = {
  1: 'EVERYDAY',
  2: 'MON',
  3: 'TUE',
  4: 'WED',
  5: 'THU',
  6: 'FRI',
  7: 'SAT',
  8: 'SUN',
}

const DayTypeValues = {
  EVERYDAY: '1',
  MON: '2',
  TUE: '3',
  WED: '4',
  THU: '5',
  FRI: '6',
  SAT: '7',
  SUN: '8',
}

const NotificationTypes = {

}

const NotificationTypesText = {

}

const CalculateSpace = () => {
  const MaxWidth = 480
  let SPACE = (Dimensions.get('window').width - MaxWidth)/2;

  SPACE = (SPACE < 20) ? 20 : SPACE
  console.log(SPACE)

  return SPACE
}

const SPACE = CalculateSpace()

module.exports = {
  Patterns,
  APIs,
  Pages,
  Colors,
  FontSizes,
  FontNames,
  StorageKeys,
  APIStatuses,
  DayTypes,
  DayTypeValues,
  NotificationTypes,
  NotificationTypesText,
  AccountAuth,
  AccountGoogleSignin,
  TextStatic,
  SPACE
}
