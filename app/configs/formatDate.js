import moment from 'moment-timezone'

class FormatDateTime {
  switchToLocalTime = (date) => {
    let gmtDateTime = moment.utc(date, "YYYY-MM-DD HH:mm")
    let local = gmtDateTime.local().format('DD MMM YYYY HH:mm')

    return local
  }

  changeFormat = (date) => {
    let newFormat = moment(date).format('DD MMM YYYY HH:mm')

    // console.log(newFormat, 'test newFormat')

    return newFormat
  }
}

const FormatDate = new FormatDateTime();

module.exports = FormatDate;
