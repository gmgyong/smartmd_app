import React from 'react'
import { Icon } from 'react-native-elements'
import { Colors } from './constants'

const iconStyle = {
  tabs: {
    type: 'font-awesome',
    color: Colors.TEXT_TABS_ICON,
    size: 17
  },
}

const Icons = {
  Home: () => (
    <Icon name="home" type={iconStyle.tabs.type} size={iconStyle.tabs.size} color={iconStyle.tabs.color} />
  ),
  Doctors: () => (
    <Icon name="stethoscope" type={iconStyle.tabs.type} size={iconStyle.tabs.size} color={iconStyle.tabs.color} />
  ),
  Appointments: () => (
    <Icon name="history" type={iconStyle.tabs.type} size={iconStyle.tabs.size} color={iconStyle.tabs.color} />
  ),
  Billing: () => (
    <Icon name="usd" type={iconStyle.tabs.type} size={iconStyle.tabs.size} color={iconStyle.tabs.color} />
  ),
  Account: () => (
    <Icon name="user-circle-o" type={iconStyle.tabs.type} size={iconStyle.tabs.size} color={iconStyle.tabs.color} />
  ),
} 

export default Icons;
