import React from 'react';

import {
  NetInfo,
  Alert
} from 'react-native';

class GmgNetInfomation {
  checkNetInfo = () => {
    NetInfo.fetch().then((reach) => {
      console.log('Initial: ' + reach);

      if(reach === 'NONE' || reach === 'none') {
        this.alertDisconnect
      }
    });


    NetInfo.addEventListener(
      'change',
      this.handleFirstConnectivityChange
    );
  }

  alertDisconnect = () => {
    Alert.alert(
      'Alert',
      'Your device is disconnect. Please connect network to continue.',
      [
        {text: 'OK', onPress: () => console.log('click Ok')},
      ]
    );
  }
  
  handleFirstConnectivityChange = (reach) => {
    console.log('Then, is ' + reach);

    if(reach === 'NONE' || reach === 'none') {
      this.alertDisconnect()
    }
    // NetInfo.isConnected.removeEventListener(
    //   'change',
    //   this.handleFirstConnectivityChange
    // );
  }
}

const GmgNetInfo = new GmgNetInfomation();

module.exports = GmgNetInfo;
