import React, { Component, PropTypes } from 'react'
import { View, Text, Picker, TouchableOpacity, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import SettingsActions from '../Redux/SettingsRedux'
import I18n from 'react-native-i18n'
import Icons from '../../../configs/icons'
import localStyles from './localStyles'

class SettingsContainer extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: I18n.t('account.language.titleUpper'),
  })


  render() {
    const {language, changeLanguage} = this.props
    const {setParams} = this.props.navigation
    const languageOptions = Object.keys(I18n.translations).map((lang, i) => {
      return (
        <Picker.Item key={ i }
                     label={ I18n.translations[lang].id }
                     value={ lang } />
      )
    })

    return (
      <View style={ localStyles.container }>
      <View style={ localStyles.main }>
        <View style={{justifyContent: 'center', flex: 1,}}>
          <Text style={ localStyles.header }>
            { I18n.t('account.language.titleLower', { locale: language }) }
          </Text>
        </View>
        <Picker style={ localStyles.picker }
                selectedValue={ language }
                onValueChange={ this._languageChanged(changeLanguage, setParams) }>
                { languageOptions }
        </Picker>
        </View>
      </View>
    )
  }

  _languageChanged = (changeLanguage, setParams) => (newLang) => {
    changeLanguage(newLang)
    setParams({
      title: I18n.t('account.title', { locale: newLang })
    })
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language,
  }
}

const mapStateToDispatch = dispatch => ({
  changeLanguage: (newLang) => dispatch(SettingsActions.changeLanguage(newLang))
})

export default connect(mapStateToProps, mapStateToDispatch)(SettingsContainer)

SettingsContainer.propTypes = {
  changeLanguage: PropTypes.func.isRequired,
  language: PropTypes.string.isRequired
}
