import { StyleSheet } from 'react-native'
import { Colors } from '../../../configs/constants'

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_MAIN_SCREEN
  },
  main: {
      flexDirection: 'row',
      marginLeft: 30,
      marginRight: 30,
  },
  header: {
    fontSize: 14,
  },
  picker: {
    flex: 2,
  },
  wrapper: {
    marginLeft: 30,
    marginRight: 30,
    marginTop: 15,
    marginBottom: 15
  },
  wrapText: {
    height: 35,
    borderBottomColor: Colors.BACKGROUND_BUTTON_GREY,
    borderBottomWidth: 0.5,
    paddingVertical: 5,
    marginBottom: 5,
  },
  text: {
    fontSize: 14,
    color: Colors.TEXT_PRIMARY,
  }
})

/** export module */
module.exports = localStyles
