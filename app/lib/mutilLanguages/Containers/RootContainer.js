import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import ReduxPersist from '../Config/ReduxPersist'

import I18n from 'react-native-i18n'
import { TabNavigator } from 'react-navigation'

import WelcomeContainer from '../Containers/WelcomeContainer'
import SettingsContainer from '../Containers/SettingsContainer'
import About from '../Components/About'



class RootContainer extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    if (!ReduxPersist.active) {
      this.props.startup()
    }

  }

  render() {
    const {language} = this.props
    const AppNavigator = TabNavigator({
      Home: {
        screen: WelcomeContainer,
      },
      Settings: {
        screen: SettingsContainer,
      },
      About:{
        screen: About,
        navigationOptions: {
          title: I18n.t('about.title', { locale: language })/*(navigation) => {
            return navigation.state.params.title
          }*/
        }
      },
      TestSettings: {
        screen: SettingsContainer,
      },
    }, {
      tabBarPosition: 'bottom'
    })

    return (
      <AppNavigator />
    )
  }
}

const mapStateToDispatch = dispatch => ({
  startup: () => dispatch(StartupActions.startup())
})


export default connect(null, mapStateToDispatch)(RootContainer)

RootContainer.propTypes = {
  startup: PropTypes.func.isRequired
}
