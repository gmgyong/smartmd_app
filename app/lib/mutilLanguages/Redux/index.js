import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'
// import testReduce from '../../../configs/Redux/testAPI'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    settings: require('./SettingsRedux').reducer,
    // updateAPIs: /*testReduce*/require('../../../configs/Redux/updateAPIs').reducer
  })

  return configureStore(rootReducer, rootSaga)
}
